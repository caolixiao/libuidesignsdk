//
//  MIPageView.swift
//  mice
//
//  Created by caolixiao on 2/9/20.
//  Copyright © 2020 Grand China Express International Business Travel Co.,Ltd. All rights reserved.
//

import UIKit
import LibBaseSDK
import FSPagerView

protocol MIPageViewDelegate: class {
    func pagerViewdidSelectItemAt(index: Int)
}

public class MIPageView: UIView, FSPagerViewDelegate, FSPagerViewDataSource {
    
    weak var delegate: MIPageViewDelegate?
    public var cellCornerRadius: CGFloat = 0.0
    
    public var pagerModels = [MITeamInfoRespModel](){
        didSet {
            if oldValue != pagerModels {
                _pagerControl.numberOfPages = pagerModels.count
                _pagerView.reloadData()
            }
        }
    }
    
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        
        self.addSubview(_pagerView)
        self.addSubview(_pagerControl)
    }
        
    lazy var _pagerView : FSPagerView = {
        let pager = FSPagerView(frame: self.bounds)
        pager.backgroundColor = .white
        pager.dataSource = self as FSPagerViewDataSource
        pager.delegate = self as FSPagerViewDelegate
        pager.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "identify_cell_pager")
        //设置自动翻页事件间隔，默认值为0（不自动翻页）
        pager.automaticSlidingInterval = 5
        pager.itemSize = FSPagerView.automaticSize
        pager.removesInfiniteLoopForSingleItem = true
        //        pager.alwaysBounceHorizontal = true
        //设置页面之间的间隔距离
        //        _fpage.interitemSpacing = 8.0
        //设置可以无限翻页，默认值为false，false时从尾部向前滚动到头部再继续循环滚动，true时可以无限滚动
        pager.isInfinite = true
        
        return pager
    }()
    
    lazy var _pagerControl: FSPageControl = {
        let pageControl = FSPageControl(frame: CGRect(x: 20, y: VIEWH(self) - 35, width: __SCREEN_WIDTH - 20, height: 25))
        
        pageControl.contentHorizontalAlignment = .center
        pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        pageControl.setFillColor(color_main, for: .selected)
        pageControl.interitemSpacing = 5
        pageControl.setImage(UIImage(named:"home_page_view_seleted")?.withRenderingMode(.alwaysTemplate), for: .normal)
        pageControl.setImage(UIImage(named:"home_page_view_unseleted")?.withRenderingMode(.alwaysTemplate), for: .selected)
        pageControl.tintColor = color_f2f2f2
        pageControl.hidesForSinglePage = true
        return pageControl
    }()
    
    func pagerModelCommonUI() {
        _pagerControl.contentHorizontalAlignment = .right
        cellCornerRadius = 0
    }
    
    // MARK:- FSPagerViewDataSource
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return pagerModels.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        var imgUrl: URL?
        if let url = NSURL.init(string: pagerModels[index].picUrl) as URL? {
            imgUrl = url
        }

        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "identify_cell_pager", at: index)
        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.image = UIImage(named: "personal_head_man")
//        cell.imageView?.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "00000000006"))
        cell.isSelected = false
        cell.isHighlighted = false
        cell.contentView.layer.cornerRadius = cellCornerRadius
        cell.contentView.layer.masksToBounds = true
        return cell
    }
    
    
    public func pagerView(_ pagerView: FSPagerView, shouldHighlightItemAt index: Int) -> Bool {
        return true
    }
    
    public func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        _pagerControl.currentPage = index
        delegate?.pagerViewdidSelectItemAt(index: index)
    }
    
    public func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        _pagerControl.currentPage = pagerView.currentIndex
    }
    
    public func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        _pagerControl.currentPage = targetIndex
    }
}
