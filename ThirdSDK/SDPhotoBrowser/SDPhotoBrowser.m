//
//  SDPhotoBrowser.m
//  photobrowser
//
//  Created by aier on 15-2-3.
//  Copyright (c) 2015年 aier. All rights reserved.
//

#import "SDPhotoBrowser.h"
#import "SDBrowserImageView.h"

#import <LibBaseSDK/UIImage+MLCustom.h>
#import <LibBaseSDK/LibBaseSDK-Swift.h>
#import "UIImage+UidesignBundle.h"

 
//  ============在这里方便配置样式相关设置===========

//                      ||
//                      ||
//                      ||
//                     \\//
//                      \/

#import "SDPhotoBrowserConfig.h"

//  =============================================

@implementation SDPhotoBrowser 
{
    UIScrollView *_scrollView;
    BOOL _hasShowedFistView;
    UILabel *_indexLabel;

    UIButton *_butDel;
    UIButton *_butClose;
    
    UIActivityIndicatorView *_indicatorView;
    BOOL _willDisappear;
    
    UIView *_longPressView;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = SDPhotoBrowserBackgrounColor;
    }
    return self;
}


- (void)didMoveToSuperview {
    [self setupScrollView];
    [self setupToolbars];
}

- (void)dealloc {
    [[UIApplication sharedApplication].keyWindow removeObserver:self forKeyPath:@"frame"];
}

- (void)setupToolbars {
    // 1. 序标
    UILabel *indexLabel = [[UILabel alloc] init];
    indexLabel.bounds = CGRectMake(0, 0, 80, 30);
    indexLabel.textAlignment = NSTextAlignmentCenter;
    indexLabel.textColor = [UIColor whiteColor];
    indexLabel.font = [UIFont boldSystemFontOfSize:20];
    indexLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    indexLabel.layer.cornerRadius = indexLabel.bounds.size.height * 0.5;
    indexLabel.clipsToBounds = YES;
    if (self.imageCount > 1) {
        indexLabel.text = [NSString stringWithFormat:@"1/%ld", (long)self.imageCount];
        _indexLabel = indexLabel;
        [self addSubview:indexLabel];
    }

    
    // 2.删除按钮
    UIButton *butDel = [[UIButton alloc] init];
    [butDel setTitle:@"删除" forState:UIControlStateNormal];
    [butDel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    butDel.titleLabel.font = [UIFont systemFontOfBaseSize:18];
    [butDel addTarget:self action:@selector(delImage) forControlEvents:UIControlEventTouchUpInside];
    _butDel = butDel;
    [self addSubview:butDel];
    [_butDel setHidden: !_isDel];
    
    // 3.关闭按钮
    UIImage *img = [[UIImage imageUidesignNamed: @"nav_close_normal"] tintWithColor: [UIColor whiteColor]];
    UIButton *butClose = [[UIButton alloc] init];
    [butClose setImage: img forState: UIControlStateNormal];
    [butClose addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    _butClose = butClose;
    [self addSubview: butClose];
}

- (void)setupScrollView {
    _scrollView = [[UIScrollView alloc] init];
    _scrollView.delegate = self;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.pagingEnabled = YES;
    [self addSubview:_scrollView];
    
    [self refreah];
}

- (void) refreah {
    if (self.imageCount < 1) {
        [self close];
        return;
    }
    
    [_scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    for (int i = 0; i < self.imageCount; i++) {
        SDBrowserImageView *imageView = [[SDBrowserImageView alloc] init];
        imageView.tag = i;
        
        // 双击放大图片
        UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewDoubleTaped:)];
        doubleTap.numberOfTapsRequired = 2;
        
        [imageView addGestureRecognizer:doubleTap];
        [_scrollView addSubview:imageView];
    }
    
    [self layoutIfNeeded];
    [self setNeedsLayout];
    [self scrollViewDidScroll:_scrollView];
    
    [self setupImageOfImageViewForIndex: self.currentImageIndex];
}

// 加载图片
- (void)setupImageOfImageViewForIndex:(NSInteger)index {
    if (self.imageCount <= index) { index--; }
    
    if (!_willDisappear) {
        _indexLabel.text = [NSString stringWithFormat:@"%d/%ld", index + 1, (long)self.imageCount];
    }

    SDBrowserImageView *imageView = _scrollView.subviews[index];
    self.currentImageIndex = index;
    if (imageView.hasLoadedImage) return;
    imageView.image = [self placeholderImageForIndex:index];
    imageView.hasLoadedImage = YES;
}

#pragma mark -
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    UIView * bgView = (UIView *)[_longPressView viewWithTag:200];
    if (touch.view == bgView) {
        return  NO;
    }
    return  YES;
}

#pragma mark -
- (void) close {
    [UIView animateWithDuration:SDPhotoBrowserHideImageAnimationDuration animations:^{
//        self.backgroundColor = [UIColor clearColor];
//        self->_indexLabel.backgroundColor = [UIColor clearColor];
//        self->_butDel.backgroundColor = [UIColor clearColor];
//        self->_butClose.backgroundColor = [UIColor clearColor];
//        self->_scrollView.backgroundColor = [UIColor clearColor];
//        self->_longPressView.backgroundColor = [UIColor clearColor];
//
        self.alpha = 0.1;
        self->_indexLabel.alpha = 0.1;
        self->_butDel.alpha = 0.1;
        self->_butClose.alpha = 0.1;
        self->_scrollView.alpha = 0.1;
        self->_longPressView.alpha = 0.1;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void) delImage {
    if ([self.delegate respondsToSelector:@selector(photoBrowser:delImageForIndex:)]) {
        int index = (_scrollView.contentOffset.x + _scrollView.bounds.size.width * 0.5) / _scrollView.bounds.size.width;
        return [self.delegate photoBrowser:self delImageForIndex:index];
    }
}


- (void)imageViewDoubleTaped:(UITapGestureRecognizer *)recognizer {
    SDBrowserImageView *imageView = (SDBrowserImageView *)recognizer.view;
    CGFloat scale;
    if (imageView.isScaled) {
        scale = 1.0;
    } else {
        scale = 2.0;
    }
    
    SDBrowserImageView *view = (SDBrowserImageView *)recognizer.view;

    [view doubleTapToZommWithScale:scale];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect rect = self.bounds;
    rect.size.width += SDPhotoBrowserImageViewMargin * 2;
    
    _scrollView.bounds = rect;
    _scrollView.center = self.center;
    
    CGFloat y = 0;
    CGFloat w = _scrollView.frame.size.width - SDPhotoBrowserImageViewMargin * 2;
    CGFloat h = _scrollView.frame.size.height;
    
    
    
    [_scrollView.subviews enumerateObjectsUsingBlock:^(SDBrowserImageView *obj, NSUInteger idx, BOOL *stop) {
        CGFloat x = SDPhotoBrowserImageViewMargin + idx * (SDPhotoBrowserImageViewMargin * 2 + w);
        obj.frame = CGRectMake(x, y, w, h);
    }];
    
    _scrollView.contentSize = CGSizeMake(_scrollView.subviews.count * _scrollView.frame.size.width, 0);
    _scrollView.contentOffset = CGPointMake(self.currentImageIndex * _scrollView.frame.size.width, 0);
    
    
    if (!_hasShowedFistView) {
        [self showFirstImage];
    }
    
    _indexLabel.center = CGPointMake(self.bounds.size.width * 0.5, 55);
    _butDel.frame = CGRectMake(12, 34, 50, 30);
    _butClose.frame = CGRectMake(self.bounds.size.width - 44, 34, 30, 30);
}

- (void)show {
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    self.frame = window.bounds;
    [window addObserver:self forKeyPath:@"frame" options:0 context:nil];
    [window addSubview:self];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(UIView *)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"frame"]) {
        self.frame = object.bounds;
        SDBrowserImageView *currentImageView = _scrollView.subviews[_currentImageIndex];
        if ([currentImageView isKindOfClass:[SDBrowserImageView class]]) {
            [currentImageView clear];
        }
    }
}

- (void)showFirstImage {
    UIView *sourceView = self.sourceImagesContainerView.subviews[self.currentImageIndex];
    CGRect rect = [self.sourceImagesContainerView convertRect:sourceView.frame toView:self];
    
    UIImageView *tempView = [[UIImageView alloc] init];
    tempView.image = [self placeholderImageForIndex:self.currentImageIndex];
    
    [self addSubview:tempView];
    
    CGRect targetTemp = [_scrollView.subviews[self.currentImageIndex] bounds];
    
    tempView.frame = rect;
    tempView.contentMode = [_scrollView.subviews[self.currentImageIndex] contentMode];
    _scrollView.hidden = YES;
    
    
    [UIView animateWithDuration:SDPhotoBrowserShowImageAnimationDuration animations:^{
        tempView.center = self.center;
        tempView.bounds = (CGRect){CGPointZero, targetTemp.size};
    } completion:^(BOOL finished) {
        self->_hasShowedFistView = YES;
        [tempView removeFromSuperview];
        self->_scrollView.hidden = NO;
    }];
}

- (UIImage *)placeholderImageForIndex:(NSInteger)index {
    if ([self.delegate respondsToSelector:@selector(photoBrowser:placeholderImageForIndex:)]) {
        return [self.delegate photoBrowser:self placeholderImageForIndex:index];
    }
    return nil;
}

#pragma mark - scrollview代理方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    int index = (scrollView.contentOffset.x + _scrollView.bounds.size.width * 0.5) / _scrollView.bounds.size.width;
    [self setupImageOfImageViewForIndex:index];
}

@end
