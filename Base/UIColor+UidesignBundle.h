//
//  UIColor+AuthBundle.h
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (UidesignBundle)

+ (UIColor *) colorUidesignNamed:(NSString *) name;

@end

NS_ASSUME_NONNULL_END
