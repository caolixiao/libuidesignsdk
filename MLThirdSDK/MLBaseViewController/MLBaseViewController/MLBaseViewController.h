//
//  MLBaseViewController.h
//

#import <UIKit/UIKit.h>

@class MLNavViewBar;
@interface MLBaseViewController : UIViewController {
    @package
    NSString *identifier_cell;
    
    @private
    BOOL isScrollTo;
}

/**
 *  @brief 设置默认
 */
- (void) addTitle:(NSString *) title;
- (void) setShowTitle:(NSString *) title;
- (void) addleftAndTitle:(NSString *) title;

- (void) addLeftV:(UIView *) view;
- (void) addRightV:(UIView *) view;
- (void) addCenterV:(UIView *)view;

/**
 *  @brief 设置默认
 */
- (void) backItemButton:(UIButton *) button;


#pragma mark - set child vc
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) NSArray *viewControllers;
@property (nonatomic, strong) UIViewController *selectedViewController;
@property (nonatomic) NSInteger selectedIndex;


#pragma mark - set left right vc 
@property (nonatomic, strong) MLNavViewBar *vNavBar;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSArray *vcs;
@property (nonatomic, assign, setter=selected:) int selected;

#pragma mark - set status bar
//设置状态栏颜色
- (void) setStatusBarBackgroundColor:(UIColor *) color;

@end
