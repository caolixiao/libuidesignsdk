//
//  MLTextView.swift
//

import UIKit
import LibBaseSDK

open class MLTextView: UITextView, UITextViewDelegate {
    
    deinit {
        Log.i("deinit MLTextView")
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        delegate = self
    }
    
    override open func caretRect(for position: UITextPosition) -> CGRect {
        var rect = super.caretRect(for: position)
        
//        rect.origin.y = rect.origin.y + 8
//        rect.size.height = (self.font?.lineHeight ?? 0) + 8
//        rect.size.width = 6
        
        return rect
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        sizeToPlaceholder()
    }
    
    // MARK: -
//    public func textViewDidChange(_ textView: UITextView) {
//        if let selectedRange = textView.markedTextRange, let strNew = textView.text(in: selectedRange) {
//            if strNew.count > 0 { return }
//        }
//        attributedText = NSAttributedString.setTitle(height: 8, str: textView.text)
//    }
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        if let tableView = getSuperView(to: UITableView.self, view: self) as? UITableView,
            let cell = getSuperView(to: UITableViewCell.self, view: self) as? UITableViewCell {
            let indexPath = tableView.indexPath(for: cell)
            tableView.scrollToRow(at: indexPath!, at: .none, animated: true)
        }
    }
}

