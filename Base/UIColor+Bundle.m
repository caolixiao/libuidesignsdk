//
//  UIColor+boundle.m
//

#import "UIColor+Bundle.h"

@implementation UIColor (Bundle)

+ (UIColor *) initWithBundleName:(NSString *)boudleName colorName:(NSString *) colorName class:(Class) aClass {
    NSBundle *bundle = [NSBundle bundleForClass:aClass];
    NSURL *url = [bundle URLForResource:boudleName withExtension:@"bundle"];
    NSBundle *targetBundle = [NSBundle bundleWithURL:url];

    UIColor *color = [UIColor colorNamed: colorName inBundle: targetBundle compatibleWithTraitCollection:nil];
    return color;
}

@end
