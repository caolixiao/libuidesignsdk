//
//  MLQRView.h
//

#import <UIKit/UIKit.h>


@interface MLQRView : UIView

@property (nonatomic, assign) CGFloat hqr;              // 扫描区域的到头的高度 默认 100
@property (nonatomic, assign) CGSize sTArea;            // 透明的区域 大小 transparentArea
@property (nonatomic, assign, readonly) CGRect rCrop;   // 透明区域的位置

// 让线动起来
- (void) reloadQRLine;
- (void) stopQRLine;

@end
