//
//  MLDrawModel.h
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, MLDrawingStatus) {
    MLDrawingStatusBegin, //准备绘制
    MLDrawingStatusMove,  //正在绘制
    MLDrawingStatusEnd    //结束绘制
};

typedef NS_ENUM(NSInteger, MLDrawingShapeType) {
    MLDrawingShapeCurve = 0, //曲线
    MLDrawingShapeLine,      //直线
    MLDrawingShapeEllipse,   //椭圆
    MLDrawingShapeRect,      //矩形
};



UIKIT_EXTERN NSString * const CollectionImageBoardViewID;
UIKIT_EXTERN NSString * const ImageBoardNotification;
UIKIT_EXTERN NSString * const SendColorAndWidthNotification;

/**改变背景**/
UIKIT_EXTERN NSString * const Action_changebackground;
/**画**/
UIKIT_EXTERN NSString * const Action_playing;
/**回退**/
UIKIT_EXTERN NSString * const Action_return;
/**向前**/
UIKIT_EXTERN NSString * const Action_goforward;
/**清屏**/
UIKIT_EXTERN NSString * const Action_clearAll;



@interface MLDrawModel : NSObject

@property (nonatomic, strong) NSArray * pointList; // cgpoint
@property (nonatomic, copy) NSString * paintColor;
@property (nonatomic, copy) NSString * background;
@property (nonatomic, copy) NSString * action;
@property (nonatomic, strong) NSNumber * paintSize;
@property (nonatomic, strong) NSNumber * width;
@property (nonatomic, strong) NSNumber * height;
@property (nonatomic, strong) NSNumber * isEraser;
@property (nonatomic, strong) NSNumber * shapType;

@end



@interface MLPath : NSObject

@property (nonatomic, assign) MLDrawingShapeType type; // 绘制样式
@property (nonatomic, assign) CGFloat fWLine;          // 线宽
@property (nonatomic, assign) BOOL isEraser;           // 橡皮擦
@property (nonatomic, assign, readonly) CGPoint bPoint;          // 开始点

@property (nonatomic, strong) UIColor *colorPan;       // 画笔颜色
@property (nonatomic, strong) UIBezierPath *bp;

@property (nonatomic, strong) NSString *imgPath;       // 生产图片路径

- (instancetype) initWithPoint:(CGPoint) beginPoint;
- (void) pathToPoint:(CGPoint) movePoint type:(MLDrawingShapeType) type;

@end


@interface MLDrawView : UIView

- (void) setBrush:(MLPath *) path;

@end

