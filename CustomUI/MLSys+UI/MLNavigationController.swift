//
//  MLNavigationController.swift
//

import UIKit
import LibBaseSDK

open class MLNavigationController: UINavigationController, UINavigationControllerDelegate {
    
    private var _delegate: UIGestureRecognizerDelegate?
    
    override open func viewDidLoad() {
        _delegate = interactivePopGestureRecognizer?.delegate
        super.viewDidLoad()
        
        delegate = self
        navigationBar.setNavBar(bkgColor: color_nav_bg, lineColor: color_nav_line, wordColor: color_nav_title)
    }
    
    
    // MARK: - UINavigationControllerDelegate
     open func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        interactivePopGestureRecognizer?.delegate = viewController == viewControllers[0] ? _delegate : nil
    }
    
    
    override open var shouldAutorotate: Bool {
        return self.viewControllers.last?.shouldAutorotate ?? false
    }
    
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return self.viewControllers.last?.supportedInterfaceOrientations ?? .portrait
    }
    
    override open var preferredInterfaceOrientationForPresentation : UIInterfaceOrientation {
        return self.viewControllers.last?.preferredInterfaceOrientationForPresentation ?? .portrait
    }


    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return self.topViewController?.preferredStatusBarStyle ?? .default
    }
}
