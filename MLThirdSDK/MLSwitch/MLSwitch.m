//
//  MLSwitch.m
//

#import "MLSwitch.h"
#import <MLGeneralLibary/MLGlobalHeader.h>
#import <LibBaseSDK/LibBaseSDK-Swift.h>

@interface MLSwitch ()

@property(nonatomic,strong) UIView * vSelected;
@property(nonatomic,strong) UILabel * labLeft;
@property(nonatomic,strong) UILabel * labRight;

@property(nonatomic,strong) UITapGestureRecognizer * tapGes;
@property(nonatomic,assign) BOOL isOn;

@end


@implementation MLSwitch

- (instancetype)init {
    self = [super init];
    if (self) {
        self.isOn = NO;
        [self buildBaseView];
    }
    return self;
}


- (void) layoutSubviews {
    [super layoutSubviews];
    self.layer.cornerRadius = VIEWH(self)*0.5;
    self.vSelected.layer.cornerRadius = VIEWH(self)*0.5 - 2;
    
    self.labLeft.frame = CGRectMake(0, 0, VIEWW(self)/2.0, VIEWH(self));
    self.labRight.frame = CGRectMake(VIEWMaxX(self.labLeft), 0, VIEWW(self)/2.0, VIEWH(self));
    
    [self stateOff];
}

- (void) buildBaseView {
    self.layer.masksToBounds = YES;
    self.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.2];
    
    [self addSubview:self.vSelected];
    [self addSubview:self.labRight];
    [self addSubview:self.labLeft];
    
    [self addGestureRecognizer:self.tapGes];
}



//关闭状态的UI
- (void)stateOff {
    CGFloat x,y,w,h;
    
    x = 1;
    y = 1;
    w = VIEWW(self)/2.0 - 2;
    h = self.frame.size.height - 2;
    self.vSelected.frame = CGRectMake(x, y, w, h);
    
    _labLeft.textColor = COLOR_GENERAL;
    _labRight.textColor = __RGB(0xffffff);//
}


//打开状态的UI
- (void)stateOn {
    
    CGFloat x,y,w,h;
    x = self.frame.size.width - self.vSelected.frame.size.width - 1;
    y = 1;
    w = VIEWW(self)/2.0 - 2;
    h = self.frame.size.height - 2;
    self.vSelected.frame = CGRectMake(x, y, w, h);
    
    _labRight.textColor = COLOR_GENERAL;
    _labLeft.textColor = __RGB(0xffffff);//__RGB(0x68c4df);
}


//改变开关状态
- (void)change {
    
    __weak MLSwitch * weakSelf = self;
    if (self.isOn) {
        self.isOn = NO;
        if (self.switchChangeStateBlock) self.switchChangeStateBlock(self.isOn);
        [UIView animateWithDuration:0 animations:^{
            [weakSelf stateOff];
        }];
    }
    else {
        self.isOn = YES;
        if (self.switchChangeStateBlock) self.switchChangeStateBlock(self.isOn);
        [UIView animateWithDuration:0 animations:^{
            [weakSelf stateOn];
        }];
    }
}

#pragma mark - 懒加载
- (UILabel *) labLeft {
    if (!_labLeft) {
        _labLeft = [[UILabel alloc] init];
        _labRight.textColor = __RGB(0x68c4df);
        _labLeft.font = [UIFont systemFontOfBaseSize:15];
        _labLeft.textAlignment = NSTextAlignmentCenter;
        _labLeft.backgroundColor = [UIColor clearColor];
    }
    
    _labLeft.text = _left;
    return _labLeft;
}

- (UILabel *) labRight {
    if (!_labRight) {
        _labRight = [[UILabel alloc] init];
        _labRight.textColor = __RGB(0x68c4df);
        _labRight.font = [UIFont systemFontOfBaseSize:15];
        _labRight.textAlignment = NSTextAlignmentCenter;
        _labRight.backgroundColor = [UIColor clearColor];
    }
    
    _labRight.text = _right;
    return _labRight;
}


- (UIView *) vSelected {
    if (!_vSelected) {
        _vSelected = [[UIView alloc] init];
        _vSelected.backgroundColor = [UIColor whiteColor];
        _vSelected.layer.masksToBounds = true;
        _vSelected.layer.cornerRadius = CGScale(33);
    }
    return _vSelected;
}

- (UITapGestureRecognizer *) tapGes {
    if (!_tapGes) _tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(change)];
    return _tapGes;
}

#pragma mark -
- (void) setRight:(NSString *)right {
    _right = right;
    if (_labRight) _labRight.text = right;
}

- (void) setLeft:(NSString *)left {
    _left = left;
    if (_labLeft) _labLeft.text = left;
}

@end
