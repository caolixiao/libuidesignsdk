//
//  MLNotice.swift
//

import Foundation
import UIKit
import LibBaseSDK


extension NSObject {
    static public func clearAllNotice() {
        MLNotice.clear()
    }
    
    static public func pleaseWait(text msg: String) {
        MLNotice.clear()
        MLNotice.waitWithText(msg)
    }
    
    static public func pleaseWait(images imageNames: Array<UIImage>, timeInterval: Int) {
        MLNotice.clear()
        MLNotice.wait(imageNames, timeInterval: timeInterval)
    }
    
    static public func pleaseWait() {
        MLNotice.clear()
        MLNotice.wait()
    }
    
    static public func noticeOnlyText(_ text: String?, autoClear: Bool = true, autoClearTime: Int = 2) {
        guard let msg = text else { return }
        MLNotice.clear()
        MLNotice.showText(msg, autoClear: autoClear, autoClearTime: autoClearTime)
    }
    
    static public func notice(_ text: String, type: NoticeType = .success, autoClear: Bool = true, autoClearTime: Int = 2) {
        MLNotice.clear()
        MLNotice.showNoticeWithText(type, text: text, autoClear: autoClear, autoClearTime: autoClearTime)
    }
    
    static public func noticeTop(_ text: String, autoClear: Bool = true, autoClearTime: Int = 2) {
        MLNotice.clear()
        MLNotice.noticeOnStatusBar(text, autoClear: autoClear, autoClearTime: autoClearTime)
    }
}

extension NSObject {
    public func clearAllNotice() {
        MLNotice.clear()
    }
    
    public func pleaseWait(text msg: String) {
        MLNotice.clear()
        MLNotice.waitWithText(msg)
    }
    
    public func pleaseWait(images imageNames: Array<UIImage>, timeInterval: Int) {
        MLNotice.clear()
        MLNotice.wait(imageNames, timeInterval: timeInterval)
    }
    
    public func pleaseWait() {
        MLNotice.clear()
        MLNotice.wait()
    }
    
    public func noticeOnlyText(_ text: String?, autoClear: Bool = true, autoClearTime: Int = 2) {
        guard let msg = text else { return }
        MLNotice.clear()
        MLNotice.showText(msg, autoClear: autoClear, autoClearTime: autoClearTime)
    }
    
    public func notice(_ text: String, type: NoticeType = .success, autoClear: Bool = true, autoClearTime: Int = 2) {
        MLNotice.clear()
        MLNotice.showNoticeWithText(type, text: text, autoClear: autoClear, autoClearTime: autoClearTime)
    }
    
    public func noticeTop(_ text: String, autoClear: Bool = true, autoClearTime: Int = 2) {
        MLNotice.clear()
        MLNotice.noticeOnStatusBar(text, autoClear: autoClear, autoClearTime: autoClearTime)
    }
}


class MLWindowAllTouch: UIWindow {
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        return nil
    }
}

public enum NoticeType {
    case success
    case error
    case info
}

public class MLNotice: NSObject {
    
    static var windows = Array<UIWindow?>()
    static var timer: DispatchSource? = nil
    static var timerTimes = 0
    static var degree: Double {
        get {
            let d = [0, 0, 180, 270, 90][UIApplication.shared.statusBarOrientation.rawValue] as Double
            return d
        }
    }
    static var center: CGPoint {
        get {
            var array = [UIScreen.main.bounds.width, UIScreen.main.bounds.height]
            array = array.sorted(by: <)
            let screenWidth = array[0]
            let screenHeight = array[1]
            let x = [0, screenWidth/2, screenWidth/2, 10, screenWidth-10][UIApplication.shared.statusBarOrientation.rawValue] as CGFloat
            let y = [0, 10, screenHeight-10, screenHeight/2, screenHeight/2][UIApplication.shared.statusBarOrientation.rawValue] as CGFloat
            return CGPoint(x: x, y: y)
        }
    }
    
    // fix https://github.com/johnlui/MLNotice/issues/2
    // thanks broccolii(https://github.com/broccolii) and his PR https://github.com/johnlui/MLNotice/pull/5
    static public func clear() {
        self.cancelPreviousPerformRequests(withTarget: self)
        if let _ = timer {
            timer?.cancel()
            timer = nil
            timerTimes = 0
        }
        for  window  in windows
        {
            if(window == nil)
            {
                break;
            }
            window?.rootViewController=nil
            if(window?.subviews == nil)
            {
                continue
            }
            for sub in (window?.subviews)!
            {
                sub.removeFromSuperview()
            }
        }
        windows.removeAll(keepingCapacity: false)
    }
    
    static public func noticeOnStatusBar(_ text: String, autoClear: Bool, autoClearTime: Int) {
        let frame = UIApplication.shared.statusBarFrame
        let window = UIWindow()
        window.rootViewController = UIViewController.init(nibName: nil, bundle: nil)
        window.rootViewController?.view.backgroundColor = UIColor.clear
        window.backgroundColor = UIColor.clear
        let view = UIView()
        view.backgroundColor = UIColor(red: 0x6a/0x100, green: 0xb4/0x100, blue: 0x9f/0x100, alpha: 1)
        
        let label = UILabel(frame: frame)
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont.systemFont(ofBaseSize: 12)
        label.textColor = UIColor.white
        label.text = text
        view.addSubview(label)
        
        window.frame = UIScreen.main.bounds
        view.frame = frame
        
        window.windowLevel = UIWindow.Level.statusBar
        window.isHidden = false
        // change orientation
        window.center = center
        window.transform = CGAffineTransform(rotationAngle: CGFloat(degree * Double.pi / 180))
        window.addSubview(view)
        windows.append(window)
        if autoClear {
            let selector = #selector(MLNotice.hideNotice(_:))
            self.perform(selector, with: window, afterDelay: TimeInterval(autoClearTime))
        }
    }
    
    static public func waitWithText(_ msg: String = "加载中") {
        let frame = CGRect(x: 0, y: 0, width: 78, height: 78)
        let window = UIWindow()
        window.rootViewController=UIViewController.init(nibName: nil, bundle: nil)
        window.rootViewController?.view.backgroundColor=UIColor.clear
        window.backgroundColor = UIColor.clear
        let mainView = UIView()
        mainView.layer.cornerRadius = 12
        mainView.backgroundColor = UIColor(red:0, green:0, blue:0, alpha: 0.8)
        
        
        let ai = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        ai.frame = CGRect(x: 21, y: 21, width: 36, height: 36)
        ai.startAnimating()
        mainView.addSubview(ai)
        
        let label = UILabel()
        label.text = msg
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofBaseSize: 13)
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor.white
        label.sizeToFit()
        mainView.addSubview(label)
        
        let wid = label.frame.height + 91
        let superFrame = CGRect(x: 0, y: 0, width: wid , height: wid)
        window.frame = UIScreen.main.bounds
        mainView.frame = superFrame
        
        ai.center.x = mainView.center.x
        label.center = CGPoint.init(x: mainView.center.x, y: ai.center.y+36) //mainView.center
        
        window.windowLevel = UIWindow.Level.alert
        mainView.center = getRealCenter()
        // change orientation
        window.transform = CGAffineTransform(rotationAngle: CGFloat(degree * Double.pi / 180))
        window.isHidden = false
        window.addSubview(mainView)
        windows.append(window)
    }
    
    static public func wait(_ imageNames: Array<UIImage> = Array<UIImage>(), timeInterval: Int = 0) {
        let frame = CGRect(x: 0, y: 0, width: 78, height: 78)
        let window = UIWindow()
        window.rootViewController=UIViewController.init(nibName: nil, bundle: nil)
        window.rootViewController?.view.backgroundColor=UIColor.clear
        window.backgroundColor = UIColor.clear
        let mainView = UIView()
        mainView.layer.cornerRadius = 12
        mainView.backgroundColor = UIColor(red:0, green:0, blue:0, alpha: 0.8)
        
        if imageNames.count > 0 {
            if imageNames.count > timerTimes {
                let iv = UIImageView(frame: frame)
                iv.image = imageNames.first!
                iv.contentMode = UIView.ContentMode.scaleAspectFit
                mainView.addSubview(iv)
                timer = DispatchSource.makeTimerSource(flags: DispatchSource.TimerFlags(rawValue: 0), queue: DispatchQueue.main) /*Migrator FIXME: Use DispatchSourceTimer to avoid the cast*/ as? DispatchSource
                //                timer?.setTimer(start: DispatchTime.now(), interval: UInt64(timeInterval) * NSEC_PER_MSEC, leeway: 0)
                timer?.setEventHandler(handler: { () -> Void in
                    let name = imageNames[timerTimes % imageNames.count]
                    iv.image = name
                    timerTimes += 1
                })
                timer?.resume()
            }
        } else {
            let ai = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
            ai.frame = CGRect(x: 21, y: 21, width: 36, height: 36)
            ai.startAnimating()
            mainView.addSubview(ai)
        }
        
        window.frame = UIScreen.main.bounds
        mainView.frame = frame
        
        window.windowLevel = UIWindow.Level.alert
        mainView.center = getRealCenter()
        // change orientation
        window.transform = CGAffineTransform(rotationAngle: CGFloat(degree * Double.pi / 180))
        window.isHidden = false
        window.addSubview(mainView)
        windows.append(window)
    }
    
    static public func showText(_ text: String, autoClear: Bool, autoClearTime: Int) {
        let window = MLWindowAllTouch()
        window.rootViewController = UIViewController.init(nibName: nil, bundle: nil)
        window.rootViewController?.view.backgroundColor = .clear
        window.backgroundColor = UIColor.clear
        window.frame = UIScreen.main.bounds
        window.windowLevel = UIWindow.Level.alert
        
        let mainView = UIView()
        mainView.layer.cornerRadius = 10
        mainView.backgroundColor = UIColor(red:0, green:0, blue:0, alpha: 0.7)
        
        let label = UILabel()
        label.text = text
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofBaseSize: 13)
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor.white
        label.sizeToFit()
        mainView.addSubview(label)
        
        let superFrame = CGRect(x: 0, y: 0, width: label.frame.width + 50 , height: label.frame.height + 30)
        mainView.frame = superFrame
        label.center = mainView.center
        
        var center = getRealCenter()
        center.y = center.y + (__iPhoneX ? 220 : 200)
        mainView.center = center
        // change orientation
        window.transform = CGAffineTransform(rotationAngle: CGFloat(degree * Double.pi / 180))
        window.isHidden = false
        window.addSubview(mainView)
        windows.append(window)
        
        if autoClear {
            let selector = #selector(MLNotice.hideNotice(_:))
            self.perform(selector, with: window, afterDelay: TimeInterval(autoClearTime))
        }
    }
    
    static public func showNoticeWithText(_ type: NoticeType, text: String, autoClear: Bool, autoClearTime: Int) {
        let frame = CGRect(x: 0, y: 0, width: 90, height: 90)
        let window = UIWindow()
        window.frame = UIScreen.main.bounds
        window.rootViewController = UIViewController()
        window.rootViewController?.view.backgroundColor = UIColor.clear
        window.backgroundColor = UIColor.clear
        window.transform = CGAffineTransform(rotationAngle: CGFloat(degree * Double.pi / 180))
        window.windowLevel = UIWindow.Level.alert
        window.isHidden = false
        
        let mainView = UIView(frame: frame)
        mainView.layer.cornerRadius = 10
        mainView.backgroundColor = UIColor(red:0, green:0, blue:0, alpha: 0.7)
        
        var image = UIImage()
        switch type {
        case .success:
            image = MLNoticeSDK.imageOfCheckmark
        case .error:
            image = MLNoticeSDK.imageOfCross
        case .info:
            image = MLNoticeSDK.imageOfInfo
        }
        let checkmarkView = UIImageView(image: image)
        checkmarkView.frame = CGRect(x: 27, y: 15, width: 36, height: 36)
        mainView.addSubview(checkmarkView)
        
        let label = UILabel(frame: CGRect(x: 0, y: 60, width: 90, height: 16))
        label.font = UIFont.systemFont(ofBaseSize: 13)
        label.textColor = UIColor.white
        label.text = text
        label.textAlignment = NSTextAlignment.center
        mainView.addSubview(label)
        

        mainView.center = getRealCenter()
        // change orientation

        window.addSubview(mainView)
        windows.append(window)
        
        if autoClear {
            let selector = #selector(MLNotice.hideNotice(_:))
            self.perform(selector, with: window, afterDelay: TimeInterval(autoClearTime))
        }
    }
    
    // fix https://github.com/johnlui/MLNotice/issues/2
    @objc
    static private func hideNotice(_ sender: AnyObject) {
        if let window = sender as? UIWindow {
            window.rootViewController=nil
            for sub in window.subviews {
                sub.removeFromSuperview()
            }
            if let index = windows.firstIndex(where: { (item) -> Bool in
                return item == window
            }) {
                windows.remove(at: index)
            }
        }
    }
    
    // fix orientation problem
    static private func getRealCenter() -> CGPoint {
        let point = UIApplication.shared.windows.first?.center ?? CGPoint(x: 0.5, y: 0.5)
        if UIApplication.shared.statusBarOrientation.rawValue >= 3 {
            return CGPoint(x: point.y, y: point.x)
        } else {
            return point
        }
    }
}

public class MLNoticeSDK {
    struct Cache {
        static var imageOfCheckmark: UIImage?
        static var imageOfCross: UIImage?
        static var imageOfInfo: UIImage?
    }
    
    class func draw(_ type: NoticeType) {
        let checkmarkShapePath = UIBezierPath()
        
        // draw circle
        checkmarkShapePath.move(to: CGPoint(x: 36, y: 18))
        checkmarkShapePath.addArc(withCenter: CGPoint(x: 18, y: 18), radius: 17.5, startAngle: 0, endAngle: CGFloat(Double.pi*2), clockwise: true)
        checkmarkShapePath.close()
        
        switch type {
        case .success: // draw checkmark
            checkmarkShapePath.move(to: CGPoint(x: 10, y: 18))
            checkmarkShapePath.addLine(to: CGPoint(x: 16, y: 24))
            checkmarkShapePath.addLine(to: CGPoint(x: 27, y: 13))
            checkmarkShapePath.move(to: CGPoint(x: 10, y: 18))
            checkmarkShapePath.close()
        case .error: // draw X
            checkmarkShapePath.move(to: CGPoint(x: 10, y: 10))
            checkmarkShapePath.addLine(to: CGPoint(x: 26, y: 26))
            checkmarkShapePath.move(to: CGPoint(x: 10, y: 26))
            checkmarkShapePath.addLine(to: CGPoint(x: 26, y: 10))
            checkmarkShapePath.move(to: CGPoint(x: 10, y: 10))
            checkmarkShapePath.close()
        case .info:
            checkmarkShapePath.move(to: CGPoint(x: 18, y: 6))
            checkmarkShapePath.addLine(to: CGPoint(x: 18, y: 22))
            checkmarkShapePath.move(to: CGPoint(x: 18, y: 6))
            checkmarkShapePath.close()
            
            UIColor.white.setStroke()
            checkmarkShapePath.stroke()
            
            let checkmarkShapePath = UIBezierPath()
            checkmarkShapePath.move(to: CGPoint(x: 18, y: 27))
            checkmarkShapePath.addArc(withCenter: CGPoint(x: 18, y: 27), radius: 1, startAngle: 0, endAngle: CGFloat(Double.pi*2), clockwise: true)
            checkmarkShapePath.close()
            
            UIColor.white.setFill()
            checkmarkShapePath.fill()
        }
        
        UIColor.white.setStroke()
        checkmarkShapePath.stroke()
    }
    class var imageOfCheckmark: UIImage {
        if (Cache.imageOfCheckmark != nil) {
            return Cache.imageOfCheckmark!
        }
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 36, height: 36), false, 0)
        
        MLNoticeSDK.draw(NoticeType.success)
        
        Cache.imageOfCheckmark = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return Cache.imageOfCheckmark!
    }
    class var imageOfCross: UIImage {
        if (Cache.imageOfCross != nil) {
            return Cache.imageOfCross!
        }
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 36, height: 36), false, 0)
        
        MLNoticeSDK.draw(NoticeType.error)
        
        Cache.imageOfCross = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return Cache.imageOfCross!
    }
    class var imageOfInfo: UIImage {
        if (Cache.imageOfInfo != nil) {
            return Cache.imageOfInfo!
        }
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 36, height: 36), false, 0)
        
        MLNoticeSDK.draw(NoticeType.info)
        
        Cache.imageOfInfo = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return Cache.imageOfInfo!
    }
}
