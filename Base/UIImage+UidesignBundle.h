//
//  UIImage+AuthBundle.h
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (UidesignBundle)

+ (UIImage *) imageUidesignNamed:(NSString *)name;

@end

NS_ASSUME_NONNULL_END



