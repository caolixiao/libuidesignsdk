//
//  MLBaseViewController.m
//

#import "MLBaseViewController.h"
#import "UINavigationController+MLExpand.h"
#import "MLNavViewBar.h"


@interface MLBaseViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, MLNavViewBarDelegate>

@end

@implementation MLBaseViewController

- (void) dealloc
{
    INFO_NSLOG(@"dealloc MLBaseViewController");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype) init {
    if (self = [super init]) {
        self.automaticallyAdjustsScrollViewInsets = false;
        self.edgesForExtendedLayout = UIRectEdgeAll;
        
        [self.navigationController.navigationBar setDefaultBackground];
        [self.navigationItem setDelegate:self];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = COLOR_BKG;
    
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
- (void) addTitle:(NSString *) title {
    [self.navigationItem addTitle:title];
}

- (void) setShowTitle:(NSString *) title {
    [self.navigationItem modTitle:title];
}

- (void) addleftAndTitle:(NSString *) title
{
    [self.navigationItem modTitle:title];
    [self.navigationItem setLeftImg:@"back"];
//    [self.navigationItem setLeftImg:nil title:@"取消" color:[UIColor whiteColor]];
}

- (void) addLeftV:(UIView *) view {
    [self.navigationItem setLeftView:view];
}

- (void) addRightV:(UIView *) view
{
    [self.navigationItem addTitle:nil];
    [self.navigationItem setRightView:view];
}

-(void)addCenterV:(UIView *)view{
    [self.navigationItem setLeftImg:@"back"];
//    [self.navigationItem setLeftImg:@"nav-back" title:@"取消" color:[UIColor whiteColor]];
    self.navigationItem.titleView = view;
}


#pragma mark - super action
- (void) backItemButton:(UIButton *) button {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - set child vc
- (UIView *) contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
        _contentView.frame = self.view.bounds;
        [_contentView setBackgroundColor:[UIColor clearColor]];
        [_contentView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight)];
    }
    return _contentView;
}

#pragma mark - view controller
- (UIViewController *)selectedViewController {
    return _selectedViewController = [_viewControllers objectAtIndex:_selectedIndex];
}

- (void)setSelectedIndex:(NSInteger)selectedIndex {
    if (selectedIndex >= _viewControllers.count) return;
    _selectedIndex = selectedIndex;
    
    if (_selectedViewController) {
        [_selectedViewController willMoveToParentViewController:nil];
        [_selectedViewController.view removeFromSuperview];
        [_selectedViewController removeFromParentViewController];
    }
    
    self.selectedViewController = [_viewControllers objectAtIndex:selectedIndex];
    [self addChildViewController:_selectedViewController];
    _selectedViewController.view.frame = [self contentView].bounds;
    [[self contentView] addSubview:_selectedViewController.view];
    [_selectedViewController didMoveToParentViewController:self];
}

- (void) setViewControllers:(NSMutableArray *) viewControllers {
    if (_viewControllers && _viewControllers.count) {
        for (UIViewController *viewController in _viewControllers) {
            [viewController willMoveToParentViewController:nil];
            [viewController.view removeFromSuperview];
            [viewController removeFromParentViewController];
        }
    }
    
    if (viewControllers && [viewControllers isKindOfClass:[NSArray class]]) {
        _viewControllers = [viewControllers copy];
        [self setSelectedIndex:0];
    } else {
        _viewControllers = nil;
    }
}

#pragma mark -
#pragma mark - build view
- (MLNavViewBar *) vNavBar {
    if (_vNavBar) return _vNavBar;
    
    _vNavBar = [[MLNavViewBar alloc] init];
    _vNavBar.delegate = self;
    _vNavBar.titleFont = [UIFont systemFontOfBaseSize:16];
    [_vNavBar setTitles:@[@"1", @"2"] state:MLSelectStateNormal];
    [_vNavBar setTitleColor:COLOR_WORD state:MLSelectStateNormal];
    [_vNavBar setTitleColor:COLOR_GENERAL state:MLSelectStateSelected];
    [_vNavBar showLoadView];
    
    _vNavBar.isShowTopLine = true;
    _vNavBar.isShowBottomLine = true;
    _vNavBar.colorLine = COLOR_LINE;
    
    _vNavBar.colorSelectLine = COLOR_GENERAL;
    _vNavBar.isSelectLine = true;
    
    _vNavBar.fH_Line = CGScale(24);
    _vNavBar.colorSplitLine = COLOR_LINE;
    _vNavBar.isShowSplitLine = true;
    
    return _vNavBar;
}

- (UICollectionView *) collectionView {
    if (_collectionView) return _collectionView;
    
    isScrollTo = false;
    identifier_cell = @"identifier_cell";
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.headerReferenceSize = CGSizeZero;
    flowLayout.minimumLineSpacing = 0;
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.sectionInset = UIEdgeInsetsZero;
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = [UIColor whiteColor];
    _collectionView.pagingEnabled = true;
    _collectionView.alwaysBounceVertical = false;
    _collectionView.alwaysBounceHorizontal = false;
    _collectionView.showsVerticalScrollIndicator = false;
    _collectionView.showsHorizontalScrollIndicator = false;
    
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:identifier_cell];
    
    self.selected = 0;
    return _collectionView;
}


#pragma mark - MLNavViewBarDelegate
- (void)MLNavViewBar:(MLNavViewBar *)target didSelectIndex:(NSInteger)index {
    isScrollTo = true;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [[self collectionView] scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionRight animated:true];
    self.selected = index;
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return collectionView.bounds.size;
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _vcs.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier_cell forIndexPath:indexPath];
    
    UIViewController *vc = _vcs[indexPath.row];
    if (cell.contentView != vc.view.superview) {
        [self addChildViewController:vc];
        [cell.contentView addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    } else {
        [vc.view bringToFront];
    }
    vc.view.frame = cell.bounds;
    return cell;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!isScrollTo) {
        int page = scrollView.contentOffset.x/scrollView.frame.size.width+0.5;
        if (_vNavBar && page != _vNavBar.selectedIndex) _vNavBar.selectedIndex = page;
        if (page != _selected) self.selected = page;
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    isScrollTo = false;
}

- (void) scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    isScrollTo = false;
}


#pragma mark -
//- (BOOL) prefersStatusBarHidden {
//    return false;
//}

- (UIStatusBarStyle) preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void) setStatusBarBackgroundColor:(UIColor *) color {
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) statusBar.backgroundColor = color;
}

#pragma mark -
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return  (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate{
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

@end
