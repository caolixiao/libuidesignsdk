//
//  MLView.swift
//

import UIKit
import LibBaseSDK

open class MLView: UIView {

        
}


extension UIView {
    public func layoutBorderRadius() {
        backgroundColor = color_white
        layer.masksToBounds = true
        layer.borderColor = color_line.cgColor
        layer.borderWidth = 0.5
        layer.cornerRadius = 8
    }
}
