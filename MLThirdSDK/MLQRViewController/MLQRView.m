//
//  MLQRView.m
//


#import "MLQRView.h"
#import <MLGeneralLibary/MLGlobalHeader.h>

@implementation MLQRView
{
    UIImageView *qrLine;
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype) initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
        
        _hqr = CGScale(110.0f);
        _sTArea = CGScaleSize(200, 200);
    }
    return self;
}

- (CGRect) rCrop {
    return (CGRect){{(__SCREEN_SIZE.width - _sTArea.width)/2.0, _hqr}, _sTArea};
}

- (void) drawRect:(CGRect) rect {
    CGRect rCrop = self.rCrop;
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextSetRGBFillColor(ctx, 0/255.0, 0/255.0, 0/255.0, 0.5);
    CGContextFillRect(ctx, __SCREEN_BOUNDS);   //draw the transparent layer

    CGContextClearRect(ctx, rCrop);  //clear the center rect  of the layer
    
    CGContextStrokeRect(ctx, rCrop);
    CGContextSetRGBStrokeColor(ctx, 187.0 /255.0, 93.0f/255.0, 90.0/255.0, 1);
    CGContextSetLineWidth(ctx, 0.5);
    CGContextAddRect(ctx, rCrop);
    CGContextStrokePath(ctx);
    
    [self addCornerLineWithContext:ctx rect:rCrop];
    
    [self addMoveLine:rect];
}

- (void)addCornerLineWithContext:(CGContextRef)ctx rect:(CGRect)rect {
    
    CGFloat angleLong = 15;
    CGFloat angleEdge = 2.0;
    CGFloat ae = angleEdge * 3.0f/2.0f;
    
    //画四个边角
    CGContextSetLineWidth(ctx, 2.5);
    CGContextSetRGBStrokeColor(ctx, 231.0 /255.0, 116.0f/255.0, 113.0/255.0, 1);
    
    //左上角
    CGPoint poinsTopLeftA[] = {
        CGPointMake(rect.origin.x - angleEdge, rect.origin.y - ae),
        CGPointMake(rect.origin.x - angleEdge, rect.origin.y + angleLong)
    };
    
    CGPoint poinsTopLeftB[] = {
        CGPointMake(rect.origin.x - ae, rect.origin.y - angleEdge),
        CGPointMake(rect.origin.x + angleLong, rect.origin.y - angleEdge)
    };
    [self addLine:poinsTopLeftA pointB:poinsTopLeftB ctx:ctx];
    
    //左下角
    CGPoint poinsBottomLeftA[] = {
        CGPointMake(rect.origin.x-angleEdge, rect.origin.y + rect.size.height - angleLong),
        CGPointMake(rect.origin.x-angleEdge, rect.origin.y + rect.size.height + ae)
    };
    
    CGPoint poinsBottomLeftB[] = {
        CGPointMake(rect.origin.x - angleEdge , rect.origin.y + rect.size.height + angleEdge) ,
        CGPointMake(rect.origin.x + angleLong, rect.origin.y + rect.size.height + angleEdge)
    };
    
    [self addLine:poinsBottomLeftA pointB:poinsBottomLeftB ctx:ctx];
    
    //右上角
    CGPoint poinsTopRightA[] = {
        CGPointMake(rect.origin.x + rect.size.width - angleLong, rect.origin.y - angleEdge),
        CGPointMake(rect.origin.x + rect.size.width + angleEdge, rect.origin.y - angleEdge )
    };
    
    CGPoint poinsTopRightB[] = {
        CGPointMake(rect.origin.x + rect.size.width + angleEdge, rect.origin.y - ae),
        CGPointMake(rect.origin.x + rect.size.width + angleEdge, rect.origin.y + angleLong)
    };
    [self addLine:poinsTopRightA pointB:poinsTopRightB ctx:ctx];
    
    CGPoint poinsBottomRightA[] = {
        CGPointMake(rect.origin.x + rect.size.width + angleEdge, rect.origin.y + rect.size.height - angleLong),
        CGPointMake(rect.origin.x + rect.size.width + angleEdge, rect.origin.y + rect.size.height + ae)
    };
    
    CGPoint poinsBottomRightB[] = {
        CGPointMake(rect.origin.x + rect.size.width - angleLong, rect.origin.y + rect.size.height + angleEdge),
        CGPointMake(rect.origin.x + rect.size.width + ae, rect.origin.y + rect.size.height + angleEdge )
    };
    [self addLine:poinsBottomRightA pointB:poinsBottomRightB ctx:ctx];
    CGContextStrokePath(ctx);
}

- (void)addLine:(CGPoint[])pointA pointB:(CGPoint[])pointB ctx:(CGContextRef)ctx {
    CGContextAddLines(ctx, pointA, 2);
    CGContextAddLines(ctx, pointB, 2);
}


- (void) addMoveLine:(CGRect) rect {
    if (!qrLine)
    {
        qrLine  = [[UIImageView alloc] initWithFrame:CGRectMake(rect.origin.x,  rect.origin.y, rect.size.width + 20, 20)];
        qrLine.image = [UIImage imageNamed:@"MLQR_line-"];
        qrLine.contentMode = UIViewContentModeScaleToFill;
        [self addSubview:qrLine];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, rect.origin.y + rect.size.height + 40, CGRectGetWidth(self.frame), 60)];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfBaseSize:14];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor whiteColor];
        label.numberOfLines = 0;
        label.text = [NSString stringWithFormat:@"请将二维码／条码放入框内，%@即可自动扫描", @"\n"];
        [self addSubview:label];
    }
    
    CABasicAnimation *translation = [CABasicAnimation animationWithKeyPath:@"position"];
    
    translation.fromValue = [NSValue valueWithCGPoint:CGPointMake(CGRectGetWidth(self.frame)/2, rect.origin.y)];
    translation.toValue = [NSValue valueWithCGPoint:CGPointMake(CGRectGetWidth(self.frame)/2, rect.origin.y + rect.size.height - 7)];
    translation.duration = 4;
    translation.repeatCount = HUGE_VALF;
    
    [qrLine.layer addAnimation:translation forKey:@"translation"];
}

#pragma mark -
- (void) applicationDidBecomeActive:(id) sender {
    [self addMoveLine:self.rCrop];
}

#pragma mark - 
- (void) reloadQRLine {
    [self addMoveLine:self.rCrop];
}

- (void) stopQRLine {
    [qrLine.layer removeAllAnimations];
}

@end
