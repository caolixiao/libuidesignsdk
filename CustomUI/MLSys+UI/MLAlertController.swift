//
//  MLAlertController.swift
//

import UIKit
import LibBaseSDK

open class MLAlertController: UIAlertController {
    
    //点击空白处dismiss弹框
    public var isBlankDismiss: Bool = false
    private var tapGestureRecognizer: UITapGestureRecognizer?

    override open func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(action(close:)))
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let transform = self.view.superview?.transform {
            if UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.landscapeRight {
                self.view.superview?.transform = transform.rotated(by: CGFloat((90.0*M_PI)/180))
            }
            else if UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.landscapeLeft {
                self.view.superview?.transform = transform.rotated(by: CGFloat((270.0*M_PI)/180));
            }
            else {
                self.view.superview?.transform = transform.rotated(by: 0);
            }
        }
        
        for action in self.actions {
            if action.title == "知道了" {
                action.setValue(__RGB(0x2a2a2a), forKey: "titleTextColor")
            }
            else {
                action.setValue(__RGB(0x2a2a2a), forKey: "titleTextColor")
            }
        }
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isBlankDismiss {
            if let superView = self.view.superview, let tap = tapGestureRecognizer, let tmp = superView.gestureRecognizers, !tmp.contains(tap) {
                superView.addGestureRecognizer(tap)
                superView.isUserInteractionEnabled = true
            }
        }
    }
    
    override open func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.landscapeRight ||
            UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.landscapeLeft {
            self.view.superview?.frame = __SCREEN_BOUNDS_SIZE_CHAGE;
        }
        else {
            self.view.superview?.frame = __SCREEN_BOUNDS;
        }
        
    }
    
    // MARK: -
    @objc private func action(close gestureRecognizer: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: -
    override open var shouldAutorotate :Bool {
        return false
    }
    
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    override open var preferredInterfaceOrientationForPresentation : UIInterfaceOrientation {
        return UIInterfaceOrientation.portrait
    }
}
