//
//  MLStepper.h
//

#import <UIKit/UIKit.h>


@class MLStepper;
@protocol MLStepperDelegate <NSObject>

- (void) stepper:(MLStepper *) stepper changeValue:(NSInteger) value;

@end

@interface MLStepper : UIView {
    @package
    UIButton *_butLess;
    UITextField *_tfValue;
    UIButton *_butPlus;
    
    UIView *_vLineLeft;
    UIView *_vLineRight;
}

@property(nonatomic, assign) NSInteger minNum; // defalut 1
@property(nonatomic, assign) NSInteger maxNum; // defalut 100
@property(nonatomic, assign) NSInteger value;  // defalut 1

@property (nonatomic, assign) id<MLStepperDelegate> delegate;

@end
