//
//  MLPageControl .swift
//

import UIKit
import LibBaseSDK

public class MLPageControl : UIView {

    public var numberOfPages: Int = 0 // default is 0
    
    public var currentPage: Int = 0 {didSet{ pageDeal(oldValue)}}

    public var pageIndicatorTintColor: UIColor?
    
    public var currentPageIndicatorTintColor: UIColor?
    
    fileprivate final let space:CGFloat = 6
    fileprivate final let __width:CGFloat = 5
    fileprivate final let maxShow = 5
    fileprivate final let baseTag = 999
    fileprivate var shouldLitDot = false
    fileprivate var offTag = 0
    fileprivate final let btnView = UIScrollView.init(frame: CGRect.zero)
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        btnView.center = CGPoint(x: VIEWW(self)*0.5, y: VIEWH(self)*0.5)
    }
    override public func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        
        if nil == currentPageIndicatorTintColor{
            currentPageIndicatorTintColor = color_main
        }
        if nil == pageIndicatorTintColor {
            pageIndicatorTintColor = color_93999f
        }
        
        shouldLitDot = numberOfPages > maxShow
        let dotCount = shouldLitDot ? maxShow:numberOfPages
        let itemWidth = space+__width
        btnView.bounds = CGRect(x: 0, y: 0, width: itemWidth*CGFloat(numberOfPages > maxShow ? maxShow:numberOfPages), height: __width)
        for i in 0...dotCount+1 {
            let btn = newBtn(i)
            btnView.addSubview(btn)
            if i >= dotCount && shouldLitDot{
                dealBtnSize(btn, isBig: false)
            }
        }
        btnView.contentOffset = CGPoint.init(x: itemWidth, y: 0)
        self.addSubview(btnView)
    }
    
    
    private func pageDeal(_ oldValue:Int){
        Log.i("old = \(oldValue)   new = \(currentPage)")
        guard oldValue != currentPage else{return}
        let itemWidth = space+__width
        guard let oldDot = btnView.viewWithTag(oldValue-offTag+baseTag) else {return}
        guard let newDot = btnView.viewWithTag(currentPage-offTag+baseTag) else {return }
        if !shouldLitDot{
            // 个数比较少就不往下走了
            oldDot.backgroundColor = pageIndicatorTintColor!
            newDot.backgroundColor = currentPageIndicatorTintColor!
            return
        }
        let oldIndex = oldValue-offTag
        
        //        let oldIndex = Int((oldDot.convert(CGPoint.zero, to: self).x - VIEWX(btnView))/itemWidth)

        let toLeft = currentPage < oldValue
        
        let shouldScroll = isShouldScroll(toLeft, oldIndex: oldIndex)
        
        if !shouldScroll{
            oldDot.backgroundColor = pageIndicatorTintColor!
            newDot.backgroundColor = currentPageIndicatorTintColor!
            return
        }
        let offX = btnView.contentOffset.x
        if toLeft{
            offTag -= 1
            UIView.animate(withDuration: 0.2, animations: {
                self.btnView.contentOffset = CGPoint.init(x: offX - itemWidth, y: 0)
            }) { (complete) in
                if complete{
                    self.reset()
                }
            }
        }else{
            offTag += 1
            UIView.animate(withDuration: 0.2, animations: {
                self.btnView.contentOffset = CGPoint.init(x: offX + itemWidth, y: 0)
            }) { (complete) in
                if complete{
                    self.reset()
                }
            }
        }
        //            perform(#selector(reset), with: nil, afterDelay: 0.3)

    }
    //
    private func dotBigDeal(){
        guard let leftV = btnView.viewWithTag(baseTag),
            let leftHidV = btnView.viewWithTag(baseTag-1),
            let rightV = btnView.viewWithTag(baseTag+maxShow-1),
            let rightHidV = btnView.viewWithTag(baseTag+maxShow) else{return}
        Log.i("offTag = \(offTag)")
        if offTag <= 0{
            // 右边小
            dealBtnSize(leftV, isBig: true)
            dealBtnSize(leftHidV, isBig: true)
            dealBtnSize(rightV, isBig: false)
            dealBtnSize(rightHidV, isBig: false)
        }else if offTag >= numberOfPages-maxShow{
            // u左边小
            dealBtnSize(leftV, isBig: false)
            dealBtnSize(leftHidV, isBig: false)
            dealBtnSize(rightV, isBig: true)
            dealBtnSize(rightHidV, isBig: true)
        }else {
            // 都小
            dealBtnSize(leftV, isBig: false)
            dealBtnSize(leftHidV, isBig: false)
            dealBtnSize(rightV, isBig: false)
            dealBtnSize(rightHidV, isBig: false)
        }
    }
    // scrollView 滑动以后归位 滑动只为动画效果
    private func reset(){
        dotBigDeal()
        btnView.contentOffset = CGPoint.init(x: space+__width, y: 0)
    }
    
    // scrollview是否需要滑动
    private func isShouldScroll(_ toLeft:Bool,oldIndex:Int)->Bool{
        if toLeft{
            if oldIndex == 1 && currentPage != 0{
                return true
            }
        }else{
            if oldIndex == maxShow-2 && currentPage != numberOfPages-1{
                return true
            }
        }
        return false
    }
    
    private func newBtn(_ tag:Int)->UIView{
        let itemWidth = space+__width
        let dotBtn = UIView.init(frame: CGRect.init(x: itemWidth*CGFloat(tag), y: 0, width: __width, height: __width))
        dotBtn.backgroundColor = tag==1 ? currentPageIndicatorTintColor!:pageIndicatorTintColor!
        dotBtn.layer.cornerRadius = __width/2
        dotBtn.layer.masksToBounds = true
        dotBtn.tag = tag+baseTag-1
        return dotBtn
    }
    private func dealBtnSize(_ v:UIView,isBig:Bool){
        let vWidth = isBig ? __width : __width*0.7
        v.frame.size = CGSize.init(width: vWidth, height: vWidth)
        v.center.y = __width/2
        v.layer.cornerRadius = vWidth/2
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
