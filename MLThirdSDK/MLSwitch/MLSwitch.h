//
//  MLSwitch.h
//

#import <UIKit/UIKit.h>

typedef void (^MLSwitchChangeStateBlock)(BOOL isOn) ;

@interface MLSwitch : UIView

@property (nonatomic, strong) NSString *left;
@property (nonatomic, strong) NSString *right;

@property (nonatomic, strong) MLSwitchChangeStateBlock switchChangeStateBlock;

@end
