//
//  UIColor+Bundle.h
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (Bundle)

+ (UIColor *) initWithBundleName:(NSString *)boudleName colorName:(NSString *)imgName class:(Class) aClass;

@end

NS_ASSUME_NONNULL_END
