//
//  UIImage+AuthBundle.m
//

#import "UIImage+UidesignBundle.h"
#import "UIImage+Bundle.h"
#import <LibUidesignSDK/LibUidesignSDK-Swift.h>

@implementation UIImage (UidesignBundle)

+ (UIImage *) imageUidesignNamed:(NSString *)name {
    return [UIImage initWithBundleName: @"Uidesign" imgName: name class: [MLViewController class]];
}

@end
