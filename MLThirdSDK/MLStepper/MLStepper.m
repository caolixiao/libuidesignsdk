//
//  MLStepper.m
//

#import "MLStepper.h"

@interface MLStepper() <UITextFieldDelegate>

@end

@implementation MLStepper

- (instancetype) init {
    if (self = [super init]) {
        _minNum = 1;
        _maxNum = 100;
        _value = 1;
        [self buildBaseView];
    }
    return self;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    CGFloat fWH = VIEWH(self);
    
    _tfValue.frame = self.bounds;
    _butLess.frame = CGRectMake(0, 0, fWH, fWH);
    _butPlus.frame = CGRectMake(0, 0, fWH, fWH);
    
    _vLineLeft.frame = CGRectMake(fWH, 0, 1, fWH);
    _vLineRight.frame = CGRectMake(VIEWW(self) - fWH, 0, 1, fWH);
}

- (void) buildBaseView {
    _butLess = [UIButton buttonWithType: UIButtonTypeCustom];
    [_butLess setTitle:@"-" forState: UIControlStateNormal];
    [_butLess setTitleColor:COLOR_WORD forState:UIControlStateNormal];
//    [_butLess setImage:[UIImage imageNamed:@"stepper_less"] forState: UIControlStateNormal];
    [_butLess addTarget:self action:@selector(actionLessWithButton:) forControlEvents: UIControlEventTouchUpInside];
    
    _butPlus = [UIButton buttonWithType:UIButtonTypeCustom];
    [_butPlus setTitle:@"+" forState: UIControlStateNormal];
    [_butPlus setTitleColor:COLOR_WORD forState:UIControlStateNormal];
//    [_butPlus setImage:[UIImage imageNamed:@"stepper_plus"] forState: UIControlStateNormal];
    [_butPlus addTarget:self action:@selector(actionPlusWithButton:) forControlEvents: UIControlEventTouchUpInside];
    
    _tfValue = [[UITextField alloc] init];
//    _tfValue.enabled = false;
    _tfValue.text = [NSString stringWithFormat:@"%ld", _value];
    _tfValue.delegate = self;
    _tfValue.textAlignment = NSTextAlignmentCenter;
    _tfValue.layer.masksToBounds = true;
    _tfValue.layer.borderColor = [COLOR_GENERAL CGColor];
    _tfValue.layer.borderWidth = 1.0;
    _tfValue.layer.cornerRadius = 6;
    _tfValue.keyboardType = UIKeyboardTypeNumberPad;
    [self addSubview:_tfValue];
    
//    _butLess.backgroundColor = [UIColor redColor];
//    _butPlus.backgroundColor = [UIColor redColor];
    _tfValue.leftView = _butLess;
    _tfValue.rightView = _butPlus;
    _tfValue.leftViewMode = UITextFieldViewModeAlways;
    _tfValue.rightViewMode = UITextFieldViewModeAlways;
    
    _vLineLeft = [[UIView alloc] init];
    _vLineLeft.backgroundColor = COLOR_GENERAL;
    [_tfValue addSubview:_vLineLeft];
    
    _vLineRight = [[UIView alloc] init];
    _vLineRight.backgroundColor = COLOR_GENERAL;
    [_tfValue addSubview:_vLineRight];
    
    [self reloadValue];
}

- (void) actionLessWithButton:(UIButton *) button {
    NSInteger val = _tfValue.text ? _tfValue.text.integerValue : 0;
    if (val > 0 || val < _maxNum) {
        _value = val - 1;
        _tfValue.text = [NSString stringWithFormat:@"%ld", _value];
        [self reloadValue];
    }
}

- (void) actionPlusWithButton:(UIButton *) button {
    NSInteger val = _tfValue.text ? _tfValue.text.integerValue : 0;
    if (val > 0 || val < _maxNum) {
        _value = val + 1;
        _tfValue.text = [NSString stringWithFormat:@"%ld", _value];
        [self reloadValue];
    }
}


#pragma mark - UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self reloadValue];
    if (_value > _maxNum) _tfValue.text = [NSString stringWithFormat:@"%ld", _maxNum];
}

//对键盘输入的操作
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@"\n"]) return true;
    
    NSString *toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    if (_tfValue == textField) {
        if ([toBeString integerValue] > _maxNum) { //如果输入框内容大于10则弹出警告
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"温馨提示"
                                                                                      message:@"输入的数值不能超过最大值"
                                                                               preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil]];
            [self.currentViewController presentViewController:alertController animated:true completion:nil];
            return false;
        }
        
        [self reloadValue];
    }
    return true;
}

- (void) touchesBegan:(NSSet<UITouch *> *) touches withEvent:(UIEvent *) event{
    [self endEditing:YES];
}

- (void) reloadValue {
    _value = _tfValue.text.integerValue;
    _butPlus.enabled = _value < _maxNum;
    _butLess.enabled = _value > _minNum;
    
    if ([_delegate respondsToSelector:@selector(stepper:changeValue:)])
        [_delegate stepper:self changeValue:_value];
}

@end
