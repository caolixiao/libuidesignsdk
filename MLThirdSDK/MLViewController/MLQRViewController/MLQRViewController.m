//
//  MLQRViewController.m
//

#import "MLQRViewController.h"
#import <MLGeneralLibary/MLGlobalHeader.h>

#import <AVFoundation/AVFoundation.h>
#import "MLQRDEcoder.h"
#import "MLQRView.h"

@interface MLQRViewController () <MLQRDEcoderDelegate>
{
    MLQRDEcoder *qrDecoder;
    MLQRView *qrRectView;
}

@end

@implementation MLQRViewController


- (void) dealloc
{
    NSLog(@"dealloc MLQRViewController");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addleftAndTitle:@"扫一扫"];
    self.view.backgroundColor = [UIColor whiteColor];

    qrRectView = [[MLQRView alloc] initWithFrame:self.view.bounds];
    qrRectView.backgroundColor = [UIColor clearColor];
    qrRectView.center = CGPointMake(VIEWW(self.view)/2.0, VIEWH(self.view)/2.0);
    
    //修正扫描区域
    CGFloat screenHeight = self.view.frame.size.height;
    CGFloat screenWidth = self.view.frame.size.width;
    
    qrDecoder = [[MLQRDEcoder alloc] initWithFrame:self.view.bounds];
    [qrDecoder setRectOfInterest:CGRectMake(qrRectView.rCrop.origin.y / screenHeight,
                                            qrRectView.rCrop.origin.x / screenWidth,
                                            qrRectView.rCrop.size.height / screenHeight,
                                            qrRectView.rCrop.size.width / screenWidth)];
    
    [self.view addSubview:qrDecoder];
    [self.view addSubview:qrRectView];
}

#pragma mark -
#pragma mark Private Method
- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus != AVAuthorizationStatusAuthorized) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Please allow unixue to access your device's camera in \"Setting\" -> \"Privacy\" -> \"Camera\"." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    [qrDecoder start];
    [qrRectView reloadQRLine];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.view.backgroundColor = [UIColor clearColor];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.view.backgroundColor = [UIColor clearColor];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - MLQRDEcoderDelegate
- (void) qrDEcoder:(MLQRDEcoder *) qrDEcoder isRunning:(BOOL) running {
    if (running) [qrRectView reloadQRLine]; else [qrRectView stopQRLine];
}

- (void) qrDEcoder:(MLQRDEcoder *) qrDEcoder result:(NSString *) result {
    [qrRectView stopQRLine];
    if (_qrUrlBlock) _qrUrlBlock(result);
    [self.navigationController popViewControllerAnimated:YES];
}

@end
