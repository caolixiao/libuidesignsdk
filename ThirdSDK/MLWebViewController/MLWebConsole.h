//
//  MLWebConsole.h
//

#import <Foundation/Foundation.h>

@interface MLWebConsole : NSURLProtocol

+ (void) loadMLClass:(Class) cl;

@end
