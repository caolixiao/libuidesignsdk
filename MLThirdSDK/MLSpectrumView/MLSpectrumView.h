//
//  MLSpectrumView.h
//

#import <UIKit/UIKit.h>

@interface MLSpectrumView : UIView

@property (nonatomic, assign) CGFloat level;
@property (nonatomic, assign) NSUInteger numberOfItems;
@property (nonatomic, strong) UIColor *itemColor;

- (void) startAudioValue;

@end
