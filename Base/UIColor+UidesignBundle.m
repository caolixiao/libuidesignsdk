//
//  UIColor+AuthBundle.m
//

#import "UIColor+UidesignBundle.h"
#import "UIColor+Bundle.h"
#import <LibUidesignSDK/LibUidesignSDK-Swift.h>

@implementation UIColor (UidesignBundle)

+ (UIColor *) colorUidesignNamed:(NSString *)name {
    return [UIColor initWithBundleName: @"Auth" colorName: name class: [MLViewController class]];
}

@end
