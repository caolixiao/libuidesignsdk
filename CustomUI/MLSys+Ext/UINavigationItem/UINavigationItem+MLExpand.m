//
//  UINavigationItem+MLExpand.m
//

#import "UINavigationItem+MLExpand.h"
#import <objc/runtime.h>
#import <QuartzCore/QuartzCore.h>
#import <LibBaseSDK/UIImage+MLCustom.h>

NSString *const kDelegateA = @"__DelegateA";

@implementation UINavigationItem (MLExpand)

@dynamic butLeft;
@dynamic butRight;
@dynamic butRight2;

static const char *__CustomNavItem__butLeft__ = "__CustomNavItem__butLeft__";
static const char *__CustomNavItem__butRight__ = "__CustomNavItem__butRight__";
static const char *__CustomNavItem__butRight2__ = "__CustomNavItem__butRight2__";
- (void) setButLeft:(UIButton *)butLeft { objc_setAssociatedObject(self, __CustomNavItem__butLeft__, butLeft, OBJC_ASSOCIATION_RETAIN_NONATOMIC);}
- (UIButton *) butLeft { return objc_getAssociatedObject(self, __CustomNavItem__butLeft__); }
- (void) setButRight:(UIButton *)butRight { objc_setAssociatedObject(self, __CustomNavItem__butRight__, butRight, OBJC_ASSOCIATION_RETAIN_NONATOMIC);}
- (UIButton *) butRight { return objc_getAssociatedObject(self, __CustomNavItem__butRight__); }
- (void) setButRight2:(UIButton *)butRight2 { objc_setAssociatedObject(self, __CustomNavItem__butRight2__, butRight2, OBJC_ASSOCIATION_RETAIN_NONATOMIC);}
- (UIButton *) butRight2 { return objc_getAssociatedObject(self, __CustomNavItem__butRight2__); }

#pragma mark - title
// title
- (void) addTitle:(NSString *) title {
    [self setHidesBackButton:YES];
    [self setLeftBarButtonItem:nil];
    [self setRightBarButtonItem:nil];
    
    self.title = title;
}

- (void) modTitle:(NSString *) title {
    self.title = title;
}

- (void) setCenterView:(UIView *) view {
    self.titleView = view;
}

#pragma mark - left
- (void) setLeftView:(UIView *) view {
    UIBarButtonItem* leftItem = [[UIBarButtonItem alloc] initWithCustomView:view];
    [self setLeftBarButtonItem:leftItem];
}

- (void) setLeftTitle:(NSString *) title {
    [self setLeftImg:nil title:title];
}

- (void) setLeftImg:(NSString *) imgPath {
    [self setLeftImg:imgPath title:nil];
}

- (void) setLeftImg:(NSString *) imgPath title:(NSString *) title {
    [self setLeftImg:imgPath title:title imgType:NavItemImgTypeLeft];
}

- (void) setLeftImg:(NSString *) imgPath title:(NSString *) title color:(UIColor *) wColor {
    [self setLeftImg:imgPath title:title font:[UIFont systemFontOfSize:14] color:wColor imgType:NavItemImgTypeLeft];
}

- (void) setLeftImg:(NSString *) imgPath title:(NSString *) title imgType:(NavItemImgType) type; {
    [self setLeftImg:imgPath title:title font:[UIFont systemFontOfSize:14] imgType:type];
}

- (void) setLeftImg:(NSString *) imgPath title:(NSString *) title font:(UIFont*) font imgType:(NavItemImgType) type {
    [self setLeftImg:imgPath title:title font:font color:UIColor.whiteColor imgType:type];
}

- (void) setLeftImg:(NSString *) imgPath title:(NSString *) title font:(UIFont*) font color:(UIColor *) wColor imgType:(NavItemImgType) type {
    if (!imgPath && !title) return;
    
    BOOL isImg = NO;
    self.butLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.butLeft addTarget:self action:@selector(backItemButton:) forControlEvents:UIControlEventTouchUpInside];
    
    if (imgPath && type != NavItemImgTypeNone) {
        isImg = YES;
        UIImage *imgNormal = [UIImage imageNamed:[NSString stringWithFormat:@"%@_normal",imgPath]];
//        if (!imgNormal) imgNormal = [UIImage imgGNamed:[NSString stringWithFormat:@"%@_normal",imgPath]];
        UIImage *imgHighlighted = [UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",imgPath]];
//        if (!imgHighlighted) imgHighlighted = [UIImage imgGNamed:[NSString stringWithFormat:@"%@_selected",imgPath]];
        
        
        
        if (type == NavItemImgTypeBKG) {
            [self.butLeft setBackgroundImage:imgNormal forState:UIControlStateNormal];
            [self.butLeft setBackgroundImage:imgHighlighted forState:UIControlStateHighlighted];
        }else {
            [self.butLeft setImage:imgNormal forState:UIControlStateNormal];
            [self.butLeft setImage:imgHighlighted forState:UIControlStateHighlighted];
            self.butLeft.imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
    }
    
    if (title) {
        self.butLeft.titleLabel.font = font;
        [self.butLeft setTitle:title forState:UIControlStateNormal];
        [self.butLeft setTitleColor:wColor forState:UIControlStateNormal];
        [self.butLeft setTitleColor:[wColor colorWithAlphaComponent:0.4] forState:UIControlStateHighlighted];
    }
    
    {
        [self.butLeft sizeToFit];
        
        CGFloat fW = self.butLeft.bounds.size.width + 5;
        CGFloat fH = self.butLeft.bounds.size.height;
        if (fW > 120) fW = 120;
        if (fH > 40) fH = 40;
        self.butLeft.frame = (CGRect){CGPointZero, {fW, fH}};
        
        if (isImg && title) {
            CGFloat fW_img = self.butLeft.imageView.bounds.size.width;
            CGFloat fH_img = self.butLeft.imageView.bounds.size.height;
            if (type == NavItemImgTypeLeft) {
                [self.butLeft setImageEdgeInsets:UIEdgeInsetsMake(2, 0, 2, fW - fW_img - 4)];
            }else if (type == NavItemImgTypeRight) {
                [self.butLeft setImageEdgeInsets:UIEdgeInsetsMake(0, 0.0, 0, fW - fW_img)];
                [self.butLeft setTitleEdgeInsets:UIEdgeInsetsMake(0, 5.0, 0, 0.0)];
            }else if (type == NavItemImgTypeUp) {
                [self.butLeft setImageEdgeInsets:UIEdgeInsetsMake(0, 0.0, 0, fW - fH_img)];
                [self.butLeft setTitleEdgeInsets:UIEdgeInsetsMake(0, 5.0, 0, 0.0)];
            }else if (type == NavItemImgTypeDown) {
                
            }
        }
    }

    UIBarButtonItem* backItem= [[UIBarButtonItem alloc] initWithCustomView:self.butLeft];
    [self setLeftBarButtonItem:backItem];
}

#pragma mark - right
- (void) setRightView:(UIView *) view
{
    UIBarButtonItem* rightItem= [[UIBarButtonItem alloc] initWithCustomView:view];
    [self setRightBarButtonItem:rightItem];
}

- (void) setRightTitle:(NSString *) title {
    [self setRightImg:nil title:title];
}

- (void) setRightImg:(NSString *) imgPath {
    [self setRightImg:imgPath title:nil];
}

- (void) setRightImg:(NSString *) imgPath title:(NSString *) title {
    [self setRightImg:imgPath title:title imgType:NavItemImgTypeLeft];
}

- (void) setRightImg:(NSString *) imgPath title:(NSString *) title color:(UIColor *) wColor {
    [self setRightImg:imgPath title:title font:[UIFont systemFontOfSize:14] color:wColor imgType:NavItemImgTypeLeft];
}

- (void) setRightImg:(NSString *) imgPath title:(NSString *) title imgType:(NavItemImgType) type {
    [self setRightImg:imgPath title:title font:[UIFont systemFontOfSize:14] imgType:type];
}

- (void) setRightImg:(NSString *) imgPath title:(NSString *) title font:(UIFont*) font imgType:(NavItemImgType) type {
    [self setRightImg:imgPath title:title font:font color:UIColor.whiteColor imgType:type];
}

- (void) setRightImg:(NSString *) imgPath title:(NSString *) title font:(UIFont*) font color:(UIColor *) wColor imgType:(NavItemImgType) type
{
    if (!imgPath && !title) return;
    
    BOOL isImg = NO;
    self.butRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.butRight addTarget:self action:@selector(rightItemButton:) forControlEvents:UIControlEventTouchUpInside];
    
    if (imgPath && type != NavItemImgTypeNone) {
        isImg = YES;
        UIImage *imgNormal = [UIImage imageNamed:[NSString stringWithFormat:@"%@_normal",imgPath]];
//        if (!imgNormal) imgNormal = [UIImage imgGNamed:[NSString stringWithFormat:@"%@_normal",imgPath]];
        UIImage *imgHighlighted = [UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",imgPath]];
//        if (!imgHighlighted) imgHighlighted = [UIImage imgGNamed:[NSString stringWithFormat:@"%@_selected",imgPath]];
        if (!imgHighlighted && imgNormal) imgHighlighted = [imgNormal imageWithAlpha:0.6];
        
        if (type == NavItemImgTypeBKG || type == NavItemImgTypePoint) {
            [self.butRight setBackgroundImage:imgNormal forState:UIControlStateNormal];
            [self.butRight setBackgroundImage:imgHighlighted forState:UIControlStateHighlighted];
        }else {
            [self.butRight setImage:imgNormal forState:UIControlStateNormal];
            [self.butRight setImage:imgHighlighted forState:UIControlStateHighlighted];
        }
    }
    
    if (title) {
        self.butRight.titleLabel.font = font;
        [self.butRight setTitle:title forState:UIControlStateNormal];
        [self.butRight setTitleColor:wColor forState:UIControlStateNormal];
        [self.butRight setTitleColor:[wColor colorWithAlphaComponent:0.4] forState:UIControlStateHighlighted];
    }
    
    {
        [self.butRight sizeToFit];
        
        CGFloat fW = self.butRight.bounds.size.width;
        CGFloat fH = self.butRight.bounds.size.height;
        if (fW > 120) fW = 120;
        if (fH > 40) fH = 40;
        self.butRight.frame = (CGRect){CGPointZero, {fW, fH}};
        
        if (isImg && title) {
            CGFloat fW_img = self.butRight.imageView.bounds.size.width;
            CGFloat fH_img = self.butRight.imageView.bounds.size.height;
            if (type == NavItemImgTypeLeft) {
                
            }else if (type == NavItemImgTypeRight) {
                [self.butRight setImageEdgeInsets:UIEdgeInsetsMake(0, 0.0, 0, fW - fW_img)];
                [self.butRight setTitleEdgeInsets:UIEdgeInsetsMake(0, 5.0, 0, 0.0)];
            }else if (type == NavItemImgTypeUp) {
                [self.butRight setImageEdgeInsets:UIEdgeInsetsMake(0, 0.0, 0, fW - fH_img)];
                [self.butRight setTitleEdgeInsets:UIEdgeInsetsMake(0, 5.0, 0, 0.0)];
            }else if (type == NavItemImgTypeDown) {
                
            }else if (type == NavItemImgTypePoint) {
                [self.butRight setTitle:[NSString stringWithFormat:@" %@ ", title] forState:UIControlStateNormal];
                self.butRight.titleLabel.backgroundColor = [UIColor redColor];
                self.butRight.titleLabel.layer.cornerRadius = 6;
                self.butRight.titleLabel.layer.masksToBounds = YES;
                CGFloat tmp = self.butRight.titleLabel.frame.size.width/2.0 + 5;
                CGFloat fL = self.butRight.frame.size.width/2.0 + tmp - 10;
                [self.butRight setTitleEdgeInsets:UIEdgeInsetsMake(-6, fL, 12, -tmp)];
            }
        }
    }
    
    UIBarButtonItem* rightItem= [[UIBarButtonItem alloc] initWithCustomView:self.butRight];
    [self setRightBarButtonItem:rightItem];
}

- (void) setRight1Img:(NSString *) img1 right2:(NSString *) img2
{
    UIView *view = [[UIView alloc] init];
    
    self.butRight = [UIButton buttonWithType:UIButtonTypeCustom];
    self.butRight.translatesAutoresizingMaskIntoConstraints = NO;
    [self.butRight addTarget:self action:@selector(centerItemButton:) forControlEvents:UIControlEventTouchUpInside];
    UIImage *img1Normal = [UIImage imageNamed:[NSString stringWithFormat:@"%@_normal",img1]];
    [self.butRight setImage:img1Normal forState:UIControlStateNormal];
    [self.butRight setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",img1]] forState:UIControlStateHighlighted];
    [self.butRight setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",img1]] forState:UIControlStateSelected];
    [view addSubview:self.butRight];
    
    self.butRight2 = [UIButton buttonWithType:UIButtonTypeCustom];
    self.butRight2.translatesAutoresizingMaskIntoConstraints = NO;
    [self.butRight2 addTarget:self action:@selector(rightItemButton:) forControlEvents:UIControlEventTouchUpInside];
    UIImage *imgNormal = [UIImage imageNamed:[NSString stringWithFormat:@"%@_normal",img2]];
    [self.butRight2 setImage:imgNormal forState:UIControlStateNormal];
    [self.butRight2 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",img2]] forState:UIControlStateHighlighted];
    [self.butRight2 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",img2]] forState:UIControlStateSelected];
    [view addSubview:self.butRight2];
    
    view.frame = CGRectMake(0.0f, 0.0f, img1Normal.size.width + imgNormal.size.width + 20.0f, img1Normal.size.height + 3.0f);
    
    UIBarButtonItem* rightItem= [[UIBarButtonItem alloc] initWithCustomView:view];
    [self setRightBarButtonItem:rightItem];
    
}

#pragma mark - Methon
//返回上进入的视图
- (void) backItemButton:(UIButton *) button {
    id delegate = objc_getAssociatedObject( self, (__bridge void *)kDelegateA );
    if (delegate && [delegate respondsToSelector:@selector(backItemButton:)])
        [delegate backItemButton:button];
    else
        NSLog(@"需要重构 - (void) backItemButton:(UIButton *) button 在 %@", delegate);
}

- (void) rightItemButton:(UIButton *) button {
    id delegate = objc_getAssociatedObject( self, (__bridge void *)kDelegateA );
    if (delegate && [delegate respondsToSelector:@selector(rightItemButton:)])
        [delegate rightItemButton:button];
    else
        NSLog(@"需要重构 - (void) rightItemButton:(UIButton *) button 在 %@", delegate);
}

- (void) centerItemButton:(UIButton *) button {
    id delegate = objc_getAssociatedObject( self, (__bridge void *)kDelegateA );
    if (delegate && [delegate respondsToSelector:@selector(centerItemButton:)])
        [delegate centerItemButton:button];
    else
        NSLog(@"需要重构 - (void) centerItemButton:(UIButton *) button 在 %@", delegate);
}

- (void) setDelegate:(id)delegate
{
    objc_setAssociatedObject( self, (__bridge void *)kDelegateA, delegate, OBJC_ASSOCIATION_ASSIGN);
}

@end
