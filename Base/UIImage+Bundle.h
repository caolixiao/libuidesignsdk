//
//  UIImage+boundle.h
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Bundle)

+ (UIImage *) initWithBundleName:(NSString *)boudleName imgName:(NSString *)imgName class:(Class) aClass;

@end

NS_ASSUME_NONNULL_END
