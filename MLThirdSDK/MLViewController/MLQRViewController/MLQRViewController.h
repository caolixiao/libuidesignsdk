//
//  MLQRViewController.h
//


#import "MLBaseViewController.h"


typedef void(^MLQRUrlBlock)(NSString *result);

@interface MLQRViewController : MLBaseViewController

@property (nonatomic, strong) MLQRUrlBlock qrUrlBlock;

@end
