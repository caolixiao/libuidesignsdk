//
//  MLTextField.swift
//

import UIKit
import LibBaseSDK

extension UITextFieldDelegate {
    @available(iOS 9.0, *)
    func textFiled(textField: MLTextField, didSelected eye: Bool) {}
}

open class MLTextField: UITextField {
    
    public enum MLTextFieldStyle {
        case normal
        case leftTitle
        case leftImage
        case eye
        
        public enum Line {
            case normal
            case top
            case bottom
            case left
            case right
        }
    }
    
    public private(set) var style: MLTextFieldStyle = .normal
    
    // MARK: - LEFT IMAGE
    public lazy var ivLeft: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .clear
        iv.contentMode = .scaleAspectFit
        
        leftView = iv
        leftViewMode = .always
        return iv
    }()
    public var leftImgPath: String?
    public var leftImg: UIImage?
    
    // MARK: - LEFT LABEL
    public lazy var labLeftTitle: UILabel = {
        let label = UILabel()
        label.backgroundColor = .clear
        label.font = UIFont.systemFont(ofBaseSize: 17)
        label.textColor = color_black;
        label.textAlignment = .right

        leftView = label
        leftViewMode = .always
        return label
    }()
    public var leftTitle: String?
    
    public var leftSize: CGSize?
    public var leftMinWidth: CGFloat?
    
    // MARK: - RIGHT EYE
    public lazy var butEye: UIButton = {
        let but = UIButton(type: UIButton.ButtonType.custom)
        but.setImage(UIImage(named: "MLTextField_eye_close"), for: .normal)
        but.setImage(UIImage(named: "MLTextField_eye_open"), for: .disabled)
        but.setImage(UIImage(named: "MLTextField_eye_open"), for: .selected)
        but.isSelected = true
        but.addTarget(self, action: #selector(action(eye:)), for: UIControl.Event.touchUpInside)
        
        rightView = but
        rightViewMode = .always
        return but
    }()
    
    // MARK: - LINE
    public lazy var vLine: UIView = {
        let v = UIView()
        v.backgroundColor = color_line
        v.frame = CGRect(x: 0, y: VIEWH(self) - 0.5, width: VIEWW(self), height: 0.5)
        self.addSubview(v)
        return v
    }()
    public var lineStyle: MLTextFieldStyle.Line? = .normal { didSet { reload(lineStyle: lineStyle) } }
    
    // MARK: - limit
    public var limit: MLTextField.LimitLength?
    public var length: Int?
    
    
    // MARK: - Sys
    deinit {
        Log.i("deinit MLTextField")
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public init(style: MLTextFieldStyle) {
        super.init(frame: .zero)
        self.style = style
        self.clearButtonMode = .whileEditing
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        
        reload(style: style)
        if let style = lineStyle { reload(lineStyle: style) }
    }
    
    // MARK: -
    // 未编辑状态下的起始位置
    open override func textRect(forBounds bounds: CGRect) -> CGRect {
        var _bounds = bounds
        if let size = leftSize { _bounds.origin.x = size.width + CGScale(13) }
        else if let fw = leftMinWidth { _bounds.origin.x = fw }
        return _bounds
    }

    // 编辑状态下的起始位置
    open override func editingRect(forBounds bounds: CGRect) -> CGRect {
        var _bounds = bounds
        if let size = leftSize { _bounds.origin.x = size.width + CGScale(13) }
        else if let fw = leftMinWidth { _bounds.origin.x = fw }
        return _bounds
    }

    // placeholder起始位置
//    open override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
//        var _bounds = bounds
//        if let size = leftSize { _bounds.origin.x = size.width + CGScale(13) }
//        else if let fw = leftMinWidth { _bounds.origin.x = fw }
//        return _bounds
//    }
}

extension MLTextField {
    public func reload(style: MLTextFieldStyle) {
        switch style {
        case .normal:
            
            break
        case .leftImage:
            if let img = leftImg {
                ivLeft.image = img
            } else if let imgPath = leftImgPath {
                ivLeft.image = UIImage(named: imgPath)
            }
            if let size = leftSize {
                ivLeft.frame = CGRect(x: 0, y: (VIEWH(self) - size.height)*0.5, width: size.width, height: size.height)
            }
            else if let fw = leftMinWidth {
                ivLeft.frame = CGRect(x: 0, y: 0, width: fw, height: VIEWH(self))
            }
            else {
                ivLeft.sizeToFit()
            }
            break
        case .leftTitle:
            labLeftTitle.text = "\(leftTitle ?? "")"
            if let fw = leftMinWidth {
                labLeftTitle.frame = CGRect(x: 0, y: 0, width: fw, height: VIEWH(self))
            }
            else {
                labLeftTitle.sizeToFit()
            }
            break
        case .eye:
            isSecureTextEntry = true
            butEye.frame = CGRect(x: 0, y: 0, width: CGScale(40), height: VIEWH(self))
            break
        default:
            break
        }
    }
    
    // MARK: -
    @objc
    private func action(eye: UIButton) {
        eye.isSelected = !eye.isSelected
        isSecureTextEntry = eye.isSelected
        delegate?.textFiled(textField: self, didSelected: eye.isSelected)
    }
}

extension MLTextField {
    public func reload(lineStyle: MLTextFieldStyle.Line?) -> Self {
        switch lineStyle {
        case .normal:
            
            break
        case .top:
            
            break
        case .bottom:
            
            break
        case .left:
            
            break
        case .right:
            var fw: CGFloat = 0
            if style == .leftTitle  { fw = VIEWW(labLeftTitle) }
            if let tmp = leftMinWidth { fw = tmp }
            vLine.frame = CGRect(x: fw, y: VIEWH(self) - 0.5, width: VIEWW(self) - fw, height: 0.5)
            break
        default:
            break
        }
        return self
    }
}


extension MLTextField {
    
    public typealias LimitLength = (_ textField: MLTextField, _ limit: Int) -> Void
    public func limit(length: Int, complate: @escaping LimitLength) {
        self.length = length
        self.limit = complate
        self.addTarget(self, action: #selector(action(limit:)), for: UIControl.Event.editingChanged)
    }
    
    @objc
    private func action(limit textField: MLTextField) {
        if let str = textField.text?.replacingOccurrences(of: "?", with: ""), let len = length {
            // 判断当前输入法是否是中文
            if textField.textInputMode?.primaryLanguage == "zh_CN" {
                
                if let range = textField.markedTextRange,
                    let position = textField.position(from: range.start, offset: 0),
                    str.count > len {
                    textField.text = str.prefix(len).base
                    limit?(self, len)
                }
                
            }
            else {
                if str.count > len {
                    textField.text = str.prefix(len).base
                    limit?(self, len)
                }
            }
        }
    }
}
