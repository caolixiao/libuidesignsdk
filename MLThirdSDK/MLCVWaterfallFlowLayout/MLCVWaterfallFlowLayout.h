//
//  MLCVWaterfallFlowLayout.h
//

#import <UIKit/UIKit.h>

@class MLCVWaterfallFlowLayout;

@protocol MLCVWaterfallFlowLayoutDelegate <NSObject>
@required
- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;

@optional
- (CGFloat) columnCountInWaterflowLayout:(MLCVWaterfallFlowLayout *) flowLayout;
- (CGFloat) columnMarginInWaterflowLayout:(MLCVWaterfallFlowLayout *) flowLayout;
- (CGFloat) rowMarginInWaterflowLayout:(MLCVWaterfallFlowLayout *) flowLayout;
- (UIEdgeInsets) edgeInsetsInWaterflowLayout:(MLCVWaterfallFlowLayout *) flowLayout;

@end

@interface MLCVWaterfallFlowLayout : UICollectionViewLayout

@property (nonatomic, weak) id<MLCVWaterfallFlowLayoutDelegate> delegate;

@end
