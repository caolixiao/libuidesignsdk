//
//  UISearchBar+Ext.swift
//

import UIKit
import LibBaseSDK

extension UISearchBar {
    
    private struct AssociatedKey {
        static var but = "ml.toby.searchBar.butSearch"
        static var tf = "ml.toby.searchBar.textField"
    }
    
    public var butSearch: UIButton? {
        get { return objc_getAssociatedObject(self, &AssociatedKey.but) as? UIButton }
        set { objc_setAssociatedObject(self, &AssociatedKey.but, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }
    
    public var textField: UITextField? {
        get { return objc_getAssociatedObject(self, &AssociatedKey.tf) as? UITextField }
        set { objc_setAssociatedObject(self, &AssociatedKey.tf, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }

    
    public func setCancelButton(title: String, fontColor: UIColor = color_main, fontSize: CGFloat = 18) {
        showsCancelButton = true
        if let butCancel = value(forKey: "cancelButton") as? UIButton {
            butCancel.titleLabel?.font = UIFont.systemFont(ofBaseSize: fontSize)
            butCancel.setTitle(title, for: .normal)
            butCancel.setTitleColor(fontColor, for: .normal)
            butCancel.setTitleColor(fontColor, for: .selected)
            butCancel.setTitleColor(fontColor, for: .disabled)
            butCancel.setTitleColor(fontColor, for: .highlighted)
            self.butSearch = butCancel
        }
    }
    
    public func setTextField(textFieldBkg: UIColor = color_white,
                  tfRadius radius: CGFloat = 0,
                  fontColor: UIColor? = color_black,
                  placeholder: String? = nil) {
        
        tintColor = color_main // 光标是红色和取消按钮是红色
        barTintColor = color_main
        isTranslucent = true //控制收索框的透明度

        if let searchField = value(forKey: "searchField") as? UITextField {
            self.textField = searchField
            searchField.backgroundColor = textFieldBkg
            
            if let color = fontColor {
                searchField.textColor = color
            }
            
            if radius > 0 {
                searchField.layer.cornerRadius = radius
                searchField.layer.masksToBounds = true
            }
            
            if let str = placeholder, let color = fontColor {
                let mutaAttrStr = NSMutableAttributedString(string: str)
                mutaAttrStr.addAttribute(.foregroundColor, value: color, range: NSMakeRange(0, str.count))
                searchField.attributedPlaceholder = mutaAttrStr
            }
            
            if let iv = searchField.leftView as? UIImageView {
                iv.image = iv.image?.tint(with: color_main)
            }
        }
    }
}
