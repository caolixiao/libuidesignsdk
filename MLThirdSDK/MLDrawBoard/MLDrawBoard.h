//
//  MLDrawBoard.h
//

#import <UIKit/UIKit.h>
#import "MLDrawModel.h"

#define MLThumbnailPath [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:@"MLThumbnail"]

@class MLDrawBoard;

@protocol MLDrawBoardDelegate <NSObject>

- (void) drawBoard:(MLDrawBoard *) drawView drawingStatus:(MLDrawingStatus) status model:(MLDrawModel *) model;

@end


@interface MLDrawBoard : UIView {
    NSMutableArray *mAPaths;
    NSMutableArray *mAPoints;
    NSMutableArray *mABufPaths;
}

@property (nonatomic, assign) BOOL isEraser;
@property (nonatomic, assign) MLDrawingShapeType shapType;
@property (nonatomic, assign) int fWLine;
@property (nonatomic, strong) UIColor *colorPan;

@property (nonatomic, assign) id<MLDrawBoardDelegate> delegate;

- (BOOL) drawWithPoints:(MLDrawModel *) model;
- (void) revoke;

@end
