//
//  MLWebViewController.h
//

#import <MLGeneralLibary/MLBaseViewController.h>

typedef void (^MLWebFinishLoad) (NSString *title, NSString *url);
typedef void (^MLWebDidReceiveScript) (NSString *message, NSString *scheme, BOOL *is);


@class MLWebView;

@interface MLWebViewController : MLBaseViewController <UIWebViewDelegate, UINavigationControllerDelegate> {
@private
    UIView *vLeftItem;
    UIButton *butBack;
    UIButton *butClose;
    
@private
    MLWebView *_webView;
    UIProgressView* _pv;
    
    BOOL isEnter;
}

@property (nonatomic, strong) NSString *pathURL;
@property (nonatomic, assign) BOOL isRegisterClass;

@property (nonatomic, strong) MLWebFinishLoad blockWFL;
@property (nonatomic, strong) MLWebDidReceiveScript blockWDRS;

@end
