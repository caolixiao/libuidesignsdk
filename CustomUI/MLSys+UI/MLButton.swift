//
//  MLButton.swift
//

import UIKit
import LibBaseSDK

extension DispatchSource.TimerFlags {
    public struct MLButtom {
        public static let down = DispatchSource.TimerFlags(rawValue: 0)
    }
}

open class MLButton: UIButton {
    
    public var imgFull: Bool = false
    
    public enum Style {
        case normal
        case next
        case border
        case upperLower
        case borderLine
    }

    public func startCountDown(len: Int) {
        isEnabled = false
        var time: Int = len
        let title: String? = titleLabel?.text
        var timer: DispatchSource = DispatchSource.makeTimerSource(flags: DispatchSource.TimerFlags.MLButtom.down, queue: DispatchQueue.main) as! DispatchSource
        timer.setEventHandler {
            self.setTitle("\(time)秒", for: .normal)
            time-=1
            if time == 0 { timer.cancel() }
        }
        timer.setCancelHandler {
            self.setTitle(title, for: .normal)
            self.isEnabled = true
        }
        timer.resume()
    }
    

    override open func imageRect(forContentRect contentRect: CGRect) -> CGRect {
        return imgFull ? self.bounds : contentRect
    }

//    - (CGRect) titleRectForContentRect:(CGRect)contentRect
//    {
//        CGRect rect = contentRect;
//        rect.origin.x = - (VIEWW(self)/2.0f);
//        rect.origin.y = y - iw/2.0 + CGScale(10.0) + iw + (iw - CGScale(20.0))/2.0;
//        rect.size.width = VIEWW(self)*2;
//        rect.size.height = CGScale(20.0);
//        return rect;
//    }
}
