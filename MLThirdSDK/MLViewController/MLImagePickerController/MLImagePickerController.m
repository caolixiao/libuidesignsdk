//
//  MLImagePickerController.m
//
//

#import "MLImagePickerController.h"
#import "MLMessageTool.h"

@interface MLImagePickerController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    MLImagePickerBlock _block;
}

@end

@implementation MLImagePickerController


+ (instancetype) sharedInstance {
    static MLImagePickerController *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MLImagePickerController alloc] init];
    });
    
    return sharedInstance;
}

#pragma mark - changeUserIconTap
- (void) selectedImgWithComplate:(MLImagePickerBlock) block {
    _block = [block copy];
    
    __weak MLImagePickerController *weakSelf = self;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"选择方式" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"返回" style:UIAlertActionStyleCancel handler:NULL];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"从相册选取" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf setPickerController:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    }];
    UIAlertAction *ok1Action = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
#if TARGET_IPHONE_SIMULATOR
        [MLMessageTool showPromptMessage:@"模拟器无法进行拍照"];
#elif TARGET_OS_IPHONE
        [weakSelf setPickerController:UIImagePickerControllerSourceTypeCamera];
#endif
    }];
    [alert addAction:cancelAction];
    [alert addAction:okAction];
    [alert addAction:ok1Action];
    
    UIViewController *vc = [[UIView getTopWindow] currentViewController];
    [vc presentViewController:alert animated:YES completion:nil];
}


- (void) selectedImgAtCameraWitchComplate:(MLImagePickerBlock) block {
    _block = [block copy];
    [self setPickerController:UIImagePickerControllerSourceTypeCamera];
}

- (void) selectedImgAtPhotoWitchComplate:(MLImagePickerBlock) block {
    _block = [block copy];
    [self setPickerController:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
}

- (void) setPickerController:(UIImagePickerControllerSourceType) type {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = type;
    picker.allowsEditing = YES;
    
    UIViewController *vc = [[UIView getTopWindow] currentViewController];
    [vc presentViewController:picker animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *img = [info objectForKey:UIImagePickerControllerEditedImage];
    if (_block) _block(img);
    _block = nil;
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UINavigationControllerDelegate
-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    [[navigationController navigationBar] setDefaultBackground];
}

@end
