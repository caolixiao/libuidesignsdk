//
//  MLCircularProgressView.h
//

#import <UIKit/UIKit.h>


@interface MLCircularProgressView : UIControl

// A value from 0 to 1 that indicates how much progress has been made
// When progress is zero, the progress view functions as an indeterminate progress indicator (a spinner)

@property (nonatomic) float progress;

@property (nonatomic, strong) UIColor *progressTintColor;


- (void)setProgress:(float)progress animated:(BOOL)animated;

@end
