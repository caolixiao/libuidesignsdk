//
//  MLDrawBoard.m
//

#import "MLDrawBoard.h"
#import <NSFileManager+MLCoustom.h>
#import <MLMessageTool.h>

@interface MLDrawBoard ()

@property (nonatomic, strong) UIImageView *ivDraw;
@property (nonatomic, strong) MLDrawView *vDraw;

@end

@implementation MLDrawBoard

- (instancetype) init {
    if (self = [super init]) {
        self.backgroundColor = [UIColor clearColor];
        
        mAPaths = [NSMutableArray arrayWithCapacity:0];
        mAPoints = [NSMutableArray arrayWithCapacity:0];
        mABufPaths = [NSMutableArray arrayWithCapacity:0];
        
        _ivDraw = [[UIImageView alloc] init];
        _ivDraw.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_ivDraw];
        
        _vDraw = [MLDrawView new];
        _vDraw.frame = self.bounds;
        [_ivDraw addSubview:_vDraw];
    }
    return self;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    _ivDraw.frame = self.bounds;
}

#pragma mark -
- (BOOL) drawWithPoints:(MLDrawModel *) model {
    
    self.userInteractionEnabled = NO;
    
    //比值
    CGFloat xPix = ([UIScreen mainScreen].bounds.size.width * [UIScreen mainScreen].scale);
    CGFloat yPix = ([UIScreen mainScreen].bounds.size.height * [UIScreen mainScreen].scale);
    CGFloat xp = model.width.floatValue / xPix;
    CGFloat yp = model.height.floatValue / yPix;
    
//    MLDrawPoint *point = [model.pointList firstObject];
//    MLPath *path = [MLPath pathToPoint:CGPointMake(point.x.floatValue * xp , point.y.floatValue * yp) pathWidth:model.paintSize.floatValue isEraser:model.isEraser.boolValue];
//    path.pathColor = [UIColor colorWithHexString:model.paintColor];
//    [mAPaths addObject:path];
//    NSMutableArray *marray = [model.pointList mutableCopy];
//    [marray removeObjectAtIndex:0];
//    [marray enumerateObjectsUsingBlock:^(HBDrawPoint *point, NSUInteger idx, BOOL *stop) {
//        [path pathLineToPoint:CGPointMake(point.x.floatValue * xp , point.y.floatValue * yp) WithType:HBDrawingShapeCurve];
//        [self.drawView setBrush:path];
//    }];
    
    self.userInteractionEnabled = YES;
    return YES;
}

- (void) revoke {
    if (mAPaths.count == 0) {
        [MLMessageTool showPromptMessage:@"没有可以回退的了"];
        return;
    }
    
    MLPath *lastpath = [mAPaths lastObject];
    [mABufPaths addObject:lastpath];
    [mAPaths removeLastObject];
    
    MLPath *path = [mAPaths lastObject];
    
    UIImage *getImage = [NSFileManager hb_getImageFileName:[MLThumbnailPath stringByAppendingPathComponent:path.imgPath]];
    _ivDraw.image = getImage;
}


#pragma mark - Touch
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event {
    CGPoint point = [[touches anyObject] locationInView:self];
    MLPath *path = [self path:point];
    [mAPaths addObject:path];
    [mAPoints addObject:[NSValue valueWithCGPoint:point]];
    
    if ([_delegate respondsToSelector:@selector(drawBoard:drawingStatus:model:)])
        [_delegate drawBoard:self drawingStatus:MLDrawingStatusBegin model:nil];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event {
    CGPoint point = [[touches anyObject] locationInView:self];
    MLPath *path = [mAPaths lastObject];
    [path pathToPoint:point type:_shapType];
    [mAPoints addObject:[NSValue valueWithCGPoint:point]];
    
    if (_isEraser) [self setEraseBrush:path];
    else [_vDraw setBrush:path];
    
    if ([_delegate respondsToSelector:@selector(drawBoard:drawingStatus:model:)])
        [_delegate drawBoard:self drawingStatus:MLDrawingStatusMove model:nil];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event {
    [self touchesMoved:touches withEvent:event];
    
    MLPath *path = [mAPaths lastObject];
    _ivDraw.image = [UIImage imageWithView:_ivDraw];
    [_vDraw setBrush:nil];
    
    NSData *data = UIImagePNGRepresentation(_ivDraw.image);
    NSString *filePath = [MLThumbnailPath stringByAppendingPathComponent:path.imgPath];
    BOOL isSave = [NSFileManager hb_saveData:data filePath:filePath];
    if (isSave) { NSAssert(true, path.imgPath); }
    
    MLDrawModel *model = [[MLDrawModel alloc] init];
    model.paintColor = [_colorPan toColorString];
    model.paintSize = @(_fWLine);
    model.isEraser = @(path.isEraser);
    model.shapType = @(_shapType);
    model.pointList = mAPoints;
    
    if ([_delegate respondsToSelector:@selector(drawBoard:drawingStatus:model:)])
        [_delegate drawBoard:self drawingStatus:MLDrawingStatusBegin model:model];
    
    [mAPoints removeAllObjects];
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event {
    
}

#pragma marl - tools
- (MLPath *) path:(CGPoint) point {
    MLPath *path = [[MLPath alloc] initWithPoint:point];
    path.colorPan = _colorPan ? _colorPan : [UIColor blackColor];
    path.isEraser = _isEraser;
    path.fWLine = _fWLine;
    path.imgPath = [NSString sssString];
    return path;
}

- (void) setEraseBrush:(MLPath *) path {
    UIGraphicsBeginImageContextWithOptions(self.frame.size, false, 0);
    
    [_ivDraw.image drawInRect:self.bounds];
    [[UIColor clearColor] set];
    [path.bp strokeWithBlendMode:kCGBlendModeClear alpha:1.0];
    [path.bp stroke];
    _ivDraw.image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
}


@end
