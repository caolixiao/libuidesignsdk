//
//  MLNavViewBar.h
//

#import <UIKit/UIKit.h>
#import "UIFont+MLResize.h"
#import "NSObject+MergeData.h"
#import "NSAttributedString+MLCustom.h"

typedef void(^MLNavViewBarClick)(int index);;

@class MLNavViewBar;
@protocol MLNavViewBarDelegate <NSObject>

@optional
- (void) MLNavViewBar:(MLNavViewBar *) target willSelectedIndex:(NSInteger) index;
- (void) MLNavViewBar:(MLNavViewBar *) target didSelectIndex:(NSInteger) index;

@end

@interface MLNavViewBar : UIView

typedef NS_ENUM(NSInteger, MLSelectState){
    MLSelectStateNormal      = UIControlStateNormal,
    MLSelectStateSelected    = UIControlStateSelected,
    MLSelectStateHighlighted = UIControlStateHighlighted,
};

//
@property (nonatomic, assign) id<MLNavViewBarDelegate> delegate;


@property (nonatomic, strong) UIFont *titleFont;

//导航栏的高度
@property (nonatomic, readonly) float height;

///他表示有多少标题 //目前必须设置
@property (nonatomic, readonly) NSUInteger toal;

// 设置选择na个
@property (nonatomic, assign) NSInteger selectedIndex;

// 设置是否显示下画线
@property (nonatomic, assign) BOOL isShowTopLine;
@property (nonatomic, assign) BOOL isShowBottomLine;
@property (nonatomic, strong) UIColor *colorLine;

// 设置风格线；加载完标题后在设置
@property (nonatomic, assign) BOOL isShowSplitLine;
@property (nonatomic, strong) UIColor *colorSplitLine;
@property (nonatomic, assign) CGFloat fH_Line; // 线的高度

@property (nonatomic, assign) BOOL isAttributedTitle;

@property (nonatomic, assign) CGFloat edge_width;   // 变宽
@property (nonatomic, assign) CGFloat fW_Edge; // 间距

@property (nonatomic, copy) MLNavViewBarClick didSelectAtIndex;

@property (nonatomic, strong) UIButton *current_Button;

// 设置是否有选择线
@property (nonatomic, assign, setter=setSelectLine:) BOOL isSelectLine;
@property (nonatomic, strong) UIColor *colorSelectLine;

/**
 * @brief 设置背影图片
 * @param img 设置的图片
 */
- (void) setBackgroundImage:(UIImage *) img;

/**
 * @brief 设置控件元素的标体颜色 (所有)
 * @param m_Color 默认为白色
 * @param state 他是一个状态标记显示的类型
 */
- (void) setContentColor:(UIColor *) m_Color state:(MLSelectState) state;

/**
 * @brief 设置控件元素的标体颜色 (所有)
 * @param m_Color 默认为白色
 * @param state 他是一个状态标记显示的类型
 */
- (void) setTitleColor:(UIColor *) m_Color state:(MLSelectState) state;

/**
 * @brief 设置控件元素的标题
 * @param m_Title 他是一个标题数组  toal 会根据数组的大小来设置 （MLSelectStateNormal 为标准）
 * @param state 他是一个状态标记显示的类型
 */
- (void) setTitles:(NSArray *) m_Title state:(MLSelectState) state;

/**
 * @brief 设置控件元素的标题颜色
 * @param m_Color 他是颜色数组 默认为白色
 * @param state 他是一个状态标记显示的类型
 */
- (void) setTitleColors:(NSArray *) m_Color state:(MLSelectState) state;

/**
 * @brief 设置控件元素的标题和颜色
 * @param m_Title 他是一个标题数组  toal 会根据数组的大小来设置 （MLSelectStateNormal 为标准）
 * @param m_Color 他是颜色数组 默认为白色
 * @param state 他是一个状态标记显示的类型
 */
- (void) setTitles:(NSArray *) m_Title color:(NSArray *) m_Color state:(MLSelectState) state;


/**
 * @brief 设置控件元素的背景
 * @param m_Title 他是一个背景图片数组
 * @param state 他是一个状态标记显示的类型
 */
- (void) setBackgroundImages:(NSArray *) m_Title state:(MLSelectState) state;

- (void) setRightImages:(NSArray *) m_Title state:(MLSelectState) state;

/**
 * @brief 设置控件的背景 (所有)
 * @param m_Title 如果没有设置 toal 会根据数组的大小来设置 （MLSelectStateNormal 为标准）
 * @param state 他是一个状态标记显示的类型
 */
- (void) setBackgroundImage:(UIImage *) m_Title state:(MLSelectState) state;

- (void) setRightImage:(NSString *) m_ImgPath state:(MLSelectState) state;

/** 
 * @brief 设置选择na个
 * @param index 设置选择（从0 开始 小于 toal）
 */
- (void) setSelectedIndex:(NSInteger) index;

- (void) setIndex:(int) index;

/**
 * @brief showLoadView 显示按扭界面
 * @return  返回load是否成功
 */
- (BOOL) showLoadView;


- (NSAttributedString *) contentWithTitle:(NSString *) title color:(UIColor *) color;
- (NSAttributedString *) contentWithTitle:(NSString *) title imgPath:(NSString *) path;
- (NSAttributedString *) contentWithTitle:(NSString *) title imgPath:(NSString *) path color:(UIColor *) color;
 

@end
