//
//  MLProgressView.swift
//

import UIKit
import LibBaseSDK

open class MLProgressView: UIProgressView {
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //        self.trackTintColor = [UIColor redColor];
        //        self.progressTintColor = [UIColor lightGrayColor];
        //        self.contentMode = UIViewContentModeScaleAspectFill;
        
        self.layer.cornerRadius = CGScale(1.5)
        self.layer.masksToBounds = true
        self.transform = CGAffineTransform(scaleX: 1, y: CGScale(3));
    }
}
