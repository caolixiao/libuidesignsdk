//
//  MLWebRightCell.h
//

#import <UIKit/UIKit.h>

@interface MLWebRightCell : UITableViewCell {
    @private
    
    UIImageView *ivPic;
    UILabel *labTitle;
}

@property (nonatomic, strong) UIImageView *ivPic;
@property (nonatomic, strong) UILabel *labTitle;

@end
