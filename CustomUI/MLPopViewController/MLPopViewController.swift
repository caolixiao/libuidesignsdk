//
//  MLPopViewController.swift
//

import UIKit
import LibBaseSDK

open class MLPopViewController: MLViewController {
    
    public enum MLPopStyle {
        case normal
        case rect
    }
    
    public var window : UIWindow?
    private var _style: MLPopViewController.MLPopStyle?
    
    public var vBkg : UIView = UIView.init()
    public var labTitle : UILabel = UILabel.init()
    public var butClose : UIButton = UIButton(type: .custom)
    public var vLine : UIView = UIView.init()

    open var frame: CGRect = CGRect(x: 0, y: __SCREEN_HEIGHT - CGScale(445), width: __SCREEN_WIDTH, height: CGScale(445))
    open var fH_head = CGScale(41)
    open var color_bkg : UIColor = color_f2f2f2
    open var isHiddenLine: Bool = false { didSet{ vLine.isHidden = isHiddenLine } }
    
    deinit {
        Log.i("deinit MLPopViewController")
    }
        
    override open func viewDidLoad() {
        super.viewDidLoad()
        
//        vBkg.backgroundColor = color_bkg
//        view.addSubview(vBkg)
        
        labTitle.font = UIFont.systemFont(ofBaseSize: 14)
        labTitle.textColor = color_2a2a2a
        view.addSubview(labTitle)
        
        butClose.setImage(UIImage(uidesignNamed: "nav_close_normal"), for: .normal)
        butClose.addTarget(self, action: #selector(action(close:)), for: .touchUpInside)
        butClose.contentEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        view.addSubview(butClose)
        
        vLine.backgroundColor = color_line
        view.addSubview(vLine)
    }
    
    override open func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        vBkg.frame = CGRect(x: 0, y: 0, width: VIEWW(self.view), height: fH_head)
        labTitle.frame = CGRect(x: CGScale(15), y: 0, width: CGScale(200), height: fH_head)
        butClose.frame = CGRect(x: VIEWW(self.view) - fH_head, y: 0, width: fH_head, height: fH_head)
        vLine.frame = CGRect(x: 0, y: fH_head - 0.5, width: VIEWW(self.view), height: 0.5)
    }
    
    
    open func reloadStyle() {
        switch _style {
        case .normal:
            view.layoutBorderRadius()
            break
        default:
            break
        }
    }
    
    // MARK: -
    @objc
    open func action(close but: UIButton) {
        hide()
    }
}


extension MLPopViewController {
    static public func show(vc: MLPopViewController, style: MLPopViewController.MLPopStyle = .normal) {
        window.frame = UIScreen.main.bounds
        window.windowLevel = UIWindow.Level.normal
        window.rootViewController = UIViewController()
        window.backgroundColor = UIColor.clear
        window.makeKeyAndVisible()
    
        vc.modalPresentationStyle = .fullScreen
        vc.view.backgroundColor = UIColor.clear
        vc.view.alpha = 0
        window.rootViewController?.present(vc, animated: false, completion: {
            let _v = UIView.init(frame: window.frame)
            _v.backgroundColor = .clear
            vc.push(hideView: _v, style: style)
            vc.view.superview?.insertSubview(_v, at: 0)
        })
    }

  
    public func push(hideView: UIView? = nil, style: MLPopViewController.MLPopStyle = .normal) {
        _style = style
        view.frame = frame
        
        reloadStyle()
        
        if hideView != nil {
            let tap = UITapGestureRecognizer(target: self, action: #selector(hide))
            hideView?.addGestureRecognizer(tap)
        }
        
        UIView.animate(withDuration: 0.23) {
            MLViewController.window.backgroundColor = __RGBA(0x000000, alpha: 0.4)
            self.view.backgroundColor = self.color_bkg
            self.view.alpha = 1
        }
    }
}
