//
//  MLChoiceMenuView.m
//

#import "MLChoiceMenuView.h"

@implementation MLChoiceModel

@end


@implementation MLChoiceMenuView

+ (instancetype) sharedInstance
{
    static MLChoiceMenuView *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [MLChoiceMenuView new];
    });
    
    return sharedInstance;
}

+ (void) showPickerData:(NSArray *)m_AData block:(PickerViewBlock) _complete {
    [[MLChoiceMenuView sharedInstance] showPickerData:m_AData block:_complete choice:nil];
}

+ (void) showOnePickerData:(NSArray *) m_AData block:(ChoiceBlock) complete {
    [[MLChoiceMenuView sharedInstance] showPickerData:m_AData block:nil choice:complete];
}

#pragma mark -
- (void) showPickerData:(NSArray *) _m_AData block:(PickerViewBlock) _complete choice:(ChoiceBlock) _choice
{
    m_AData = _m_AData;
    complete = _complete;
    choice = _choice;
    
    if (![m_AData isKindOfClass:[NSArray class]] || m_AData.count == 0) {
        NSAssert(true, @"=========");
        return;
    }

    comp = 0;
    [self comp:m_AData index:&comp];
    if (comp > 3) comp = 3;
    
    [_pickerView reloadAllComponents];
    
    UIView *window = [UIView getTopWindow];
    [window addSubview:self];
    
    _mainView.frame =CGRectMake(0, _backgroundView.bounds.size.height, _backgroundView.bounds.size.width, 270);
    [UIView animateWithDuration:0.3 animations:^{
        _mainView.frame =CGRectMake(0, _backgroundView.bounds.size.height-270, _backgroundView.bounds.size.width, 270);
        _backgroundView.alpha=1.0;
    }];
}

- (void) comp:(NSArray *) m_ATmp index:(int *) tmp{
    if (!m_ATmp || ![m_ATmp isKindOfClass:[NSArray class]] || m_ATmp.count == 0) {
        
    }else {
        *tmp+=1;
        MLChoiceModel *model = [m_ATmp firstObject];
        return [self comp:model.next index:tmp];
    }
}

#pragma mark -
- (instancetype)init
{
    if (self = [super init]) {
        [self buildView];
    }
    return self;
}

- (void) buildView {
    
    UIView *window = [UIView getTopWindow];
    self.frame = window.bounds;
    
    // ===========================================================
    _backgroundView = [[UIView alloc] initWithFrame:window.bounds];
    _backgroundView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    [self addSubview:_backgroundView];
    
    // ===========================================================
    _mainView =[[UIView alloc] initWithFrame:CGRectMake(0, _backgroundView.bounds.size.height, _backgroundView.bounds.size.width, 270)];
    _mainView.backgroundColor=__RGB(0xd6d6e0);
    [self addSubview:_mainView];
    
    // ===========================================================
    _pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0,0.0,_mainView.frame.size.width,220.0)];
    _pickerView.backgroundColor = [UIColor whiteColor];
    _pickerView.delegate=self;
    _pickerView.showsSelectionIndicator=YES;    //    显示选中框
    [_mainView addSubview:_pickerView];
    
    // ===========================================================
    _butCancel =[UIButton buttonWithType:UIButtonTypeCustom];
    _butCancel.frame=CGRectMake(0, _mainView.frame.size.height-44, _mainView.frame.size.width, 44);
    _butCancel.backgroundColor = [UIColor whiteColor];
    _butCancel.titleLabel.font=[UIFont systemFontOfSize:14];
    [_butCancel setTitle:@"确 定" forState:UIControlStateNormal];
    [_butCancel setTitleColor:COLOR_GENERAL forState:UIControlStateNormal];
    [_butCancel setTitleColor:[COLOR_GENERAL colorWithAlphaComponent:0.6] forState:UIControlStateHighlighted];
    [_butCancel addTarget:self action:@selector(actionCancelWithButton:) forControlEvents:UIControlEventTouchUpInside];
    [_mainView addSubview:_butCancel];
}

#pragma mark - action
- (void) actionCancelWithButton:(UIButton *) button {
    int row = (int)[_pickerView selectedRowInComponent:0];
    int row1 = (comp > 1) ? (int)[_pickerView selectedRowInComponent:1] : 0;
    int row2 = (comp > 2) ? (int)[_pickerView selectedRowInComponent:2] : 0;
    
    if(complete) complete(row, row1, row2, m_AData);
    complete = nil;
    
    if (choice) choice(m_AData[row]);
    choice = nil;
    
    [UIView animateWithDuration:0.3 animations:^{
        _mainView.frame =CGRectMake(0, _backgroundView.bounds.size.height, _backgroundView.bounds.size.width, 270);
        _backgroundView.alpha=0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark -
#pragma mark Picker Date Source Methods
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return comp;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return m_AData.count;
    }
    else if (component == 1)
    {
        MLChoiceModel *first = m_AData[[pickerView selectedRowInComponent:0]];
        return first.next.count;
    }
    else if (component == 2)
    {
        MLChoiceModel *first = m_AData[[pickerView selectedRowInComponent:0]];
        MLChoiceModel *second = first.next[[pickerView selectedRowInComponent:1]];
        return second.next.count;
    }

    return 0;
}

#pragma mark - Picker Delegate Methods
-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0) {
        MLChoiceModel *model = m_AData[row];
        return model.title;
    }
    else if (component == 1)
    {
        MLChoiceModel *first = m_AData[[pickerView selectedRowInComponent:0]];
        MLChoiceModel *model = first.next[row];
        return model.title;
    }
    else if (component == 2)
    {
        MLChoiceModel *first = m_AData[[pickerView selectedRowInComponent:0]];
        MLChoiceModel *second = first.next[[pickerView selectedRowInComponent:1]];
        MLChoiceModel *model = second.next[row];
        return model.title;
    }

    return nil;
}

-(void)pickerView:(UIPickerView *) pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (component == 0) {
        if (comp > 1){
            [pickerView reloadComponent:1];
            [pickerView selectRow:0 inComponent:1 animated:NO];
        }
        if (comp > 2) {
            [pickerView reloadComponent:2];
            [pickerView selectRow:0 inComponent:2 animated:NO];
        }
    }
    else if (component == 1)
    {
        if (comp > 2) {
            [pickerView reloadComponent:2];
            [pickerView selectRow:0 inComponent:2 animated:NO];
        }
    }
}

@end
