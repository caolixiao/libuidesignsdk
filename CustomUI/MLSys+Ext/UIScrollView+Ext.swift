//
//  UIScrollView+Ext.swift
//

import UIKit

extension UIScrollView {
    private struct AssociatedKey {
        static var tap = "ml.toby.scrollView.tap"
        static var ui = "ml.toby.scrollView.cancelui"
    }
    
    public var tapGestureRecognizer: UITapGestureRecognizer? {
        get { return objc_getAssociatedObject(self, &AssociatedKey.tap) as? UITapGestureRecognizer }
        set { objc_setAssociatedObject(self, &AssociatedKey.tap, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }
    
    public var cancelui: UIResponder? {
        get { return objc_getAssociatedObject(self, &AssociatedKey.ui) as? UIResponder }
        set { objc_setAssociatedObject(self, &AssociatedKey.ui, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }
    
    
    public func addKeyboardNotification(cancelsTouchesInView: Bool = false) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboard(willShow:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboard(willHide:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(keyboard(cancel:)))
        tapGestureRecognizer?.cancelsTouchesInView = cancelsTouchesInView
        superview?.addGestureRecognizer(tapGestureRecognizer!)
    }
    
    public func removeKeyboardNotification() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        
//        if let tap = tapGestureRecognizer {
//            keyboard(cancel: tap)
//            superview?.removeGestureRecognizer(tap)
//        }
    }
    
    
    //
    @objc
    public func keyboard(willShow notification: Notification) {
        if let info = notification.userInfo, let rect = info[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            contentInset = UIEdgeInsets(top: 0, left: 0, bottom: rect.size.height, right: 0)
            scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: rect.size.height, right: 0)
        }
    }
    
    @objc
    public func keyboard(willHide notification: Notification) {
        contentInset = .zero
        scrollIndicatorInsets = .zero
    }
    
    @objc
    public func keyboard(cancel tap: UITapGestureRecognizer) {
        self.endEditing(true)
        if let ui = cancelui, ui.isFirstResponder {
            ui.resignFirstResponder()
        }
    }
    
    // MARK: -
    override open func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return !pan(back: gestureRecognizer)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return pan(back: gestureRecognizer)
    }
    

    //location_X可自己定义,其代表的是滑动返回距左边的有效长度
    public func pan(back gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if gestureRecognizer == self.panGestureRecognizer, let pan = gestureRecognizer as? UIPanGestureRecognizer {
            let point = pan.translation(in: self)
            let state = gestureRecognizer.state
            if state == .began || state == .possible {
                let location = gestureRecognizer.location(in: self)
                if point.x > 0 && location.x < 40 && contentOffset.x <= 0 {
                    return true
                }
            }
        }
        return false;
    }
}
