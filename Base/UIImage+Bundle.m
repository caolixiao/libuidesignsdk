//
//  UIImage+boundle.m
//

#import "UIImage+Bundle.h"

@implementation UIImage (Bundle)

+ (UIImage *) initWithBundleName:(NSString *)boudleName imgName:(NSString *)imgName class:(Class) aClass {
    NSBundle *bundle = [NSBundle bundleForClass:aClass];
    NSURL *url = [bundle URLForResource:boudleName withExtension:@"bundle"];
    NSBundle *targetBundle = [NSBundle bundleWithURL:url];
    
    //    NSBundle *targetBundle = [NSBundle bundleWithPath:[[NSBundle bundleForClass:aClass] pathForResource:boudleName ofType:@"bundle"]];
    UIImage *image = [UIImage imageNamed:imgName inBundle:targetBundle compatibleWithTraitCollection:nil];
    return image;
}

@end
