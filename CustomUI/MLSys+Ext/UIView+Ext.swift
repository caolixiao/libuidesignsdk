//
//  UIView+Ext.swift
//

import UIKit

extension UIView {
    
    /// 部分圆角
    ///
    /// - Parameters:
    ///   - corners: 需要实现为圆角的角，可传入多个
    ///   - radii: 圆角半径
    open func corner(byRoundingCorners corners: UIRectCorner, radii: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radii, height: radii))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = maskPath.cgPath
        self.layer.mask = maskLayer
    }
    //全角
    open func corner(byRoundingRadii radii: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: radii)
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = maskPath.cgPath
        self.layer.mask = maskLayer
    }
    
    //全角
    open func corner(byRoundingRadii radii: CGFloat,borderColor:UIColor) {
        let maskPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: radii)
        let maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath

        maskLayer.fillColor = UIColor.clear.cgColor
        maskLayer.strokeColor = borderColor.cgColor
        maskLayer.lineWidth = 0.5
//        maskLayer.frame = self.bounds
        self.layer.mask = maskLayer
    }
    
    
    
    // MARK:- 绘制虚线
    open func drawDashLine(size: CGSize, strokeColor: UIColor, lineLength: Int = 10, lineSpacing: Int = 5) {
        self.clipsToBounds = true
        let shapeLayer = CAShapeLayer()
        shapeLayer.bounds = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        shapeLayer.anchorPoint = .zero
        shapeLayer.fillColor = UIColor.blue.cgColor
        shapeLayer.strokeColor = strokeColor.cgColor
        
        shapeLayer.lineWidth = size.height
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        
        // 每一段虚线长度 和 每两段虚线之间的间隔
        shapeLayer.lineDashPattern = [NSNumber(value: lineLength), NSNumber(value: lineSpacing)]
     
//      borderLayer.path = [UIBezierPath bezierPathWithRoundedRect:borderLayer.bounds cornerRadius:1].CGPath;
        let path = CGMutablePath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: size.width, y: 0))
        shapeLayer.path = path
        self.layer.addSublayer(shapeLayer)
    }
    
    func getSuperView(to cls: AnyClass, view: UIView?) -> UIView? {
        if let v = view?.superview {
            if v.isKind(of: cls) {
                return v
            } else {
                return getSuperView(to: cls, view: v)
            }
        }
        return nil
    }
}

