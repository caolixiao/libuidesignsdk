//
//  MLWebView.m
//

#import "MLWebView.h"
#import <WebKit/WebKit.h>

@interface MLWebView () <WKUIDelegate> {
    CGFloat estimatedProgress;
}

@end

@implementation MLWebView

@synthesize webView = _webView;
@synthesize delegate = _delegate;

@synthesize title;


-(void)dealloc {
    if([_webView isKindOfClass:[WKWebView class]]) {
        [_webView removeObserver:self forKeyPath:@"loading"];
        [_webView removeObserver:self forKeyPath:@"estimatedProgress"];
        [_webView removeObserver:self forKeyPath:@"title"];
        if(_isEmbed) [_webView.scrollView removeObserver:self forKeyPath:@"contentOffset"];
    }
}

- (instancetype) init {
    if(self = [super init]) {
        _webView = [[WKWebView alloc] init];
        _webView.navigationDelegate = self;
        
        [_webView addObserver:self forKeyPath:@"loading" options:NSKeyValueObservingOptionNew context:nil];
        [_webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
        [_webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
        
        _webView.scrollView.alwaysBounceVertical = false;
        _webView.scrollView.alwaysBounceHorizontal = false;
        
        estimatedProgress = 0.1;
        if([_delegate respondsToSelector:@selector(webViewLoadProgress:)])
            [_delegate webViewLoadProgress:estimatedProgress];
        
        [self addSubview:_webView];
    }
    return self;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    _webView.frame = self.bounds;
}

- (void) setIsEmbed:(BOOL) isEmbed {
    _isEmbed = isEmbed;
    
    if(isEmbed) {
        self.backgroundColor=[UIColor clearColor];
        _webView.opaque = NO;
        if([_webView isKindOfClass:[WKWebView class]]) {
            _webView.scrollView.scrollsToTop = NO;
            _webView.scrollView.scrollEnabled = NO;
            _webView.scrollView.bouncesZoom=NO;
            [_webView.scrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:nil];
        }
        else {
            [(UIScrollView*)[[_webView subviews] objectAtIndex:0] setBounces:NO];
            for (UIView *subView in _webView.subviews) {
                if ([subView isKindOfClass:[UIScrollView class]]) {
                    ((UIScrollView *)subView).scrollsToTop = NO;
                    ((UIScrollView *)subView).scrollEnabled = NO;
                }
            }
        }
    }
}

#pragma mark - kvo
- (void)observeValueForKeyPath:(NSString *) keyPath ofObject:(id) object change:(NSDictionary *) change context:(void *) context {
    if ([keyPath isEqualToString:@"contentOffset"]) {
        CGPoint changPoint = [change[@"new"] CGPointValue];
        if(changPoint.y!=0 || changPoint.x!=0)
            _webView.scrollView.contentOffset=CGPointMake(0, 0);
    }
    else if ([keyPath isEqualToString:@"loading"]) {
        
    }
    else if([keyPath isEqualToString:@"estimatedProgress"]) {
         WKWebView *webView= (WKWebView *)_webView;
        if(webView.estimatedProgress > 0.1) {
            if([_delegate respondsToSelector:@selector(webViewLoadProgress:)])
                [_delegate webViewLoadProgress:webView.estimatedProgress];
        }
    }
    else if([keyPath isEqualToString:@"title"]) {
        title = [(WKWebView *)_webView title];
    }
}

#pragma mark - super
- (void) setBackgroundColor:(UIColor*) bgColor {
    [super setBackgroundColor:bgColor];
    _webView.backgroundColor = bgColor;
}

#pragma mark -
- (void) loadHTMLString:(NSString *) string baseURL:(nullable NSURL *) baseURL
{
    if(string.length>0) [_webView loadHTMLString:string baseURL:baseURL];
}

- (void)loadRequest:(NSURLRequest *) request {
    [_webView loadRequest:request];
}

- (void)evaluateJavaScript:(NSString *)javaScriptString completionHandler:(void (^ __nullable)(__nullable id, NSError * __nullable error)) completionHandler {
    [_webView evaluateJavaScript:javaScriptString completionHandler:completionHandler];
}

#pragma mark - WKUIDelegate
- (void) webView:(WKWebView *) webView didFinishNavigation:(WKNavigation *)navigation {
    if([_delegate respondsToSelector:@selector(webViewDidFinishLoad:)])
        [_delegate webViewDidFinishLoad:self];
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    
    if([error code] == NSURLErrorCancelled) return;
    if([_delegate respondsToSelector: @selector(webView:didFailLoadWithError:)])
        [_delegate webView:self didFailLoadWithError:error];
    
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    if([self shouldStartLoadWithRequest: navigationAction.request])
        decisionHandler(WKNavigationActionPolicyAllow);
    else
        decisionHandler(WKNavigationActionPolicyCancel);
}

- (BOOL) shouldStartLoadWithRequest:(NSURLRequest *)request
{
    NSString* scheme = request.URL.scheme;
    _pathURL = request.URL.absoluteString;
//    NSString *resourceSpecifier = [[request URL] resourceSpecifier];
    
    if([request.URL.host rangeOfString:@"itunes.apple.com"].length>0) {
        BOOL result= [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_pathURL]];
        if([_delegate respondsToSelector:@selector(navigationToAppStore:completion:)])
            [_delegate navigationToAppStore:_pathURL completion:result];
        return NO;
    }
    
    if(_scriptSchemes.count == 0) {
        if([scheme isEqualToString:@"http"]) return YES;
        if([scheme isEqualToString:@"https"]) return YES;
        if([scheme isEqualToString:@"file"]) return YES;
        
        if([_delegate respondsToSelector:@selector(webViewDidReceiveScriptMessage:scheme:)])
            return [_delegate webViewDidReceiveScriptMessage:_pathURL scheme:(NSString*)scheme];
        
        return YES;
    }
    
    for(NSString* _scheme in _scriptSchemes)
    {
        if ([scheme hasPrefix:_scheme]) {
            if([_delegate respondsToSelector:@selector(webViewDidReceiveScriptMessage:scheme:)])
                return [_delegate webViewDidReceiveScriptMessage:_pathURL scheme:(NSString*)_scheme];
            return NO;
        }
    }
    
    return YES;
}

@end
