//
//  MLWebConsole.m
//

#import "MLWebConsole.h"
#import <objc/runtime.h>

@implementation MLWebConsole

+ (void) loadMLClass:(Class) cl {
    SEL nSel = @selector(canInitWithRequest:);
    Method new = class_getClassMethod(cl, nSel);
    Method old = class_getClassMethod([MLWebConsole class], nSel);
//    class_getInstanceMethod()
    method_exchangeImplementations(old, new);
}

@end
