//
//  MLCyclePageView.h
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MLCyclePageHeadViewType) {
    MLCyclePageHeadViewTypeNone = 1,
    MLCyclePageHeadViewTypeNumber,
    MLCyclePageHeadViewTypePage,
    MLCyclePageHeadViewTypeDefault = MLCyclePageHeadViewTypeNumber,
};

typedef NS_ENUM(NSInteger, MLCyclePageViewPageStatus) {
    MLCyclePageViewPageStatusNone = 1,
    MLCyclePageViewPageStatusPre,
    MLCyclePageViewPageStatusNext,
};

@class MLCyclePageView, MLExtPageControl;
@protocol MLCyclePageViewDataSource <NSObject>

- (NSUInteger) numberOfItemsInCyclePageView:(MLCyclePageView *) cyclePageView;
- (UIView *) cyclePageView:(MLCyclePageView *) cyclePageView viewForItemAtIndex:(NSUInteger) index resetView:(UIView *) view;

@optional

// LXTableViewCellTyleDefault
- (MLCyclePageHeadViewType) cyclePageViewTypeInTarget:(MLCyclePageView *) cyclePageView;

// default YES
- (BOOL) cyclePageView:(MLCyclePageView *) cyclePageView shadowAtIndex:(NSUInteger) index;

@end

@interface MLCyclePageView : UIView
{
    @private
    UIView *vHead;
    UILabel *labHeadInfo;
    MLExtPageControl *pcHeadInfo;
    
    UIView *contentView;
    UIView *preView;
    UIView *currentView;
    UIView *nextView;
    
    NSTimer *timer;
    BOOL decelerating;
    MLCyclePageViewPageStatus pageStatus;
    CGFloat head_height;
    CGFloat perspective;
    
    CGFloat beganPointX;
    CGFloat startPointX;
    CGFloat scrollOffset;
    
    NSTimeInterval startTime;
    NSTimeInterval scrollDuration;
}

@property (nonatomic, assign) id <MLCyclePageViewDataSource> dataSource;

@property (nonatomic, strong, readonly) UIView *contentView;

@property (nonatomic, assign, readonly) NSUInteger numberOfItems;
@property (nonatomic, assign) NSInteger currentNumberOfItem;
@property (nonatomic, strong, readonly) NSMutableDictionary *m_Items;

@property (nonatomic, assign, readonly) MLCyclePageHeadViewType headType;

//// head info
@property (nonatomic, assign) UIFont *titleFont;
@property (nonatomic, strong) UIColor *titleColor;

@property (nonatomic, assign) BOOL scrollEnabled;

@property (nonatomic, strong, readonly) UIView *currentView;

// 刷新数据
- (void) reloadData;

@end


@interface UIView (shadow)

@property (nonatomic, assign) CGFloat radius;
@property (nonatomic, strong) NSNumber *isRadiusOfObj;
- (void) setShadow:(BOOL) shadow;

@end
