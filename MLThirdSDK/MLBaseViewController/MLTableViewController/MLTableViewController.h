//
//  MLTableViewController.h
//

#import "MLBaseViewController.h"


#define MLISLOADDZNEmptyDataSet     // 加上是加载null
#define MLISLOADMJREFRESH           // 是否加载刷新

static NSString *const kIdentify_cell     = @"UITableViewCell";
static NSString *const kIdentify_header   = @"UITableViewHeaderFooterView-header";
static NSString *const kIdentify_footer   = @"UITableViewHeaderFooterView-footer";


#ifdef MLISLOADMJREFRESH
@protocol ListViewControllerRefreshDelegate <NSObject>

@optional
- (void) listViewController:(id) target loadDataAtPage:(int) page;

@end

#endif


@interface MLTableViewController : MLBaseViewController <UITableViewDelegate, UITableViewDataSource>
{
    @public
    UITableView *_tableView;
    
    NSMutableArray *m_AData;
    
    @package
    // 刷新数据用
    int page;
    BOOL isNotMore;
    
    @private
    BOOL isSuper;
}

@property (nonatomic, strong, readonly) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *m_AData;

@property (nonatomic, assign) int page;


/**
 * 设置UITableViewStyle的类型，默认UITableViewStylePlain
 *
 */
- (UITableViewStyle)tableViewStyle;


#ifdef MLISLOADDZNEmptyDataSet

#pragma mark -
- (void) loadNULLDataView;
- (void) removeNULLDataView;

#endif


#ifdef MLISLOADMJREFRESH

#pragma mark - begin 适用于列表加载
@property (nonatomic, weak) id<ListViewControllerRefreshDelegate> refreshDelegate;

// 注册刷新的view
- (void) loadRefreshView;


// 开始刷新 后掉用自己的或者代理的 - (void) listViewController:(id) target loadDataAtPage:(int) page;
- (void) beginRefreshing;

// 结束刷新
- (void) endRefreshing;

// 是否正在加载数据
- (BOOL) isLoadingData;

// 网络回来数据后主线程掉用
- (void) loadGetData;

/**
 *  @brief 网络加载完数据后掉用 需要覆盖使用
 *  子类方法覆盖
 */
- (void) reloadData;
- (BOOL) reloadDataWithTotal:(int) total num:(int) num; //__attribute((deprecated("这个接口 不建议使用"))); // 设置自己是否加载更多
- (void) reloadLastData; // 刷新完界面后调用
- (BOOL) reloadMoreData;


/**
 *  @brief 刷新数据是掉用改方法来联网 需要覆盖使用
 *  @seek 需要自己结束刷新动画
 *  @param target 自己本身
 *  @param page 加载的页数
 */
- (void) listViewController:(id) target loadDataAtPage:(int) page;
#pragma mark end 适用于列表加载
#pragma mark -

#endif


@end
