//
//  MLQRDEcoder.m
//

#import "MLQRDEcoder.h"
#import <AVFoundation/AVFoundation.h>
#import <MLGeneralLibary/MLGlobalHeader.h>

@interface MLQRDEcoder() <AVCaptureMetadataOutputObjectsDelegate, UIAlertViewDelegate> {
    AVCaptureSession * session;
    AVCaptureMetadataOutput * output;
    MLQRDEcoderOutputBlock handler;
}

@end


@implementation MLQRDEcoder

@synthesize delegate;


- (void) dealloc {
    [session removeObserver:self forKeyPath:@"running" context:nil];
}

- (instancetype) initWithFrame:(CGRect) frame {
    if (self = [super initWithFrame:frame]) {
        //获取摄像设备
        AVCaptureDevice * device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        //创建输入流
        AVCaptureDeviceInput * input = [AVCaptureDeviceInput deviceInputWithDevice:device error:nil];
        //创建输出流
//        AVCaptureMetadataOutput * output = [[AVCaptureMetadataOutput alloc] init];
        output = [[AVCaptureMetadataOutput alloc] init];
        //设置代理 在主线程里刷新
        [output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
        //初始化链接对象
        session = [[AVCaptureSession alloc] init];
        //高质量采集率
        [session setSessionPreset:AVCaptureSessionPresetHigh];
        
        if (input) [session addInput:input];
        else INFO_NSLOG(@"相机可能不能用");

        if (output) {
            [session addOutput:output];
            //设置扫码支持的编码格式(如下设置条形码和二维码兼容)
            NSMutableArray *a = [[NSMutableArray alloc] init];
            if ([output.availableMetadataObjectTypes containsObject:AVMetadataObjectTypeQRCode]) {
                [a addObject:AVMetadataObjectTypeQRCode];
            }
            if ([output.availableMetadataObjectTypes containsObject:AVMetadataObjectTypeEAN13Code]) {
                [a addObject:AVMetadataObjectTypeEAN13Code];
            }
            if ([output.availableMetadataObjectTypes containsObject:AVMetadataObjectTypeEAN8Code]) {
                [a addObject:AVMetadataObjectTypeEAN8Code];
            }
            if ([output.availableMetadataObjectTypes containsObject:AVMetadataObjectTypeCode128Code]) {
                [a addObject:AVMetadataObjectTypeCode128Code];
            }
            output.metadataObjectTypes = a;
        }
        
        AVCaptureVideoPreviewLayer * layer = [AVCaptureVideoPreviewLayer layerWithSession:session];
        layer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        layer.frame = self.layer.bounds;
        [self.layer insertSublayer:layer atIndex:0];
        
        [session addObserver:self forKeyPath:@"running" options:NSKeyValueObservingOptionNew context:nil];

    }
    return self;
}

- (void) startWithCaptureOutput:(MLQRDEcoderOutputBlock) _handler {
    handler = _handler ? [_handler copy] : nil;
    [session startRunning];
}

- (void) start {
    [session startRunning];
}

- (void) stop {
    [session stopRunning];
}

- (void) setRectOfInterest:(CGRect) rect {
    [output setRectOfInterest:rect];
}

- (void) captureOutput:(AVCaptureOutput *) captureOutput didOutputMetadataObjects:(NSArray *) metadataObjects fromConnection:(AVCaptureConnection *) connection {
    if (metadataObjects.count>0) {
        [session stopRunning];
        AVMetadataMachineReadableCodeObject * metadataObject = [metadataObjects objectAtIndex :0];
        NSString *data = metadataObject.stringValue;
        if (handler) handler(data);
        if ([delegate respondsToSelector:@selector(qrDEcoder:result:)])
            [delegate qrDEcoder:self result:data];
    }
}

#pragma mark -  kvo  session -> running
- (void) observeValueForKeyPath:(NSString *) keyPath ofObject:(id) object change:(NSDictionary *) change context:(void *) context
{
    if ([object isKindOfClass:[AVCaptureSession class]]) {
        if([delegate respondsToSelector:@selector(qrDEcoder:isRunning:)])
            [delegate qrDEcoder:self isRunning:((AVCaptureSession *)object).isRunning];
    }
}

@end
