//
//  MLQRDEcoder.h
//


#import <Foundation/Foundation.h>

typedef void (^MLQRDEcoderOutputBlock)(NSString *value);

@class MLQRDEcoder;
@protocol MLQRDEcoderDelegate <NSObject>

- (void) qrDEcoder:(MLQRDEcoder *) qrDEcoder isRunning:(BOOL) running;
- (void) qrDEcoder:(MLQRDEcoder *) qrDEcoder result:(NSString *) result;

@end

@interface MLQRDEcoder : UIView

@property (nonatomic, weak) id<MLQRDEcoderDelegate> delegate;

- (void) setRectOfInterest:(CGRect) rect;

- (void) startWithCaptureOutput:(MLQRDEcoderOutputBlock) handler;
- (void) start;
- (void) stop;

@end
