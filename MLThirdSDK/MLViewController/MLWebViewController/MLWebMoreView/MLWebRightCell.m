//
//  MLWebRightCell.m
//

#import "MLWebRightCell.h"
#import <MLGeneralLibary/MLGlobalHeader.h>

@implementation MLWebRightCell

@synthesize ivPic;
@synthesize labTitle;

- (instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        ivPic = [[UIImageView alloc] init];
        ivPic.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:ivPic];
        
        labTitle = [[UILabel alloc] init];
        labTitle.textColor = COLOR_WORD;
        labTitle.font = [UIFont systemFontOfBaseSize:13];
        [self.contentView addSubview:labTitle];
    }
    return self;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    CGFloat fEdge = CGScale(8);
    CGFloat fH_img = VIEWH(self) - 2*fEdge;
    ivPic.frame = CGRectMake(fEdge, fEdge, fH_img, fH_img);
    
    labTitle.frame = CGRectMake(VIEWMaxX(ivPic) + fEdge, 0, VIEWW(self) - VIEWMaxX(ivPic) - 2*fEdge, VIEWH(self));
}




@end
