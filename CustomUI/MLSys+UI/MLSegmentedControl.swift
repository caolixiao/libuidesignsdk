//
//  MLSegmentedControl.swift
//

import UIKit

open class MLSegmentedControl: UISegmentedControl {

    public func segmentedIOS13Style() {
    
        // iOS13 及其以上系统运行
        if #available(iOS 13, *) {

            let tintColor: UIColor = self.tintColor
            
            let tintColorImage: UIImage = self.from(color: tintColor)
            
            self.setBackgroundImage(self.from(color: self.backgroundColor ?? .clear), for: .normal, barMetrics: .default)
            
            self.setBackgroundImage(tintColorImage , for: .selected, barMetrics: .default)
            
            self.setBackgroundImage(self.from(color: tintColor.withAlphaComponent(0.2)) , for: .highlighted, barMetrics: .default)

            self.setBackgroundImage(tintColorImage , for: .selected, barMetrics: .default)

            self.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tintColor, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)], for: .normal)

            self.setDividerImage(tintColorImage, forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)

            self.layer.borderWidth = 1
            
            self.layer.borderColor = tintColor.cgColor
            
            self.selectedSegmentTintColor = tintColor
            
        }
  
    }
    
    public func from(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img ?? UIImage()
    }

}
