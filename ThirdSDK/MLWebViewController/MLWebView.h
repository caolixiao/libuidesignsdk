//
//  MLWebView.h
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>


@class MLWebView;

@protocol MLWebViewDelegate <NSObject>

- (void) webViewDidFinishLoad:(nullable MLWebView *) webView;
- (void) webView:(MLWebView *_Nonnull) webView didFailLoadWithError:(NSError *_Nonnull) error;

@optional
- (BOOL) webViewDidReceiveScriptMessage:(nullable NSString *) msg scheme:(nullable NSString *) scheme;
- (void) navigationToAppStore:(nullable NSString *) url completion:(BOOL) result;
- (void) webViewLoadProgress:(CGFloat) estimatedProgress;

@end

@interface MLWebView : UIView

@property (nonatomic, nullable, readonly) WKWebView *webView;
@property (nonatomic, weak, nullable) id<MLWebViewDelegate> delegate;

@property (nonatomic, nullable, strong) NSArray *scriptSchemes;
@property (nonatomic, nullable, readonly) NSString *title;
@property (nonatomic, nullable, readonly) NSString *pathURL;
@property (nonatomic, nullable, copy) NSString *script;
@property (nonatomic, assign) BOOL isEmbed;

- (void)loadHTMLString:(nullable NSString *) string baseURL:(nullable NSURL *) baseURL;
- (void)loadRequest:(nullable NSURLRequest *) request;
- (void)evaluateJavaScript:(nullable NSString *)javaScriptString completionHandler:(void (^ __nullable)(__nullable id, NSError * __nullable error))completionHandler;


@end
