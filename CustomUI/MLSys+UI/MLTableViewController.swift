//
//  MLTableViewController.swift
//

import UIKit
import LibBaseSDK
import MJRefresh

open class MLTableViewController: MLViewController {
    
    private var _tableView: UITableView = UITableView.init(frame: .zero, style: .plain)
    
    public let _identifier_cell = "_identifier_cell_base"
    public var isShowRefresh: Bool = false {
           didSet {
               if isShowRefresh {
                   _tableView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [weak self] in
                       self?.page = 1
                       self?.mAData.removeAll()
                       self?._tableView.mj_footer?.resetNoMoreData()
                       self?.loadListData();
                   })
                   _tableView.mj_header?.beginRefreshing();
               }
               else {
                   _tableView.mj_header?.endRefreshing()
                   _tableView.mj_header?.removeFromSuperview()
               }
           }
       }
    public var isShowMore: Bool = false {
        didSet {
            if isShowMore {
                _tableView.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: {  [weak self] in
                    let val = self?.page ?? 1
                    self?.page = val + 1;
                    self?.loadListData();
                })
            }
            else {
                _tableView.mj_footer?.endRefreshing()
                _tableView.mj_footer?.removeFromSuperview()
            }
        }
    }
//    @convention
    public var page: Int = 1
    public var mAData: [Any] = [Any]() {
        didSet {
            _tableView.reloadData()
            isHiddenNull = mAData.count > 0
        }
    }
    public private(set) var tableView: UITableView?

    deinit {
        Log.i("deinit MLTableViewController")
    }

    required public init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public init() {
        super.init()
        tableView = _tableView
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        buildTableView()
    }
    
    override open func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        _tableView.frame = view.bounds
    }
    
    
    // MARK: - build view
    open func buildTableView() {
        _tableView.backgroundColor = color_f2f2f2
        _tableView.separatorStyle = .none
        view.addSubview(_tableView)
    }
    
    open func loadListData() {
        
        
    }
    
    open func reloadRefreshData(_ isNoMore: Bool = true) {
        isHiddenNull = true
        if mAData.count == 0 {
            isHiddenNull = false
        } else {
            isShowMore = true
        }
        
        _tableView.reloadData()
        _tableView.mj_header?.endRefreshing()
        isNoMore ? _tableView.mj_footer?.endRefreshingWithNoMoreData() :  _tableView.mj_footer?.endRefreshing()
        _tableView.mj_footer?.isHidden = mAData.count == 0
    }
    
    open func refreshing() {
        tableView?.mj_header?.beginRefreshing()
    }
}
