//
//  MLEditImgView.h
//

#import <UIKit/UIKit.h>

@class MLEditImgView;
typedef void (^HiddenControl) (MLEditImgView *eView, BOOL isHidden);

@interface MLEditImgView : UIView {
    @private
    UIButton *butDel;
    UIButton *butRotate;
    UIButton *butZoom;
    
    UIImageView *iv;
}

@property (nonatomic, strong) UIImage *img;
@property (nonatomic, assign) BOOL hiddenControl;
@property (nonatomic, strong) HiddenControl hid;


@end
