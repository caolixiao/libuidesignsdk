//
//  UIButton+Ext.swift
//

import UIKit
import LibBaseSDK

extension UIButton {
    
    public func setBackgroundColor(_ color: UIColor?, for state: UIControl.State) {
        self.setBackgroundImage(UIImage(color: color), for: state)
    }
    
    
    public func setStyle(style: MLButton.Style) {
        switch style {
        case .next:
            layer.masksToBounds = true
            layer.cornerRadius = 4.0
            titleLabel?.font = UIFont.systemFont(ofBaseSize: 16)
            setTitleColor(color_white, for: .normal)
            setTitleColor(color_white.withAlphaComponent(0.4), for: .disabled)
            setTitleColor(color_white.withAlphaComponent(0.4), for: .selected)
            setBackgroundColor(color_main, for: .normal)
            setBackgroundColor(color_main.withAlphaComponent(0.6), for: .disabled)
            setBackgroundColor(color_main.withAlphaComponent(0.6), for: .selected)
            break
        case .border:
            layer.masksToBounds = true
            layer.cornerRadius = 4.0
            layer.borderWidth = 0.5
            layer.borderColor = color_line.cgColor;
            titleLabel?.font = UIFont.systemFont(ofBaseSize: 16)
            setTitleColor(color_white, for: .normal)
            setTitleColor(color_white.withAlphaComponent(0.4), for: .disabled)
            setTitleColor(color_white.withAlphaComponent(0.4), for: .selected)
            setBackgroundColor(color_main, for: .normal)
            setBackgroundColor(color_main.withAlphaComponent(0.6), for: .disabled)
            setBackgroundColor(color_main.withAlphaComponent(0.6), for: .selected)
            break
        case .upperLower:
            titleLabel?.font = UIFont.systemFont(ofBaseSize: 13)
            contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
            
            let fW_title = titleLabel?.frame.size.width ?? 0
            let fH_title = titleLabel?.frame.size.height ?? 0
            let fW_img = imageView?.frame.size.width ?? 0
            let fH_img = imageView?.frame.size.height ?? 0
            
            titleEdgeInsets = UIEdgeInsets(top: VIEWH(self) - fH_title, left: -fW_img, bottom: 0, right: 0)
            imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: fH_img, right: -fW_title)
            break
        case .borderLine:
            layer.masksToBounds = true
            layer.cornerRadius = 5.0
            layer.borderWidth = 1
            layer.borderColor = color_main.cgColor;
            titleLabel?.font = UIFont.systemFont(ofBaseSize: 18)
            setTitleColor(color_main, for: .normal)
            setTitleColor(color_main.withAlphaComponent(0.4), for: .disabled)
            setTitleColor(color_main.withAlphaComponent(0.4), for: .selected)
            break
        default:
            break
        }
    }
    
}
