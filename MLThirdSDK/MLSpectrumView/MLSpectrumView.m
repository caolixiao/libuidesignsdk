//
//  MLSpectrumView.m
//

#import "MLSpectrumView.h"

@interface MLSpectrumView () {
    NSMutableArray * m_ALevel;
    NSMutableArray * m_AItem;
    
    CGFloat fH_Item;
    CGFloat fW_Item;
}

@end

@implementation MLSpectrumView

//    CADisplayLink *displaylink = [CADisplayLink displayLinkWithTarget:self selector:@selector(invoke)];
//    displaylink.frameInterval = 6;
//    [displaylink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];

- (instancetype) init {
    if (self = [super init]) {
        _numberOfItems = 10;
    }
    return self;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    fH_Item = CGRectGetHeight(self.bounds);
    fW_Item  = CGRectGetWidth(self.bounds);
    
    for (CAShapeLayer *itemline in m_AItem)
        itemline.lineWidth = fW_Item*0.4/_numberOfItems;
    
    self.level = -160;
}

- (void) startAudioValue {
    m_AItem = [[NSMutableArray alloc] initWithCapacity:0];
    m_ALevel = [[NSMutableArray alloc] initWithCapacity:0];
    
    for(int i = 0; i < _numberOfItems; i++) {
        CAShapeLayer *itemline = [CAShapeLayer layer];
        itemline.lineCap       = kCALineCapButt;
        itemline.lineJoin      = kCALineJoinRound;
        itemline.strokeColor   = [UIColor clearColor].CGColor;
        itemline.fillColor     = [UIColor clearColor].CGColor;
        itemline.strokeColor   = _itemColor ? _itemColor.CGColor : [UIColor colorWithRed:241/255.f green:60/255.f blue:57/255.f alpha:1.0].CGColor;
        itemline.lineWidth     = fW_Item*0.4/_numberOfItems;
        
        [self.layer addSublayer:itemline];
        [m_AItem addObject:itemline];
        [m_ALevel addObject:@(1)];
    }

    [self updateItems];
}

- (void)setLevel:(CGFloat)level {
    level = (level+37.5) * 3.2;
    if( level < 0 ) level = 0;
    
    [m_ALevel removeObjectAtIndex:_numberOfItems - 1];
    [m_ALevel insertObject:@((level / 6) < 1 ? 1 : level / 6) atIndex:0];
    
    [self updateItems];
}

- (void)updateItems {
    UIGraphicsBeginImageContext(self.frame.size);
    
    int x = fW_Item*0.8/_numberOfItems;
    int z = fW_Item*0.2/_numberOfItems;
    int y = fW_Item*0 - z;
    
    for(int i = 0; i < _numberOfItems; i++) {
        UIBezierPath *itemLinePath = [UIBezierPath bezierPath];
        
        y += x;
        [itemLinePath moveToPoint:CGPointMake(y, fH_Item/2 + ([[m_ALevel objectAtIndex:i]intValue]+1)*z/2)];
        [itemLinePath addLineToPoint:CGPointMake(y, fH_Item/2-([[m_ALevel objectAtIndex:i]intValue]+1)*z/2)];
        
        CAShapeLayer *itemLine = [m_AItem objectAtIndex:i];
        itemLine.path = [itemLinePath CGPath];
    }
    
    UIGraphicsEndImageContext();
}

@end
