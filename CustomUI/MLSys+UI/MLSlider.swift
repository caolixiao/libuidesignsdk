//
//  MLSlider.swift
//

import UIKit

open class MLSlider: UISlider {
    
    override open func thumbRect(forBounds bounds: CGRect, trackRect rect: CGRect, value: Float) -> CGRect {
        var from = super.thumbRect(forBounds: bounds, trackRect: rect, value: value)
        if value <= 0 { from.origin.x = bounds.origin.x - 5 }
        else if value >= 0 { from.origin.x = bounds.origin.x + 5 }
        return from
    }
}
