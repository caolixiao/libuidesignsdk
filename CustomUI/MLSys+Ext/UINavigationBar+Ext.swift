//
//  UINavigationBar+Ext.swift
//

import UIKit
import LibBaseSDK

extension UINavigationBar {
        
    @objc
    open func setDefaultBackground() {
        setNavBar(bkgColor: color_nav_bg, lineColor: color_nav_line, wordColor: color_nav_title)
    }
    
    public func setNavBar(bkgColor: UIColor, lineColor: UIColor, wordColor: UIColor) {
        backgroundColor = bkgColor
        background(image: UIImage(color: bkgColor))
        shadowImage = UIImage(color: lineColor, size: CGSize(width: __SCREEN_WIDTH, height: 0.5))
//        UIBarButtonItem.appearance().image = UIImage(color: lineColor, size: CGSize(width: __SCREEN_WIDTH, height: 0.5))
        
        let fontSize: CGFloat = __DEVICE_IPAD ? 24 : 21
        let font = UIFont.systemFont(ofBaseSize: fontSize)
        setNavBar(titleColor: wordColor, font: font, shadowColor: .clear, shadowOffSet: .zero)
    }
    
    public func setNavBar(titleColor: UIColor, font: UIFont? = nil, shadowColor: UIColor? = nil, shadowOffSet: CGSize? = nil) {
        UINavigationBar.appearance().tintColor = titleColor
        var attributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key: Any]()
        attributes.updateValue(titleColor, forKey: NSAttributedString.Key.foregroundColor)
        if let f = font { attributes.updateValue(font, forKey: NSAttributedString.Key.font) }
        
        let shadow = NSShadow()
        shadow.shadowColor = shadowColor
        if let offSet = shadowOffSet { shadow.shadowOffset = offSet }
        attributes.updateValue(shadow, forKey: NSAttributedString.Key.shadow)
        
        titleTextAttributes = attributes
        
        // 设置字体颜色
//        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(.zero, for: UIBarMetrics.default)
//        UIBarButtonItem.appearance().setTitleTextAttributes(attributes, for: .normal)
    }

    public func background(image: UIImage) {
        self.isTranslucent = false
        setBackgroundImage(image, for: UIBarPosition.topAttached, barMetrics: UIBarMetrics.default)
        setBackgroundImage(image, for: UIBarMetrics.default)
    }
}
