//
//  UINavigationController+Ext.swift
//

import UIKit

extension UINavigationController {

    public func pushRoot(viewController: UIViewController, animated: Bool) {
        var mANav = viewControllers
        mANav.removeAll()
        mANav.append(viewController)
        setViewControllers(mANav, animated: animated)
    }
    
    public func push(viewController: UIViewController, to cls: AnyClass, animated: Bool) {
        var mANav = viewControllers
        for vc in mANav {
            if mANav.last?.isKind(of: cls) ?? true { mANav.removeLast(); break }
            mANav.removeLast()
        }
        
        mANav.append(viewController)
        setViewControllers(mANav, animated: animated)
    }
    
    public func push(viewController: UIViewController, remove cls: AnyClass, animated: Bool) {
        var mANav = viewControllers
        if  mANav.last?.isKind(of: cls) ?? false {
            mANav.removeLast()
        }
        
        mANav.append(viewController)
        setViewControllers(mANav, animated: animated)
    }
    
    public func push(viewControllers: [UIViewController], to cls: AnyClass, animated: Bool) {
        var mANav = viewControllers
        for vc in mANav {
            if mANav.last?.isKind(of: cls) ?? true { mANav.removeLast(); break }
            mANav.removeLast()
        }
        
        mANav.append(contentsOf: viewControllers)
        setViewControllers(mANav, animated: animated)
    }
    
    public func push(viewControllers: [UIViewController], remove cls: AnyClass, animated: Bool) {
        var mANav = viewControllers
        if  mANav.last?.isKind(of: cls) ?? false {
            mANav.removeLast()
        }
        
        mANav.append(contentsOf: viewControllers)
        setViewControllers(mANav, animated: animated)
    }
    
    
    public func pop(toViewController cls: AnyClass, animated: Bool) {
        for vc in viewControllers {
            if vc.isKind(of: cls) {
                popToViewController(vc, animated: animated)
                break
            }
        }
    }
    
    public func popTwo(animated: Bool) {
        let count = viewControllers.count - 3
        if count >= 0 {
            let vc = viewControllers[count]
            popToViewController(vc, animated: animated)
        }
        else {
            popViewController(animated: animated)
        }
    }
    
    public func isPop(viewController: UIViewController) -> Bool {
        return !viewControllers.contains(viewController)
    }
}
