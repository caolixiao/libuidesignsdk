//
//  UITextView+Ext.swift
//

import UIKit
import LibBaseSDK

extension UITextView {
    
    private struct AssociatedKey {
        static var placeholder = "ml.toby.textView.placeholder"
        static var labPlaceholder = "ml.toby.textView.labPlaceholder"
    }
    
    public var placeholder: String? {
        get { return objc_getAssociatedObject(self, &AssociatedKey.placeholder) as? String }
        set {
            objc_setAssociatedObject(self, &AssociatedKey.placeholder, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            setPlaceholder()
        }
    }
    
    public var labPlaceholder: UILabel? {
        get { return objc_getAssociatedObject(self, &AssociatedKey.labPlaceholder) as? UILabel }
        set { objc_setAssociatedObject(self, &AssociatedKey.labPlaceholder, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }
    
    public func sizeToPlaceholder() {
        labPlaceholder?.frame = CGRect(x: 0, y: 2, width: 300, height: CGScale(16))
    }
    
    private func setPlaceholder() {
        if let lab = labPlaceholder {
            
        } else {
            labPlaceholder = UILabel()
            labPlaceholder?.isExclusiveTouch = true
            labPlaceholder?.backgroundColor = UIColor.clear
            labPlaceholder?.font = UIFont.systemFont(ofBaseSize: 14)
            labPlaceholder?.textColor = __RGB(0xb2b2b2)
            labPlaceholder?.numberOfLines = 1
            addSubview(labPlaceholder!)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(action(didBeginEditing:)), name: UITextView.textDidBeginEditingNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(action(didEndEditing:)), name: UITextView.textDidEndEditingNotification, object: nil)
        
        
        if let str = placeholder {
            if text.isEmpty {
                labPlaceholder?.text = placeholder
            }
        } else {
            labPlaceholder = nil
            NotificationCenter.default.removeObserver(self, name: UITextView.textDidBeginEditingNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: UITextView.textDidEndEditingNotification, object: nil)
        }
    }
    
    @objc
    private func action(didBeginEditing notification: Notification) {
        labPlaceholder?.text = ""
    }
    
    @objc
    private func action(didEndEditing notification: Notification) {
        if text.isEmpty {
            labPlaceholder?.text = placeholder
        }
    }
}
