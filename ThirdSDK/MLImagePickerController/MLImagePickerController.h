//
//  MLImagePickerController.h
//

#import <UIKit/UIKit.h>

typedef void (^MLImagePickerBlock)(UIImage *img);

@interface MLImagePickerController : NSObject

+ (instancetype) sharedInstance;

- (void) selectedImgWithComplate:(MLImagePickerBlock) block;

- (void) selectedImgAtCameraWitchComplate:(MLImagePickerBlock) block;
- (void) selectedImgAtPhotoWitchComplate:(MLImagePickerBlock) block;

@end
