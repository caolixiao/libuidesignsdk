//
//  MLAgreeView.m
//

#import "MLAgreeView.h"
#import <NSBundle+MLCustomPath.h>

@interface MLAgreeView () {
    __block MLBlockAgreeView _black;
    UIView *vContent;
    
    UILabel *labTitle;
    UIWebView *wbContent;
    
    UIButton *butClose;
    UIButton *butAgree;
    UIButton *butDone;
    
    UIView *vLine1;
    UIView *vLine2;
    
    CGFloat fY;
    CGSize sContent;
    
    NSURL *agreeURL;
}

@end


@implementation MLAgreeView

+ (instancetype) sharedInstance
{
    static MLAgreeView *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MLAgreeView alloc] init];
    });
    
    return sharedInstance;
}

- (instancetype) init
{
    if (self = [super init]) {
        
        fY = CGScale(80);
        sContent = CGSizeMake(CGScale(335), __SCREEN_SIZE.height - CGScale(160));
        
        vContent = [[UIView alloc] init];
        vContent.backgroundColor = __RGB(0xffffff);
        vContent.layer.cornerRadius = 4;
        vContent.layer.masksToBounds = YES;
        [self addSubview:vContent];
        
        [self buildView];
    }
    return self;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    labTitle.frame = CGRectMake(0, 0, sContent.width, CGScale(50));
    butClose.frame = CGRectMake(sContent.width - CGScale(50), 0, CGScale(50), CGScale(50));

    vLine1.frame = CGRectMake(0, VIEWMaxY(labTitle), sContent.width, 1);
    wbContent.frame = CGRectMake(0.0, VIEWMaxY(vLine1), sContent.width, sContent.height - CGScale(100));
    vLine2.frame = CGRectMake(0, VIEWMaxY(wbContent), sContent.width, 1);
    
    butAgree.frame = CGRectMake(CGScale(6), sContent.height - CGScale(44), CGScale(200), CGScale(38));
    butDone.frame = CGRectMake(sContent.width - CGScale(72), sContent.height - CGScale(44), CGScale(60), CGScale(38));
}

- (void) buildView {
    labTitle = [[UILabel alloc] init];
    labTitle.textColor = COLOR_WORD_2;
    labTitle.font = FONT_SIZE18;
    labTitle.textAlignment = NSTextAlignmentCenter;
    labTitle.text = @"服务协议";
    [vContent addSubview:labTitle];
    
    wbContent = [[UIWebView alloc]init];
    [wbContent loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[NSBundle pathHtml:@"agreement"]]]];
    [vContent addSubview:wbContent];
    
    
    vLine1 = [[UIView alloc] init];
    vLine1.backgroundColor = COLOR_LINE;
    [vContent addSubview:vLine1];
    
    vLine2 = [[UIView alloc] init];
    vLine2.backgroundColor = COLOR_LINE;
    [vContent addSubview:vLine2];
    
    
    butClose = [UIButton buttonWithType:UIButtonTypeCustom];
    [butClose setImage:[UIImage imgNamed:@"agree_close"] forState:UIControlStateNormal];
    [butClose addTarget:self action:@selector(actionClickWithButton:) forControlEvents:UIControlEventTouchUpInside];
    
    butAgree = [UIButton buttonWithType:UIButtonTypeCustom];
    [butAgree setImage:[UIImage imgNamed:@"package_sel"] forState:UIControlStateSelected];
    [butAgree setImage:[UIImage imgNamed:@"package_unsel"] forState:UIControlStateNormal];
    [butAgree setImage:[UIImage imgNamed:@"package_sel"] forState:UIControlStateHighlighted];
    [butAgree setTitle:@"我已阅读并接受服务条款" forState:UIControlStateNormal];
    [butAgree setTitleColor:COLOR_GENERAL forState:UIControlStateNormal];
    butAgree.titleLabel.font = FONT_SIZE_GENERAL;
    butAgree.imageView.contentMode = UIViewContentModeScaleAspectFit;
    butAgree.titleEdgeInsets = UIEdgeInsetsMake(0, 12, 0, 0);
    [butAgree addTarget:self action:@selector(actionAgreeWithButton:) forControlEvents:UIControlEventTouchUpInside];


    butDone = [UIButton buttonWithType:UIButtonTypeCustom];
    butDone.layer.cornerRadius = 5;
    butDone.layer.masksToBounds = YES;
    butDone.titleLabel.font = FONT_SIZE_GENERAL;
    [butDone setTitle:@"确定" forState:UIControlStateNormal];
    [butDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [butDone setBackgroundImage:[UIImage imageWithColor:COLOR_GENERAL] forState:UIControlStateNormal];
    [butDone addTarget:self action:@selector(actionClickWithButton:) forControlEvents:UIControlEventTouchUpInside];
    
    butClose.tag = 0;
    butDone.tag = 1;
    butAgree.tag = 2;
    
    [vContent addSubview:butClose];
    
    [vContent addSubview:butAgree];
    [vContent addSubview:butDone];
}

#pragma mark - tool view
- (void) setPathURL:(NSString *) pathURL {
    NSURL *url = [NSURL URLWithString:pathURL];
    if (!url) url = [NSURL fileURLWithPath:pathURL];
    [wbContent loadRequest:[NSURLRequest requestWithURL:url]];
}

- (void) showInSuperView:(UIView *) view
{
    if (!view) {
        UIView *presentView = [UIApplication sharedApplication].keyWindow;
        [presentView addSubview:self];
    } else {
        [view addSubview:self];
    }
    
    vContent.frame = CGRectMake((VIEWW(self) - sContent.width)/2.0, fY, sContent.width, sContent.height);
    [self setNeedsLayout];
    [self layoutIfNeeded];
    
    self.backgroundColor = [UIColor clearColor];
    self.alpha = 0;
    [UIView animateWithDuration:0.23 animations:^{
        self.backgroundColor = __RGBA(0x000000, 0.6);
        self.alpha = 1;
    }];
}

- (void) showInWindowWithblack:(MLBlockAgreeView) black {
    _black = black;

    if (butAgree) butAgree.selected = false;
    self.frame = __SCREEN_BOUNDS;
    [self showInSuperView:nil];
}

-(void)hideView{
    self.alpha = 1;
    [UIView animateWithDuration:0.23 animations:^{
        self.alpha = 0;
        self.backgroundColor = [UIColor clearColor];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    //    [self hideView];
}

#pragma mark -
- (void) actionAgreeWithButton:(UIButton *) button {
    button.selected = !button.selected;
}

- (void) actionClickWithButton:(UIButton *) button {
    if (_black) _black(butAgree.selected, (int)button.tag);
    if (!(!butAgree.selected && button.tag == 1)) [self hideView];

    _black = nil;
}

@end
