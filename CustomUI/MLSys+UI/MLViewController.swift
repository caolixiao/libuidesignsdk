//
//  MLViewController.swift
//

import UIKit
import LibBaseSDK

open class MLViewController: UIViewController, MLNothingViewDelegate {

    private let _vNothin = MLNothingView.init(frame: .zero)
    public private(set) var nothinView: MLNothingView?
    open var isHiddenNull: Bool = true {
        didSet {
            if isHiddenNull {
                _vNothin.removeFromSuperview()
            } else {
//                view.insertSubview(_vNothin, at: 0)
                view.addSubview(_vNothin)
            }
        }
    }
    
    deinit {
        Log.i("deinit MLViewController")
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    public init() {
        super.init(nibName: nil, bundle: nil)
        nothinView  = _vNothin
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = color_f2f2f2
        buildNullData()
    }
    
    override open func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        _vNothin.frame = self.view.bounds
    }
    
    open func buildNullData() {
        _vNothin.attributedText = "暂无信息"
        _vNothin.style = .landscape
        _vNothin.delegate = self
    }
    
    // MARK: - UINavigationItem+MLExpand
    @objc
    open func backItemButton(_ button: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc
    open func rightItemButton(_ button: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc
    open func centerItemButton(_ button: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - MLNothingViewDelegate
    open func refreshNothing() {
        
    }
}


extension MLViewController {
    static let window : UIWindow = UIWindow()
    
    @discardableResult
    static public func show(vc: MLViewController, isNav: Bool = true) -> MLViewController {
        window.frame = __SCREEN_BOUNDS
        window.windowLevel = UIWindow.Level.normal
        window.rootViewController = isNav ? MLNavigationController(rootViewController: vc) : vc
        window.backgroundColor = color_f2f2f2
        window.makeKeyAndVisible()
        return vc
    }
    
    @objc
    public func hide() {
        UIView.animate(withDuration: 0.23, animations: {
            MLViewController.window.backgroundColor = .clear
            self.view.backgroundColor = .clear
            self.view.alpha = 0;
        }) { (finished : Bool) in
            self.dismiss(animated: false, completion: { })
            MLViewController.window.rootViewController = nil
            MLViewController.window.removeFromSuperview()
            MLAppDelegateService.makeKeyAndVisible()
        }
    }
    
    @objc
    public func hideNoAnimate() {
        MLViewController.window.backgroundColor = .clear
        view.backgroundColor = .clear
        view.alpha = 0;
        dismiss(animated: false, completion: { })
        MLViewController.window.rootViewController = nil
        MLViewController.window.removeFromSuperview()
        MLAppDelegateService.makeKeyAndVisible()
    }
}
