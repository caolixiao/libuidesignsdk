//
//  UINavigationItem+MLExpand.h
//

#import <UIKit/UIKit.h>

#pragma mark - UINavigationItem
typedef NS_ENUM(int, NavItemImgType) {
    NavItemImgTypeNone  = 0,
    NavItemImgTypeBKG   = 1,
    NavItemImgTypeLeft  = 2,
    NavItemImgTypeRight = 3,
    NavItemImgTypeUp    = 4,
    NavItemImgTypeDown  = 5,
    NavItemImgTypePoint = 6,
};

@interface UINavigationItem (MLExpand)

@property (nonatomic, strong) UIButton *butLeft;
@property (nonatomic, strong) UIButton *butRight;
@property (nonatomic, strong) UIButton *butRight2;

// 只有标题
- (void) addTitle:(NSString *) title;

/**
 * @brief 修改UINavigationItem的标题
 * @param title 标题，既可以传图片的名字也可以传文字
 */
- (void) modTitle:(NSString *) title;

// 设置中间的view
- (void) setCenterView:(UIView *) view;

// left
- (void) setLeftView:(UIView *) view;
- (void) setLeftTitle:(NSString *) title;
- (void) setLeftImg:(NSString *) imgPath;
- (void) setLeftImg:(NSString *) imgPath title:(NSString *) title;
- (void) setLeftImg:(NSString *) imgPath title:(NSString *) title color:(UIColor *) wColor;
- (void) setLeftImg:(NSString *) imgPath title:(NSString *) title imgType:(NavItemImgType) type;
- (void) setLeftImg:(NSString *) imgPath title:(NSString *) title font:(UIFont*) font imgType:(NavItemImgType) type;
- (void) setLeftImg:(NSString *) imgPath title:(NSString *) title font:(UIFont*) font color:(UIColor *) wColor imgType:(NavItemImgType) type;

// right
- (void) setRightView:(UIView *) view;
- (void) setRightTitle:(NSString *) title;
- (void) setRightImg:(NSString *) imgPath;
- (void) setRightImg:(NSString *) imgPath title:(NSString *) title;
- (void) setRightImg:(NSString *) imgPath title:(NSString *) title color:(UIColor *) wColor;
- (void) setRightImg:(NSString *) imgPath title:(NSString *) title imgType:(NavItemImgType) type;
- (void) setRightImg:(NSString *) imgPath title:(NSString *) title font:(UIFont*) font imgType:(NavItemImgType) type;
- (void) setRightImg:(NSString *) imgPath title:(NSString *) title font:(UIFont*) font color:(UIColor *) wColor imgType:(NavItemImgType) type;

/**
 * @brief 右边添加俩个按钮事件
 * @param img1 ---
 * @param img2 ---
 */
- (void) setRight1Img:(NSString *) img1 right2:(NSString *) img2;

/**
 * @brief 设置相应事件的接受控件 需要实现下边的重构的方法
 * @param delegate 接受事件的控件
 */
- (void) setDelegate:(id)delegate;

// 重构的方法
- (void) backItemButton:(UIButton *) button;
- (void) rightItemButton:(UIButton *) button;
- (void) centerItemButton:(UIButton *) button;

@end
