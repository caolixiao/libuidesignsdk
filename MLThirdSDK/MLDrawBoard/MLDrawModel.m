//
//  MLDrawModel.m
//

#import "MLDrawModel.h"


NSString * const CollectionImageBoardViewID    = @"CollectionImageBoardViewID";
NSString * const ImageBoardNotification        = @"ImageBoardNotification";
NSString * const Action_changebackground       = @"com.changebackground.doodle";
NSString * const Action_playing                = @"com.playing.doodle";
NSString * const SendColorAndWidthNotification = @"SendColorAndWidthNotification";
NSString * const Action_return                 = @"com.return.doodle";
NSString * const Action_goforward              = @"com.goforward.doodle";
NSString * const Action_clearAll               = @"com.clearAll.doodle";


@implementation MLDrawModel

- (instancetype) init {
    if (self = [super init]) {
        self.action = Action_playing;
        self.width = @([UIScreen mainScreen].bounds.size.width);
        self.height = @([UIScreen mainScreen].bounds.size.height);
    }
    return self;
}

@end


@implementation MLPath

- (instancetype) initWithPoint:(CGPoint) beginPoint {
    if (self = [super init]) {
        _fWLine = 2;
        _bPoint= beginPoint;
        _colorPan = [UIColor blackColor];
        _bp = [self defBP];
    }
    return self;
}

- (UIBezierPath *) defBP {
    UIBezierPath *bp = [UIBezierPath bezierPath];
    [bp moveToPoint:_bPoint];
    [self setDefBP:_bp];
    return bp;
}

- (void) setDefBP:(UIBezierPath *) bp {
    bp.lineCapStyle = kCGLineCapRound;
    bp.lineJoinStyle = kCGLineJoinRound;
    bp.lineWidth = _fWLine;
}

- (void) pathToPoint:(CGPoint) movePoint type:(MLDrawingShapeType) type {
    _type = type;
    switch (type) {
        case MLDrawingShapeCurve:
            [self setDefBP:_bp];
            [_bp addLineToPoint:movePoint];
            break;
        case MLDrawingShapeLine:
            _bp = [self defBP];
            [_bp addLineToPoint:movePoint];
            break;
        case MLDrawingShapeEllipse:
            [self setDefBP:_bp];
            _bp = [UIBezierPath bezierPathWithRect:[self getRectWithStart:_bPoint end:movePoint]];
            break;
        case MLDrawingShapeRect:
            [self setDefBP:_bp];
            _bp = [UIBezierPath bezierPathWithOvalInRect:[self getRectWithStart:_bPoint end:movePoint]];
            break;
        default:
            break;
    }
}

- (CGRect) getRectWithStart:(CGPoint) sPoint end:(CGPoint) ePoint {
    CGPoint orignal = sPoint;
    if (sPoint.x > ePoint.x) orignal = ePoint;
    CGFloat fw = fabs(sPoint.x - ePoint.x);
    CGFloat fh = fabs(sPoint.y - ePoint.y);
    return CGRectMake(orignal.x , orignal.y , fw, fh);
}

@end



@implementation MLDrawView

- (instancetype) init {
    if (self = [super init]) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

+ (Class) layerClass {
    return [CAShapeLayer class];
}

- (void) setBrush:(MLPath *)path {
    CAShapeLayer *shapeLayer = (CAShapeLayer *) self.layer;
    shapeLayer.strokeColor = path.colorPan.CGColor;
    shapeLayer.fillColor = [UIColor clearColor].CGColor;
    shapeLayer.lineJoin = kCALineJoinRound;
    shapeLayer.lineCap = kCALineCapRound;
    shapeLayer.lineWidth = path.bp.lineWidth;
    shapeLayer.path = path.bp.CGPath;
}

@end
