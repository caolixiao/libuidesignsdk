//
//  MLEditImgView.m
//

#import "MLEditImgView.h"

#define CGRectEdge(__rect, __edge) CGRectMake(__rect.origin.x + __edge, __rect.origin.y + __edge, __rect.size.width - __edge*2, __rect.size.height - __edge*2)

@implementation MLEditImgView

- (instancetype) init {
    if (self = [super init]) {
        
        self.userInteractionEnabled = true;
        self.multipleTouchEnabled = true;
        [self buildControlView:self];
    }
    return self;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    CGSize size = CGScaleSize(35, 35);
    butDel.frame = (CGRect){{VIEWW(self) - size.width, 0}, size};
    butRotate.frame = (CGRect){CGPointZero, size};
    butZoom.frame = (CGRect){{VIEWW(self) - size.width, VIEWH(self) - size.height}, size};
    iv.frame = CGRectEdge(self.bounds, CGScale(16));
}

#pragma mark -
- (void) buildControlView:(UIView *) vDarw {
    iv = [[UIImageView alloc] init];
    iv.contentMode = UIViewContentModeScaleAspectFill;
    iv.clipsToBounds = true;
    [vDarw insertSubview:iv atIndex:0];
    
    butDel = [UIButton buttonWithType:UIButtonTypeCustom];
    [butDel setImage:[UIImage imageNamed:@"icon-delete"] forState:UIControlStateNormal];
    [butDel addTarget:self action:@selector(delAtImg:) forControlEvents:UIControlEventTouchUpInside];
    [vDarw addSubview:butDel];
    
    butRotate = [UIButton buttonWithType:UIButtonTypeCustom];
    [butRotate setImage:[UIImage imageNamed:@"icon-rotate"] forState:UIControlStateNormal];
//    [butRotate addTarget:self action:@selector(rotateAtImg:) forControlEvents:UIControlEventTouchUpInside];
    [vDarw addSubview:butRotate];
    
    butZoom = [UIButton buttonWithType:UIButtonTypeCustom];
    [butZoom setImage:[UIImage imageNamed:@"icon-zoom"] forState:UIControlStateNormal];
//    [butZoom addTarget:self action:@selector(zoomAtImg:) forControlEvents:UIControlEventTouchUpInside];
    [vDarw addSubview:butZoom];
    
    UIPanGestureRecognizer *pan1 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
    [butRotate addGestureRecognizer:pan1];
    
    UIPanGestureRecognizer *pan2 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
    [butZoom addGestureRecognizer:pan2];
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
    [vDarw addGestureRecognizer:pan];
    
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinch:)];
    [vDarw addGestureRecognizer:pinch];
    
    UIRotationGestureRecognizer *rotate = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotate:)];
    [vDarw addGestureRecognizer:rotate];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [vDarw addGestureRecognizer:tap];
}

#pragma mark - 
- (void) setImg:(UIImage *)img {
    _img = img;
    iv.image = img;
    
    UIView *sup = self.superview;
    CGSize size = img.imgSize;
    CGFloat scale = img.scale_w_h;
    
    CGFloat fW = VIEWW(sup);
    CGFloat fH = VIEWH(sup);
    
    if (scale > fW/fH) {
        if (fW > size.width) fW = size.width;
        fH = fW*scale;
    }

    self.frame = CGRectMake((VIEWW(sup) - fW)/2.0, (VIEWH(sup) - fH)/2.0, fW, fH);
}

#pragma mark -
- (void) delAtImg:(UIButton *) button {
    [button.superview removeFromSuperview];
}

- (void) pan:(UIPanGestureRecognizer *) pan {
    UIView *view = pan.view;
    if (view == butZoom) {
        UIView *view = self;
        if (pan.state == UIGestureRecognizerStateBegan || pan.state == UIGestureRecognizerStateChanged) {
            
            CGPoint center = view.center;
            CGPoint prePoint = butZoom.center;
            
            CGPoint translation = [pan translationInView:butZoom];
            CGPoint curPoint = CGPointMake(prePoint.x + translation.x, prePoint.y + translation.y);
            
            // 计算缩放
            CGFloat preDistance = [self getDistance:prePoint withPointB:center];
            CGFloat curDistance = [self getDistance:curPoint withPointB:center];
            CGFloat scale = curDistance / preDistance;
            
            view.transform = CGAffineTransformScale(view.transform, scale, scale);
            [pan setTranslation:CGPointZero inView:butZoom];
        }
    }
    else if (view == butRotate) {
        UIView *view = self;
        if (pan.state == UIGestureRecognizerStateBegan || pan.state == UIGestureRecognizerStateChanged) {
            
            CGPoint center = view.center;
            CGPoint prePoint = butRotate.center;
            
            CGPoint translation = [pan translationInView:butRotate];
            CGPoint curPoint = CGPointMake(prePoint.x + translation.x, prePoint.y + translation.y);
            
            // 计算弧度
            CGFloat preRadius = [self getRadius:center withPointB:prePoint];
            CGFloat curRadius = [self getRadius:center withPointB:curPoint];
            CGFloat radius = curRadius - preRadius;
            radius = - radius;

            view.transform = CGAffineTransformRotate(view.transform, radius);
            
//            CGAffineTransform transform = CGAffineTransformScale(view.transform, scale, scale);
//            view.transform = CGAffineTransformRotate(transform, radius);
            
            [pan setTranslation:CGPointZero inView:butRotate];
        }
    }
    else {
        if (pan.state == UIGestureRecognizerStateBegan || pan.state == UIGestureRecognizerStateChanged) {
            CGPoint translation = [pan translationInView:view.superview];
            [view setCenter:(CGPoint){view.center.x + translation.x, view.center.y + translation.y}];
            [pan setTranslation:CGPointZero inView:view.superview];
        }
    }
}

- (void) pinch:(UIPinchGestureRecognizer *) pinch {
    UIView *view = pinch.view;
    if (pinch.state == UIGestureRecognizerStateBegan || pinch.state == UIGestureRecognizerStateChanged) {
        view.transform = CGAffineTransformScale(view.transform, pinch.scale, pinch.scale);
        pinch.scale = 1;
    }
}

- (void) rotate:(UIRotationGestureRecognizer *) rotate {
    UIView *view = rotate.view;
    if (rotate.state == UIGestureRecognizerStateBegan || rotate.state == UIGestureRecognizerStateChanged) {
        view.transform = CGAffineTransformRotate(view.transform, rotate.rotation);
        rotate.rotation = 0;
    }
}

- (void) tap:(UITapGestureRecognizer *) tap {
    self.hiddenControl = !self.hiddenControl;
    if (_hid) _hid(self, self.hiddenControl);
}

#pragma mark - --
- (CGFloat) getDistance:(CGPoint) pointA withPointB:(CGPoint) pointB {
    CGFloat x = pointA.x - pointB.x;
    CGFloat y = pointA.y - pointB.y;
    return sqrt(x*x + y*y);
}

- (CGFloat) getRadius:(CGPoint) pointA withPointB:(CGPoint) pointB {
    CGFloat x = pointA.x - pointB.x;
    CGFloat y = pointA.y - pointB.y;
    return atan2(x, y);
}

#pragma mark -
- (void) setHiddenControl:(BOOL) hiddenControl {
    _hiddenControl = hiddenControl;
    butDel.hidden = butRotate.hidden = butZoom.hidden = hiddenControl;
}

@end
