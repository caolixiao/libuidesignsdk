//
//  MLThumbnailGridView.swift
//

import UIKit
import Foundation

protocol MLThumbnailGridViewDelegate {
    func loadPageView(_ currentPage:Int)->UIView
}

class MLThumbnailGridView: UIView,UICollectionViewDataSource,UICollectionViewDelegate {
    
    fileprivate let collectionView:UICollectionView=UICollectionView.init(frame:CGRect.zero, collectionViewLayout: MLThumbnailGridViewLayout.init())
    var delegate:MLThumbnailGridViewDelegate?
    var numberOfPage:Int=1
    {
        didSet
        {
            collectionView.reloadData()
        }
    }
    override  init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor=UIColor.white
        let layout:UICollectionViewFlowLayout=collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.scrollDirection=UICollectionViewScrollDirection.horizontal
        layout.minimumLineSpacing=0
        collectionView.frame=CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        collectionView.delegate=self
        collectionView.dataSource=self
        collectionView.isPagingEnabled=true
        collectionView.showsVerticalScrollIndicator=false
        collectionView.showsHorizontalScrollIndicator=false
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "thumbnailGrid")
        collectionView.backgroundColor=UIColor.white
        self.addSubview(collectionView)
        // pageControl.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.6)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: UICollectionViewDataSource
    func collectionView(_ collectionView:UICollectionView, layout collectionViewLayout:UICollectionViewLayout,sizeForItemAtIndexPath indexPath:IndexPath)->CGSize
    {
        return CGSize(width: self.frame.size.width,height: self.frame.size.height);
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return numberOfPage
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "thumbnailGrid", for: indexPath)
        
        if((self.viewWithTag(100+indexPath.section)) != nil)
        {
            return cell
        }
        // Configure the cell
        let pageView=delegate?.loadPageView(indexPath.section)
        if((pageView) != nil)
        {
            for intem  in cell.contentView.subviews {
                intem.removeFromSuperview()
            }
            pageView?.tag=100+indexPath.section
            cell.contentView.addSubview(pageView!)
        }
        return cell
    }

}
class MLThumbnailGridViewLayout: UICollectionViewFlowLayout
{

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]?
    {
        if let attributes=super.layoutAttributesForElements(in: rect)
        {
            for item in attributes {
                
                let distance=abs(item.center.x - collectionView!.frame.size.width * 0.5 - collectionView!.contentOffset.x)
                var scale:Float=0.5
                let w = (self.collectionView!.frame.size.width + self.itemSize.width) * 0.5;
                if (distance >= w) {
                    
                    scale = 0.5;
                }
                else
                {
                    scale = scale +  (1 - Float(distance) / Float(w) ) * 0.5;
                    
                }
                
                item.transform = CGAffineTransform(scaleX: CGFloat(scale), y: CGFloat(scale));
            }
            //print("-----attributes:\(attributes)")
            return attributes
        }
        return nil;
    }
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint
    {
        let rect:CGRect=CGRect(x: proposedContentOffset.x, y: proposedContentOffset.y, width: collectionView!.frame.size.width, height: collectionView!.frame.size.height)
        if let attributes=super.layoutAttributesForElements(in: rect)
        {
            var gap:CGFloat=1000
            var a:CGFloat=0
            for item in attributes
            {
                if (gap > abs(item.center.x - proposedContentOffset.x - collectionView!.frame.size.width * 0.5))
                {
                    
                    gap =  abs(item.center.x - proposedContentOffset.x - collectionView!.frame.size.width * 0.5);
                    a = item.center.x - proposedContentOffset.x - collectionView!.frame.size.width * 0.5;
                }
            }
            return CGPoint(x: proposedContentOffset.x+a, y: proposedContentOffset.y)
            
        }
        return proposedContentOffset
        
    }
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool
    {
        return true
    }
}
