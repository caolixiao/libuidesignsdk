//
//  MLNothingView.swift
//

import UIKit
import LibBaseSDK

@objc
@available(iOS 9.0, *)
public protocol MLNothingViewDelegate : NSObjectProtocol {
    @available(iOS 9.0, *)
    func refreshNothing()
    
    @objc
    @available(iOS 9.0, *)
    optional func refreshButtonAction()
}

public enum MLNothingViewStyle {
    case def
    case full
    case cache
    case landscape
}

@objcMembers
public class MLNothingView: UIView {
    
    weak public var delegate: MLNothingViewDelegate?
    public var style: MLNothingViewStyle = .landscape
    public var title: String = "" { didSet { labTitle.text = title + "\n" + "点击刷新" } }
    
    public var attributedText : String = "" {
        didSet { labTitle.attributedText = NSMutableAttributedString(string: attributedText) }
    }
    
    public var attributed: NSAttributedString = NSAttributedString() {
        didSet { labTitle.attributedText = attributed }
    }
    
    public var imgName: String = "" {
        didSet { ivMsg.image = UIImage(named: imgName); }
    }
    
    public var imgContentMode: UIView.ContentMode = .scaleAspectFit {
        didSet { ivMsg.contentMode = imgContentMode }
    }
    
    public let ivMsg  = UIImageView(image: UIImage(uidesignNamed: "MLNothingView_null"))
    public let labTitle = UILabel.init(frame: CGRect.zero)
    public let butBKG  = UIButton(type: .custom)
    public let butRefresh = UIButton(type: .custom)
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        ivMsg.contentMode = UIView.ContentMode.scaleAspectFit
        self.addSubview(ivMsg)
        
        
        labTitle.numberOfLines = 1
        labTitle.textColor = .lightGray
        labTitle.textAlignment = .center
        labTitle.adjustsFontSizeToFitWidth = true
        labTitle.font = UIFont.systemFont(ofBaseSize: 13)
        self.addSubview(labTitle)
        
        butRefresh.setTitleColor(color_2a2a2a, for: .normal)
        butRefresh.titleLabel?.font = UIFont.systemFont(ofBaseSize: 16)
        butRefresh.backgroundColor = __RGB(0xF7FBFC)
        butRefresh.layer.cornerRadius = 5
        butRefresh.layer.masksToBounds = true
        butRefresh.layer.borderColor = color_line.cgColor
        butRefresh.layer.borderWidth = 0.5
        butRefresh.addTarget(self, action: #selector(refreshButton), for: .touchUpInside)
        self.addSubview(butRefresh)
        butRefresh.isHidden = true
        
        butBKG.backgroundColor = UIColor.clear
        butBKG.addTarget(self, action: #selector(MLNothingView.refresh), for: UIControl.Event.touchUpInside)
        self.addSubview(butBKG)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        switch style {
        case .def:
            let height = VIEWH(self)/4*3
            let rate : CGFloat = 347/469
            
            ivMsg.frame  = CGRect(x: 0, y: 0, width: rate*height, height: height)
            ivMsg.center = CGPoint(x: self.center.x, y: self.center.y - 20 - self.frame.origin.y)
            
            labTitle.frame = CGRect(x: 0, y: VIEWMaxY(ivMsg), width: VIEWW(self), height: CGScale(20))
            butBKG.frame  = CGRect(x: 0, y: 0,      width: VIEWW(self), height: VIEWH(self))
            break
        case .full:
            let fW = VIEWW(self)*0.5
            let fH = (__iPhoneX ? fW*2.5 : fW*2.1)
            
            let rate : CGFloat = 347/469
            
            ivMsg.frame  = CGRect(x: 0, y: 0, width: rate*fW, height: fW)
            ivMsg.center = CGPoint(x: self.center.x, y: self.center.y - 100)
            
            labTitle.frame = CGRect(x: 0, y: VIEWMaxY(ivMsg), width: VIEWW(self), height: CGScale(20))
            butBKG.frame  = labTitle.frame
            
            butRefresh.frame = CGRect(x: (VIEWW(self)-CGScale(165))/2.0, y: VIEWMaxY(labTitle)+CGScale(9), width: CGScale(165), height: CGScale(45))
            
            break
        case .cache:
            ivMsg.frame  = CGRect(x: CGScale(148), y: CGScale(213), width: CGScale(80), height: CGScale(60))
            labTitle.frame = CGRect(x: 0, y: VIEWH(ivMsg)+VIEWY(ivMsg)+CGScale(9), width: VIEWW(self), height: CGScale(20))
            butBKG.frame  = labTitle.frame
            butRefresh.frame = CGRect(x: (VIEWW(self)-CGScale(165))/2.0, y: VIEWMaxY(labTitle)+CGScale(9), width: CGScale(165), height: CGScale(45))
            break
        case .landscape:
            ivMsg.frame = CGRect(x: CGScale(109), y: CGScale(235) - __fHNav, width: CGScale(63), height: CGScale(63))
            labTitle.frame = CGRect(x: VIEWMaxX(ivMsg) + CGScale(24), y: VIEWY(ivMsg), width: VIEWW(self) - VIEWH(ivMsg) + CGScale(48), height: CGScale(63))
            butBKG.frame  = labTitle.frame
            
            labTitle.textAlignment = .left
            labTitle.textColor = __RGB(0x959595)
            labTitle.font = UIFont.systemFont(ofBaseSize: 18)
            break
        default:
            break
        }
    }
    
    public func refresh() {
        delegate?.refreshNothing()
    }
    
    public func refreshButton() {
        delegate?.refreshButtonAction!()
    }
    
    override public func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if self.isUserInteractionEnabled == false || self.isHidden == true || self.alpha <= 0.01 {
            return nil
        }
        
        if self.point(inside: point, with: event) == false {
            return nil
        }
        
        let p = self.convert(point, to: butBKG)
        if let fitView = butBKG.hitTest(p, with: event) {
            return fitView
        }
        
        let pp = self.convert(point, to: butRefresh)
        if let fitView = butRefresh.hitTest(pp, with: event) {
            return fitView
        }
        
        return nil
    }
}
