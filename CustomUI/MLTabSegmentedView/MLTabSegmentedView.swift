//
//  MLTabSegmentedView.swift
//

import UIKit
import LibBaseSDK

@objc
public protocol MLTabSegmentedViewDelegate : NSObjectProtocol {
    
    func TabSegmentedLoadView(_ indexPath: IndexPath,view:UIView?) ->UIView?
    
    @objc optional
    func TabSegmentDidChange(_ selectedSegmentIndex:Int)
    
    @objc optional
    func TabSegmentWillChange(_ selectedSegmentIndex:Int) ->Bool
    
    @objc optional
    func TabSegmentLayoutSubviewsChange(_ hContent : CGFloat)
}


open class MLTabSegmentedViewFlowLayout: UICollectionViewFlowLayout {
    
    override open func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    override open func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributes=super.layoutAttributesForElements(in: rect)
        for item in attributes! {
            item.frame=CGRect(x: item.frame.origin.x, y: 0, width: item.frame.size.width, height: self.collectionView!.bounds.size.height)
        }
        return attributes
    }
    
    override open func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attributes=super.layoutAttributesForItem(at: indexPath)
        return attributes
    }
    
    override open var collectionViewContentSize : CGSize {
        let width=CGFloat(self.collectionView!.numberOfSections)*self.collectionView!.frame.size.width
        let height=self.collectionView!.frame.size.height
        return  CGSize(width: width, height: height);
    }
}

@objc
open class MLTabSegmentedView: UIView, UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    fileprivate let _scrollviewBar  = UIScrollView.init();
    fileprivate let _segmented      = MLSegmentedControl.init(items: nil)
    fileprivate let _vSelectedLine  = UIView.init(frame: CGRect.zero)
    fileprivate let _vBottomLine    = UIImageView.init(frame: CGRect.zero)
    fileprivate let _collectionView = UICollectionView.init(frame: CGRect.zero,collectionViewLayout:MLTabSegmentedViewFlowLayout())
    
    fileprivate var isScrollTo = false
    fileprivate let identifier = "tabSegmented"
    fileprivate let segAttr = [NSAttributedString.Key.foregroundColor : color_2a2a2a, NSAttributedString.Key.font : UIFont.systemFont(ofBaseSize: 15)]
    fileprivate var selectedIndex : Int = 0
    fileprivate var animationAtSelectedIndex = 0
    
    weak open var delegate  : MLTabSegmentedViewDelegate? = nil
    open var collectionView : UICollectionView { get { return _collectionView } }
    open var tabSegmented   : MLSegmentedControl { get { return _segmented } }
    open var hContent       : CGFloat = 0.0
    open var scrolIndex     : Int = 0
    open var selectedSegmentIndex : Float { get { return Float(selectedIndex) }}
    open var isReloadSelected : Bool = true
    open var fW_min : CGFloat = 0.0;
    
    // MARK: - parame
    open var setSegmentedColor = UIColor.white { didSet { _segmented.tintColor = setSegmentedColor; _segmented.backgroundColor = setSegmentedColor; _segmented.segmentedIOS13Style()} }
    open var isShowBottomoLine = false { didSet { _vBottomLine.frame = isShowBottomoLine ? CGRect(x: 0, y: 44, width: VIEWW(self), height: 1) : .zero } }
    open var segmentTitle = [NSString]() { didSet{ if segmentTitle.count != 0 { loadingSegmentItem() } } }
    open var segmentImg   = [NSString]() { didSet{ if segmentImg.count != 0 { loadingSegmentItem() } } }
    open var selected : Int = 0 {
        didSet {
            _segmented.selectedSegmentIndex = selected;
            actionDidChangeAtSegment()
        }
    }
    
    
    // MARK: - methon
    open func reloadData() {
        _collectionView.reloadData()
    }

    open func loadingSegmentItem() {
        _segmented.removeAllSegments()
        var index=0
        var fW = (fW_min > 1) ? CGScale(0) : __SCREEN_SIZE.width;
        for item in segmentTitle {
            _segmented.insertSegment(withTitle: item as String, at:index , animated: false)
            if (fW_min > 1) {
                let fWTitle = fWAtTitle(item as String )
                let fTmp = fWTitle > fW_min ? fWTitle : fW_min
                fW += fTmp
                _segmented.setWidth(fTmp, forSegmentAt: index)
            }
            index+=1
        }
        for item in segmentImg {
            let img = UIImage.init(named: item as String)?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
            _segmented.insertSegment(with: img , at: index, animated: false)
            index+=1
        }
        
        _segmented.selectedSegmentIndex = 0
        if !isReloadSelected {
            _segmented.selectedSegmentIndex = selectedIndex;
        }
 
        _segmented.frame = CGRect(x: 0, y: 0, width: fW, height: CGScale(44))
        _scrollviewBar.contentSize = _segmented.frame.size
        
       _collectionView.reloadData()
        actionDidChangeAtSegment()
    }
    
    open func replaceImg(img named: String, index: Int ) {
        let img = UIImage.init(named: named)?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        _segmented.setImage(img, forSegmentAt: index);
    }
    
    
    // MARK: - system
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        if #available(iOS 11.0, *) {
            _scrollviewBar.contentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior.never
        }
        
        _scrollviewBar.showsHorizontalScrollIndicator = false
        _scrollviewBar.backgroundColor = UIColor.clear
        self.addSubview(_scrollviewBar)
        
        _segmented.tintColor = setSegmentedColor
        if #available(iOS 13.0, *) {
            _segmented.segmentedIOS13Style()
        }
        _segmented.backgroundColor = setSegmentedColor
        _segmented.setTitleTextAttributes(segAttr, for: UIControl.State.normal)
//        _segmented.setTitleTextAttributes(segAttr, for: UIControl.State.selected)
        _segmented.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: color_2a2a2a, NSAttributedString.Key.font: UIFont.boldSystemFont(ofBaseSize: 15)], for: .selected)
        _segmented.addTarget(self, action: #selector(actionDidChangeAtSegment), for: UIControl.Event.valueChanged)
        _segmented.selectedSegmentIndex = 0

        _scrollviewBar.addSubview(_segmented)
        
        
        let layout = _collectionView.collectionViewLayout as! MLTabSegmentedViewFlowLayout;
        layout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        layout.headerReferenceSize = CGSize.zero;
        layout.sectionInset = UIEdgeInsets.zero;
        
        _collectionView.delegate = self
        _collectionView.dataSource = self
        _collectionView.isPagingEnabled = true
        _collectionView.backgroundColor = UIColor.white
        _collectionView.showsVerticalScrollIndicator = false
        _collectionView.showsHorizontalScrollIndicator = false
        _collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: identifier)
        self.addSubview(_collectionView)
        
//        _vBottomLine.backgroundColor = UIColor.clear
        _vBottomLine.image = UIImage(uidesignNamed: "MLTabSegmentedView_000001")
        self.addSubview(_vBottomLine)
        
        _vSelectedLine.layer.cornerRadius = 1.5
        _vSelectedLine.layer.masksToBounds = true
        _vSelectedLine.backgroundColor = color_main
        _scrollviewBar.addSubview(_vSelectedLine)
        

    }

    
    override open func layoutSubviews() {
        super.layoutSubviews()
        _scrollviewBar.frame  = CGRect(x: 0, y: 0, width: VIEWW(self), height: CGScale(44))
        if (fW_min < 1) { _segmented.frame  = CGRect(x: 0, y: 0, width: VIEWW(self), height: CGScale(44))}
        _vBottomLine.frame    = isShowBottomoLine ? CGRect(x: 0, y: CGScale(44), width: VIEWW(self), height: 4) : .zero
        _collectionView.frame = CGRect(x: 0, y: VIEWMaxY(_segmented), width: VIEWW(self), height: VIEWH(self) - VIEWMaxY(_segmented))
        
        if _collectionView.indexPathsForVisibleItems.count > 0 {
            _collectionView.reloadSections(IndexSet.init(integer: selectedIndex))
        }
        
        hContent = VIEWH(_collectionView)
        reloadSelectedLineView()
        
        delegate?.TabSegmentLayoutSubviewsChange?(hContent)
    }
    
    @objc open func actionDidChangeAtSegment() {
        let enter = delegate?.TabSegmentWillChange?(_segmented.selectedSegmentIndex) ?? false
        if enter { _segmented.selectedSegmentIndex = selectedIndex; }
        else {
            if _collectionView.indexPathsForVisibleItems.count > 0 {
                isScrollTo = false
                let indexPath = IndexPath.init(row: 0, section: _segmented.selectedSegmentIndex)
                
                if selectedIndex > _segmented.selectedSegmentIndex {
                    _collectionView.scrollToItem(at: indexPath, at: .left, animated: false)
//                    isScrollTo = true
                }
                
                if selectedIndex < _segmented.selectedSegmentIndex {
                    _collectionView.scrollToItem(at: indexPath, at: .right, animated: false)
//                    isScrollTo=true
                }
            }
            
            reloadSelectedLineView()
        }
        delegate?.TabSegmentDidChange?(_segmented.selectedSegmentIndex)
        selectedIndex = _segmented.selectedSegmentIndex
    }
    
    // MARK: - reload select line
    open func reloadSelectedLineView() {
        var x : CGFloat = 4
        
        if (fW_min < 1) {
            let numberOfSegments = _segmented.numberOfSegments == 0 ? 1 : _segmented.numberOfSegments
            let fW = VIEWW(_segmented)/CGFloat(numberOfSegments)
            x = (fW - 12)/2.0 + fW * CGFloat(_segmented.selectedSegmentIndex)
            
        } else {
            for ii in 0..<_segmented.selectedSegmentIndex {
                x += _segmented.widthForSegment(at: ii)
            }
            x += (_segmented.widthForSegment(at: _segmented.selectedSegmentIndex) - 12)*0.5
        }
        
        _vSelectedLine.frame = CGRect(x: x, y: VIEWMaxY(_segmented) - 3, width: 12, height: 3)
        if (_segmented.selectedSegmentIndex >= segmentTitle.count ||
            self.segmentTitle[_segmented.selectedSegmentIndex].length == 0 ||
            VIEWW(self) < 1) {
            _vSelectedLine.frame = .zero
        }
        animationOfChangeAtselectedLine(_segmented.selectedSegmentIndex)
    }

    open func fWAtTitle(_ str : String) -> CGFloat {
        let strTmp = str as NSString
        let rect = strTmp.boundingRect(with: CGSize(width: 1000, height: 20), options: .usesFontLeading, attributes:segAttr, context: nil)
        return rect.size.width + 8
    }

    open func animationOfChangeAtselectedLine(_ index : Int) {
        let animation = CATransition.init()
        animation.type = CATransitionType(rawValue: "moveIn")
        animation.subtype = CATransitionSubtype(rawValue: animationAtSelectedIndex > index ? "fromRight" : "fromLeft")
        animation.duration = 0.23 //Double(0.10)/Double(VIEWW(self)/CGScale(375));
        _vSelectedLine.layer.removeAllAnimations()
        _vSelectedLine.layer.add(animation, forKey: nil)
        animationAtSelectedIndex = index
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    public func collectionView(_ collectionView:UICollectionView, layout collectionViewLayout : UICollectionViewLayout, sizeForItemAt indexPath : IndexPath)->CGSize {
        return CGSize(width: VIEWW(self),height: VIEWH(_collectionView));
    }

    // MARK: - UICollectionViewDataSource
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return scrolIndex == 0 ? _segmented.numberOfSegments : scrolIndex
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        cell.backgroundColor = .white
        
        let iPath = isReloadSelected ? indexPath : IndexPath.init(row: 0, section: _segmented.selectedSegmentIndex)
        let vTag = 100 + iPath.section
        let v = cell.viewWithTag(vTag)
        if let view = delegate?.TabSegmentedLoadView(iPath, view: v) {
            view.frame = CGRect(x: 0, y: 0, width: VIEWW(_collectionView), height: VIEWH(_collectionView))
            cell.contentView.addSubview(view)
            cell.contentView.bringSubviewToFront(view)
            view.tag = vTag
        }
        
        return cell
    }
    
    // MARK: - UIScrollViewDelegate
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !isScrollTo {
            let page = (Int)(scrollView.contentOffset.x/scrollView.frame.size.width+0.5)
            if page != _segmented.selectedSegmentIndex && page >= 0 {
                selectedIndex = page
                _segmented.selectedSegmentIndex = page
                actionDidChangeAtSegment()
            }
        }
    }
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        isScrollTo = false
    }
    
    public func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        isScrollTo = false
    }
}
