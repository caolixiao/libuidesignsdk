//
//  MLRunsLanternView.h
//

#import <UIKit/UIKit.h>

typedef void (^MLRunsLanternViewAtClick) (void);
@interface MLRunsLanternView : UIView

+ (instancetype) sharedInstance;
+ (void) superview:(UIView *) view;

+ (void) showRLView:(UIView *) view frame:(CGRect) frame content:(NSString *) content;
+ (void) showRLView:(UIView *) view content:(NSString *) content;
+ (void) showRL:(NSString *) content;

+ (void) setContent:(NSString *) content;

+ (void) cancelShow;

+ (void) setHidden:(BOOL) isHidden;

+ (void) didClick:(MLRunsLanternViewAtClick) click;

@end
