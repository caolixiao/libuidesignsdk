//
//  MIPageViewCell.swift
//  mice
//
//  Created by caolixiao on 2/9/20.
//  Copyright © 2020 Grand China Express International Business Travel Co.,Ltd. All rights reserved.
//

import UIKit
import LibBaseSDK
import FSPagerView

public class MIPageViewCell: FSPagerViewCell {
    let shadowView = UIImageView(frame: .zero)
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        resetUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        resetUI()
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        if let img = imageView {
            img.frame = CGRect(x: 0, y: 0, width: self.contentView.bounds.width, height: self.contentView.bounds.height-20)
            Log.i("==========\(img.frame)")
            shadowView.frame = CGRect(x: 0, y: 10, width: self.contentView.bounds.width, height: self.contentView.bounds.height-10)
        }
        
    }
    
    func resetUI() {
        self.contentView.layer.shadowColor = UIColor.clear.cgColor
        self.contentView.layer.shadowRadius = 3
        self.contentView.layer.shadowOpacity = 0
        self.contentView.layer.shadowOffset = .zero
        
        if let img = imageView {
            img.layer.cornerRadius = 6
            img.layer.masksToBounds = true
            
            shadowView.image = UIImage(named: "模板5-投影")
            //            shadowView.layer.masksToBounds = false
            //            shadowView.backgroundColor = .clear
            //            shadowView.tag = 100
            //            shadowView.layer.shadowOpacity = 0.8
            //            shadowView.layer.shadowRadius = 6
            //            shadowView.layer.shadowColor = __RGB(0xeeeeee).cgColor
            //            shadowView.layer.shadowOffset = CGSize(width: 0, height: 0)
            self.contentView.insertSubview(shadowView, belowSubview: img)
        }
    }
    
    func Model51Shadow(_ itemFrame:CGRect) {
        shadowView.frame = itemFrame
        let shadowPathWidth:CGFloat = 20
        let sizeWith = shadowView.bounds.size.width;
        let sizeHeight = shadowView.bounds.size.height;
        let rect = CGRect(x: 0, y: sizeHeight-shadowPathWidth/2, width: sizeWith, height: shadowPathWidth)
        let bezier = UIBezierPath(roundedRect: rect, cornerRadius: 6)
        shadowView.layer.shadowPath = bezier.cgPath
    }
    
    
}

